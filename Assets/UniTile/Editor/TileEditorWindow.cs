using UnityEngine;
using UnityEditor;
using System.Collections;

public class TileEditorWindow : EditorWindow {
	
	public TileLayerEditor editor;
	public TileLayer layer;
	public int selectedTile;
	public GameObject prefab;
	public Vector2 prefabOffset;
	public new string name;
	public float value;
	public bool boxCollider;
	public int boxLayer;
	public string boxTag;
	public float boxDepth;
	public PhysicMaterial boxMaterial;
	public bool resizable;
	public int propertiesLength;
	public UniTileProperty[] properties;
	
	public Vector2 scroll;
	
	void OnGUI () {
		if(editor == null) return;
		
		this.scroll = EditorGUILayout.BeginScrollView(scroll);
		
		EditorGUILayout.BeginVertical();
			this.name = EditorGUILayout.TextField("Name",this.name);
			this.value = EditorGUILayout.FloatField("Value", this.value);
		
			EditorGUILayout.BeginHorizontal();
				EditorGUILayout.PrefixLabel("Prefab");
				this.prefab = (GameObject)EditorGUILayout.ObjectField(this.prefab, typeof(GameObject));
			EditorGUILayout.EndHorizontal();
		
			if(this.prefab!=null) {
				this.prefabOffset = EditorGUILayout.Vector2Field("Prefab offset", this.prefabOffset);
			}
		
			this.boxCollider = EditorGUILayout.Toggle("Box collider", this.boxCollider);
			if(this.boxCollider) {
				this.boxLayer = EditorGUILayout.LayerField("- Box layer", this.boxLayer);
				this.boxTag = EditorGUILayout.TagField("- Box tag", this.boxTag);
				this.boxDepth = EditorGUILayout.FloatField("- Box depth", this.boxDepth);
				this.boxMaterial = (PhysicMaterial)EditorGUILayout.ObjectField("- Physics material", this.boxMaterial, typeof(PhysicMaterial));
			}
			
			this.resizable = EditorGUILayout.Toggle("Resizable", this.resizable);
		
			propertiesLength = EditorGUILayout.IntField("Properties", propertiesLength);
		
			if(propertiesLength>this.properties.Length) {
				UniTileProperty [] temp=this.properties;
				this.properties=new UniTileProperty[propertiesLength];
				for(int i=0;i<this.properties.Length;i++) {
					if(i>=temp.Length) {
						this.properties[i]=new UniTileProperty();
						this.properties[i].key=this.properties[i].value="";
					} else {
						this.properties[i]=temp[i];
					}
				}
			}
			for(int i=0;i<propertiesLength;i++) {
				EditorGUILayout.BeginHorizontal();
					this.properties[i].key = EditorGUILayout.TextField(this.properties[i].key);
					this.properties[i].value = EditorGUILayout.TextField(this.properties[i].value);
				EditorGUILayout.EndHorizontal();
			}
		
		EditorGUILayout.EndVertical();
		
		if(GUILayout.Button("Save")) {
			this.Save();
			this.Close();
		}
		
		EditorGUILayout.EndScrollView();
	}
	
	public void Setup(TileLayerEditor editor, int selectedTile) {
		this.editor = editor;
		this.selectedTile = selectedTile;
		this.layer = editor.target as TileLayer;
		this.boxDepth = 64;
		this.boxTag = "Untagged";
		this.prefabOffset = new Vector2(this.layer.tileSize.x / 2f, this.layer.tileSize.y / 2f);
		if(layer.tileset!=null) {
			this.ResizeArray();
			UniTileTile tile = layer.tileset.tiles[this.selectedTile];
			if(tile!=null) {
				this.prefab = tile.prefab;
				this.name = tile.name;
				this.value = tile.value;
				this.boxCollider = tile.boxCollider;
				this.boxLayer = tile.boxLayer;
				this.boxMaterial = tile.boxMaterial;
				this.boxTag = tile.boxTag;
				this.boxDepth = tile.boxDepth;
				this.resizable = tile.resizable;
				this.prefabOffset = tile.prefabOffset;
				if(tile.properties!=null) {
					this.properties = new UniTileProperty[tile.properties.Length];
					for(int i=0; i<this.properties.Length;i++) {
						this.properties[i] = new UniTileProperty();
						this.properties[i].key = tile.properties[i].key;
						this.properties[i].value = tile.properties[i].value;
					}
				} else {
					this.properties = new UniTileProperty[0];
				}
			}
		}
		if(this.properties == null) this.properties = new UniTileProperty[0];
		this.propertiesLength = this.properties.Length;
		if(this.name == null) this.name = "";
	}
	
	public void Save() {
		GameObject g = null;
		
		UniTileTileset tileset;
		g = (GameObject)Instantiate(layer.tileset.gameObject);
		tileset = g.GetComponent<UniTileTileset>();
		
		this.ResizeArray();
		if(tileset.tiles[this.selectedTile] == null) {
			tileset.tiles[this.selectedTile] = new UniTileTile();
		}
		UniTileTile tile = tileset.tiles[this.selectedTile];
		tile.properties = new UniTileProperty[propertiesLength];
		for(int i=0; i<this.propertiesLength; i++) {
			tile.properties[i] = new UniTileProperty();
			tile.properties[i].key=this.properties[i].key;
			tile.properties[i].value=this.properties[i].value;
		}
		tile.prefab = this.prefab;
		tile.name = this.name;
		tile.value = this.value;
		tile.boxCollider = this.boxCollider;
		tile.boxLayer = this.boxLayer;
		tile.boxMaterial = this.boxMaterial;
		tile.boxTag = this.boxTag;
		tile.boxDepth = this.boxDepth;
		tile.resizable = this.resizable;
		tile.prefabOffset = this.prefabOffset;
		
		EditorUtility.ReplacePrefab(g, layer.tileset.gameObject);
		
		DestroyImmediate(g);
	}
	
	public void ResizeArray() {
		int tilesX = (int)Mathf.Floor((float)layer.material.mainTexture.width / (layer.tileSize.x + layer.borderSize.x));
		int tilesY = (int)Mathf.Floor((float)layer.material.mainTexture.height / (layer.tileSize.y + layer.borderSize.y));
		if(layer.tileset.tiles != null && layer.tileset.tiles.Length == tilesX * tilesY) return;
		UniTileTile [] prev = layer.tileset.tiles;
		layer.tileset.tiles = new UniTileTile[tilesX * tilesY];
		if(prev!=null) {
			for(int i=0; i<Mathf.Min(prev.Length, layer.tileset.tiles.Length); i++) {
				layer.tileset.tiles[i] = prev[i];
			}
		}
	}

}
