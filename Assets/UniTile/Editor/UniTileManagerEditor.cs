// Copyright 2011 Sven Magnus

using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor (typeof(UniTileManager))]

public class UniTileManagerEditor : Editor {
	
	private int selectedTemplate;
	
	void OnEnable() {
		UniTileManager manager = this.target as UniTileManager;
		if(manager.selectedTiles.Length==1 && !manager.tilesPicked) {
			manager.selectedTiles[0] = manager.selectedTile;
		}
	}
	
	public override void OnInspectorGUI () {
		UniTileManager manager = this.target as UniTileManager;
		
		if(this.target.name!="UniTileManager") this.target.name="UniTileManager";
		
		Rect rect = EditorGUILayout.BeginVertical();
		EditorGUILayout.EndVertical();
		
		if (GUI.Button(new Rect(10, rect.y+rect.height + 10, 100, 30), "Add tile layer")) {
			manager.layerCount++;
			GameObject g = new GameObject("Layer " + manager.layerCount);
			TileLayer tl = g.AddComponent<TileLayer>();
			if(manager.lastLayer!=null) tl.material = manager.lastLayer.material;
			// todo: add other properties
			manager.lastLayer = tl;
		}
		
		if (GUI.Button(new Rect(10 + 100, rect.y+rect.height + 10, 100, 30), "Add object layer")) {
			manager.objectLayerCount++;
			GameObject g = new GameObject("ObjectLayer " + manager.objectLayerCount);
			g.AddComponent<ObjectLayer>();
		}
		
		if(manager.templates.Length>0) {
			string[] templates = new string[manager.templates.Length + 1];
			templates[0]="Choose a template to edit";
			for(int i=0;i<manager.templates.Length;i++) {
				templates[i+1] = manager.templates[i].name;
			}
			selectedTemplate = EditorGUI.Popup(new Rect(10,rect.y+rect.height+50,rect.width-20,20), selectedTemplate, templates);
			
			if(selectedTemplate>0) {
				UniTileTemplate template = manager.templates[selectedTemplate - 1];
				template.name = EditorGUI.TextField(new Rect(10,rect.y+rect.height+70,rect.width-20,20),template.name);
				
				if (GUI.Button(new Rect(10, rect.y+rect.height + 90, rect.width - 20, 20), "Remove template \""+template.name+"\"")) {
					UniTileTemplate[] newList = new UniTileTemplate[manager.templates.Length - 1];
					int count = 0;
					for(int i=0;i<manager.templates.Length;i++) {
						if(i!=selectedTemplate - 1) {
							newList[count]=manager.templates[i];
							count++;
						}
					}
					if(manager.selectedTemplate == selectedTemplate-1) {
						manager.selectedTemplate = -1;
					}
					selectedTemplate--;
					manager.templates = newList;
				}
				
			}
		}
				
		
		
	}
	
}
