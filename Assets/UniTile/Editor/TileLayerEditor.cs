// Copyright 2011 Sven Magnus

using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor (typeof(TileLayer))]

public class TileLayerEditor : Editor {
	
	private bool drawing = false;
	private bool selecting = false;
	private bool showProperties = true;
	private Vector2 prevPos;

	Vector2 tileSizeTemp = new Vector2(-1,-1);
	Vector2 tileSpacingTemp = new Vector2(-1,-1);
	Vector2 tileUvSizeTemp = new Vector2(-1,-1);
	Vector2 overlapTemp = new Vector2(-1,-1);
	Vector2 overlapUvTemp = new Vector2(-1,-1);
	Vector2 groupSizeTemp = new Vector2(-1,-1);
	Vector2 layerSizeTemp = new Vector2(-1,-1);
	Vector2 borderSizeTemp = new Vector2(-1,-1);
	Vector2 borderSizeUvTemp = new Vector2(-1,-1);
	
	SerializedObject m_Object;
	SerializedProperty material;
	
	Vector2 scrollPos;
	
	public float prevZ;
		
	public override void OnInspectorGUI () {
		
		TileLayer layer = target as TileLayer;
		if(layer.tileSpacing == new Vector2(0, 0) || layer.tileSpacing == new Vector2(-1,-1)) layer.tileSpacing = layer.tileSize;
		if(layer == null) return;
		UniTileManager manager = UniTileManager.instance;
		if(layer == manager) return;
		
		TileLayer parent = null;
		
		if(layer.transform.parent != null) {
			parent = layer.transform.parent.GetComponent<TileLayer>();
		}
		if(parent != null) {
			if(parent != layer.parent) {
				if(EditorUtility.DisplayDialog("Warning", "Parenting a layer to another layer will combine their tile groups. The selected layer may be resized. Are you sure?", "Ok", "Cancel")) {
					layer.parent = parent;
					Vector2 prevLayerSize = layer.layerSize;
					layer.tileSize = parent.tileSize;
					if(parent.tileSpacing == new Vector2(-1,-1) || parent.tileSpacing == new Vector2(0,0)) parent.tileSpacing = parent.tileSize;
					
					layer.tileSpacing = parent.tileSpacing;
					layer.tileUvSize = parent.tileUvSize;
					layer.overlap = parent.overlap;
					layer.overlapUv = parent.overlapUv;
					layer.groupSize = parent.groupSize;
					layer.layerSize = parent.layerSize;
					layer.borderSize = parent.borderSize;
					layer.borderSizeUv = parent.borderSizeUv;
					layer.material = parent.material;
					
					tileSizeTemp = layer.tileSize;
					tileSpacingTemp = layer.tileSpacing;
					tileUvSizeTemp = layer.tileUvSize;
					overlapTemp = layer.overlap;
					overlapUvTemp = layer.overlapUv;
					groupSizeTemp = layer.groupSize;
					layerSizeTemp = layer.layerSize;
					borderSizeTemp = layer.borderSize;
					borderSizeUvTemp = layer.borderSizeUv;
					
					this.RebuildMap(prevLayerSize);
					
				} else {
					if(layer.parent != null) {
						layer.transform.parent = layer.parent.transform;
					} else {
						layer.transform.parent = null;
					}
					
				}
			} else if(!Application.isPlaying && layer.transform.position.z != prevZ) {
				this.RebuildMap(layer.layerSize);
			}
		} else {
			if(layer.parent != null) {
				this.RebuildMap(layer.parent, layer.layerSize);
				layer.parent = null;
				this.RebuildMap(layer.layerSize);
			}
		}
		
		this.prevZ = layer.transform.position.z;
		
		if(tileSizeTemp == new Vector2(-1,-1)) tileSizeTemp = layer.tileSize;
		if(tileSpacingTemp == new Vector2(-1,-1)) tileSpacingTemp = layer.tileSpacing;
		if(tileUvSizeTemp == new Vector2(-1,-1)) tileUvSizeTemp = layer.tileUvSize;
		if(overlapTemp == new Vector2(-1,-1)) overlapTemp = layer.overlap;
		if(overlapUvTemp == new Vector2(-1,-1)) overlapUvTemp = layer.overlapUv;
		if(groupSizeTemp == new Vector2(-1,-1)) groupSizeTemp = layer.groupSize;
		if(layerSizeTemp == new Vector2(-1,-1)) layerSizeTemp = layer.layerSize;
		if(borderSizeTemp == new Vector2(-1,-1)) borderSizeTemp = layer.borderSize;
		if(borderSizeUvTemp == new Vector2(-1,-1)) borderSizeUvTemp = layer.borderSizeUv;
		
		
		Rect rect = EditorGUILayout.BeginVertical();
		
		showProperties = EditorGUILayout.Foldout(showProperties, "Layer Properties");
		
		if(layer.material!=null && layer.tileset==null) {
			this.GetTileset(layer.material);
		}
		
		if(showProperties) {
			
			if(layer.parent == null) {
				
				bool uvspace = EditorGUILayout.Toggle("Use UV space", layer.useUvSpace);
				if(uvspace != layer.useUvSpace) {
					HandleUtility.Repaint ();
					layer.useUvSpace = uvspace;
					return;
				}
				
				if(layer.useUvSpace) {
					tileUvSizeTemp = EditorGUILayout.Vector2Field ("Tile Size (UV)", tileUvSizeTemp);
					tileSpacingTemp = EditorGUILayout.Vector2Field ("Tile Output Size (Units)", tileSpacingTemp);
					overlapUvTemp = EditorGUILayout.Vector2Field ("Tile Overlap (UV)", overlapUvTemp);
					borderSizeUvTemp = EditorGUILayout.Vector2Field ("Tile Border Size (UV)", borderSizeUvTemp);
					
					if(layer.material != null && layer.material.mainTexture != null) {
						tileSizeTemp = new Vector2(tileUvSizeTemp.x * layer.material.mainTexture.width, tileUvSizeTemp.y * layer.material.mainTexture.height);
						overlapTemp = new Vector2(overlapUvTemp.x * layer.material.mainTexture.width, overlapUvTemp.y * layer.material.mainTexture.height);
						borderSizeTemp = new Vector2(borderSizeUvTemp.x * layer.material.mainTexture.width, borderSizeUvTemp.y * layer.material.mainTexture.height);
					}
				} else {
					Vector2 prev = tileSizeTemp;
					tileSizeTemp = EditorGUILayout.Vector2Field ("Tile Size", tileSizeTemp);
					if(prev != tileSizeTemp && prev == tileSpacingTemp) {
						tileSpacingTemp = new Vector2(Mathf.Max(0,Mathf.Floor(tileSizeTemp.x)),Mathf.Max(0,Mathf.Floor(tileSizeTemp.y)));
					}
					tileSpacingTemp = EditorGUILayout.Vector2Field ("Tile Output Size (Units)", tileSpacingTemp);
					overlapTemp = EditorGUILayout.Vector2Field ("Tile Overlap", overlapTemp);
					borderSizeTemp = EditorGUILayout.Vector2Field ("Tile Border Size", borderSizeTemp);
					
					if(layer.material != null && layer.material.mainTexture != null) {
						tileUvSizeTemp = new Vector2(tileSizeTemp.x / layer.material.mainTexture.width, tileSizeTemp.y / layer.material.mainTexture.height);
						overlapUvTemp = new Vector2(overlapTemp.x / layer.material.mainTexture.width, overlapTemp.y / layer.material.mainTexture.height);
						borderSizeUvTemp = new Vector2(borderSizeTemp.x / layer.material.mainTexture.width, borderSizeTemp.y / layer.material.mainTexture.height);
					}
				}
				
				groupSizeTemp = EditorGUILayout.Vector2Field ("Group Size", groupSizeTemp);
				layerSizeTemp = EditorGUILayout.Vector2Field ("Layer Size", layerSizeTemp);
				
				
				m_Object.Update();
				EditorGUILayout.PropertyField(material);
				if(layer.material != material.objectReferenceValue) {
					layer.material = (Material)material.objectReferenceValue;
					m_Object.ApplyModifiedProperties();
					this.GetTileset((Material)material.objectReferenceValue);
				}
			}
			
			if(layer.material!=null) {
				int tilesX = (int)Mathf.Floor((float)layer.material.mainTexture.width / (layer.tileSize.x + layer.borderSize.x));
				int tilesY = (int)Mathf.Floor((float)layer.material.mainTexture.height / (layer.tileSize.y + layer.borderSize.y));
			
				int length=2;
				for(int i=0;i<manager.templates.Length;i++) {
					if(manager.templates[i].selectionCheck == new Vector2(tilesX, tilesY)) {
						length++;
					}
				}
			
				string[] templates = new string[length];
				templates[0]="Choose a template";
				for(int i=0;i<manager.templates.Length;i++) {
					if(manager.templates[i].selectionCheck == new Vector2(tilesX, tilesY)) {
						templates[i+1] = manager.templates[i].name;
					}
				}
				templates[length - 1]="Add current selection as a template";
				int selection = EditorGUILayout.Popup(Mathf.Min(manager.selectedTemplate + 1,length-2), templates);
				manager.selectedTemplate = selection - 1;
				if(selection == length - 1) {
					UniTileTemplate[] newList = new UniTileTemplate[manager.templates.Length + 1];
					for(int i=0;i<manager.templates.Length;i++) {
						newList[i]=manager.templates[i];
					}
					newList[newList.Length-1] = new UniTileTemplate();
					newList[newList.Length-1].Init();
					manager.templates = newList;
				} else if (selection>0) {
					manager.templates[selection - 1].Use();
				}
				manager.marker.Init();
			}
		}
		
		EditorGUILayout.EndVertical();
		
		
		if(layer.useUvSpace) {
			tileSizeTemp = new Vector2(Mathf.Max(0,tileSizeTemp.x),Mathf.Max(0,tileSizeTemp.y));
			borderSizeTemp = new Vector2(Mathf.Max(0,borderSizeTemp.x),Mathf.Max(0,borderSizeTemp.y));
		} else {
			tileSizeTemp = new Vector2(Mathf.Max(0,Mathf.Floor(tileSizeTemp.x)),Mathf.Max(0,Mathf.Floor(tileSizeTemp.y)));
			borderSizeTemp = new Vector2(Mathf.Max(0,Mathf.Floor(borderSizeTemp.x)),Mathf.Max(0,Mathf.Floor(borderSizeTemp.y)));
		}
		
		
		tileSpacingTemp = new Vector2(Mathf.Max(0,tileSpacingTemp.x),Mathf.Max(0,tileSpacingTemp.y));
		tileUvSizeTemp = new Vector2(Mathf.Max(0,tileUvSizeTemp.x),Mathf.Max(0,tileUvSizeTemp.y));
		groupSizeTemp = new Vector2(Mathf.Max(0,Mathf.Floor(groupSizeTemp.x)),Mathf.Max(0,Mathf.Floor(groupSizeTemp.y)));
		layerSizeTemp = new Vector2(Mathf.Max(0,Mathf.Floor(layerSizeTemp.x)),Mathf.Max(0,Mathf.Floor(layerSizeTemp.y)));
		
		
		
		
		if(showProperties) {
			int x = 5;
			GUILayout.BeginHorizontal();
			
			if(layer.parent == null) {
				if (GUILayout.Button(new GUIContent("Rebuild map", "Apply changes to the size properties."), GUILayout.Width(80), GUILayout.Height(30))) {
					Undo.RegisterSceneUndo("Resize map");
					Vector2 prevLayerSize = layer.layerSize;
					layer.tileSize = tileSizeTemp;
					layer.tileSpacing = tileSpacingTemp;
					layer.tileUvSize = tileUvSizeTemp;
					layer.groupSize = groupSizeTemp;
					layer.layerSize = layerSizeTemp;
					layer.borderSize = borderSizeTemp;
					layer.borderSizeUv = borderSizeUvTemp;
					layer.overlap = overlapTemp;
					layer.overlapUv = overlapUvTemp;
					
					for(int i=0; i<layer.transform.childCount; i++) {
						TileLayer child = layer.transform.GetChild(i).GetComponent<TileLayer>();
						if(child != null) {
							child.tileSize = tileSizeTemp;
							child.tileSpacing = tileSpacingTemp;
							child.tileUvSize = tileUvSizeTemp;
							child.groupSize = groupSizeTemp;
							child.layerSize = layerSizeTemp;
							child.borderSize = borderSizeTemp;
							child.borderSizeUv = borderSizeUvTemp;
							child.overlap = overlapTemp;
							child.overlapUv = overlapUvTemp;
						}
					}
					
					this.RebuildMap(prevLayerSize);
					
					manager.marker.Init();
				}
				
				x+=80;
				
				if (GUILayout.Button(new GUIContent("Revert", "Cancel changes to the size properties."), GUILayout.Width(50), GUILayout.Height(30))) {
					tileSizeTemp = layer.tileSize;
					tileSpacingTemp = layer.tileSpacing;
					tileUvSizeTemp = layer.tileUvSize;
					groupSizeTemp = layer.groupSize;
					layerSizeTemp = layer.layerSize;
					borderSizeTemp = layer.borderSize;
					borderSizeUvTemp = layer.borderSizeUv;
					overlapTemp = layer.overlap;
					overlapUvTemp = layer.overlapUv;
				}
				
				x+=50;
			}
			
			if (GUILayout.Button(new GUIContent("Clear layer", "Erase all tile data from the layer."), GUILayout.Width(70), GUILayout.Height(30))) {
				Undo.RegisterSceneUndo("Clear layer");
				this.ClearMap();
			}
			
			
			x+=70;
			
			if (GUILayout.Button(new GUIContent("Create objects", "Instantiate tile prefabs and box colliders."), GUILayout.Width(90), GUILayout.Height(30))) {
				Undo.RegisterSceneUndo("Clear layer");
				layer.InstantiatePrefabs();
			}
			
			x+=90;
			
			if(layer.parent == null && layer.material!= null && layer.material.mainTexture!=null) {
				if (GUILayout.Button(new GUIContent("Padder", "Add padding to textures."), GUILayout.Width(50), GUILayout.Height(30))) {
					PadderEditorWindow window = PadderEditorWindow.CreateInstance<PadderEditorWindow>();
					window.Setup((Texture2D)layer.material.mainTexture, layer.tileSize, layer.borderSize);
					window.ShowUtility();
				}
			}
			
			GUILayout.EndHorizontal();
		}
		
		
		
		
		if(layer.material != null) {
			
			layer.useScrollView = EditorGUILayout.Toggle("Use scrollview", layer.useScrollView);
			
			float top = rect.y + rect.height + (this.showProperties?55:22);
			
			// Hack: If we don't close the following group, we can get the panel's height by using rect2.height
			// There might be a cleaner way to achieve this but I couldn't find it...
			Rect rect2 = EditorGUILayout.BeginHorizontal();
			float height = Mathf.Max(128, rect2.height - 40);
			
			float w = Mathf.Min(rect.width,layer.material.mainTexture.width);
			float scale = w / layer.material.mainTexture.width;
			float h = layer.material.mainTexture.height * scale;
			if(h>height) {
				h = height;
				scale = h / layer.material.mainTexture.height;
				w = layer.material.mainTexture.width * scale;
			}
			
			
			int tilesX = (int)Mathf.Floor((float)layer.material.mainTexture.width / (layer.tileSize.x + layer.borderSize.x));
			
			int tileX1 = (int)Mathf.Floor((float)manager.selectedTile % (float)tilesX);
			int tileY1 = (int)Mathf.Floor((float)manager.selectedTile / (float)tilesX);
			
			int tileX2 = (int)Mathf.Floor((float)manager.selectedTileEnd % (float)tilesX);
			int tileY2 = (int)Mathf.Floor((float)manager.selectedTileEnd / (float)tilesX);
			
			int tileXMin=Mathf.Min(tileX1, tileX2);
			int tileYMin=Mathf.Min(tileY1, tileY2);
			int tileXMax=Mathf.Max(tileX1, tileX2);
			int tileYMax=Mathf.Max(tileY1, tileY2);
			
			int tileWidth = tileXMax - tileXMin + 1;
			int tileHeight = tileYMax - tileYMin + 1;
			
			float tempTop = top;
			
			if(layer.useScrollView) {
				scale = 1;
				w = layer.material.mainTexture.width;
				h = layer.material.mainTexture.height;
				
				
				GUILayout.BeginArea(new Rect (10,10,100,100));
				
				GUILayout.EndArea ();
				scrollPos = EditorGUILayout.BeginScrollView(scrollPos, false, false, GUILayout.Width(Screen.width));
				GUILayout.Label("", GUILayout.Width(w), GUILayout.Height(h));
				tempTop = 0;
				
			}
			
			GUI.DrawTexture(new Rect(0,tempTop,w,h), layer.material.mainTexture);
			
			
			if(manager.selectedTile>=0 && !manager.tilesPicked) {
				GUIStyle style = new GUIStyle(GUI.skin.customStyles[0]);
				style.normal.background=manager.selectionTexture;
				GUI.Box(
					new Rect(
						(tileXMin * (layer.tileSize.x + layer.borderSize.x * 2f) + layer.borderSize.x) * scale,
						tempTop + (tileYMin * (layer.tileSize.y + layer.borderSize.y * 2f) + layer.borderSize.y) * scale,
						(tileWidth * (layer.tileSize.x + layer.borderSize.x * 2f) - layer.borderSize.x * 2f) * scale,
						(tileHeight * (layer.tileSize.y + layer.borderSize.y * 2f) - layer.borderSize.y * 2f) * scale
					),
					"",
					style
				);
			}
			
			if(layer.useScrollView) {
				EditorGUILayout.EndScrollView();
			}
			
			
			if(Event.current!=null) {
				if(Event.current.type==EventType.MouseDown) {
					if(Event.current.button==0) {
						Vector2 pos = Event.current.mousePosition;
						if(layer.useScrollView) pos += scrollPos;
						this.selecting=true;
						manager.tilesPicked = false;
						
						if(pos.x>=0 && pos.x<=layer.material.mainTexture.width * scale && pos.y>=top && pos.y<=top+layer.material.mainTexture.height * scale) {
							manager.selectedTemplate = - 1;
							manager.selectedTile = manager.selectedTileEnd = (int)Mathf.Floor((pos.x / scale) /(layer.tileSize.x + layer.borderSize.x * 2f)) + (int)(Mathf.Floor((pos.y - top)/(scale * (layer.tileSize.y + layer.borderSize.y * 2f))) * tilesX);
							if(Event.current.clickCount==2) {
								TileEditorWindow window = TileEditorWindow.CreateInstance<TileEditorWindow>();
								window.Setup(this, manager.selectedTile);
								window.ShowUtility();
							}
						}
					} else {
						manager.tilesPicked = false;
						manager.selectedTile = -1;
						manager.selectedTileEnd = -1;
					}
				}
				if(Event.current.type==EventType.mouseDrag) {
					if(selecting) {
						Vector2 pos = Event.current.mousePosition;
						if(layer.useScrollView) pos += scrollPos;
						if(pos.x>=0 && pos.x<=layer.material.mainTexture.width * scale && pos.y>=top && pos.y<=top+layer.material.mainTexture.height * scale) {
							manager.selectedTileEnd = (int)Mathf.Floor((pos.x / scale) /(layer.tileSize.x + layer.borderSize.x * 2f)) + (int)(Mathf.Floor((pos.y - top)/(scale * (layer.tileSize.y + layer.borderSize.y * 2f))) * tilesX);
							
							manager.tilesPicked = false;
							manager.selectedTilesWidth = tileXMax - tileXMin + 1;
							manager.selectedTiles = new int[(tileXMax - tileXMin + 1) * (tileYMax - tileYMin + 1)];
							HandleUtility.Repaint ();
							if(manager.selectedTile>=0) {
								for(int i = tileXMin; i<= tileXMax; i++) {
									for(int j = tileYMin; j<= tileYMax; j++) {
										int x = i-tileXMin;
										int y = j-tileYMin;
										manager.selectedTiles[x + y * manager.selectedTilesWidth] = i + j * tilesX;
									}
								}
								
							} else {
								manager.selectedTiles[0]=-1;
							}
							manager.marker.Init();
							
						}
					}
				}
				if(Event.current.type==EventType.MouseUp) {
					manager.tilesPicked = false;
					manager.selectedTilesWidth = tileXMax - tileXMin + 1;
					manager.selectedTiles = new int[(tileXMax - tileXMin + 1) * (tileYMax - tileYMin + 1)];
					HandleUtility.Repaint ();
					if(manager.selectedTile>=0) {
						for(int i = tileXMin; i<= tileXMax; i++) {
							for(int j = tileYMin; j<= tileYMax; j++) {
								int x = i-tileXMin;
								int y = j-tileYMin;
								manager.selectedTiles[x + y * manager.selectedTilesWidth] = i + j * tilesX;
							}
						}
						
					} else {
						manager.selectedTiles[0]=-1;
					}
					manager.marker.Init();
					
					
					this.selecting=false;
				}
			}
			
		}
		
		if (GUI.changed) {
            EditorUtility.SetDirty (target);
		}
		
    }
	
	void OnEnable() {
		UniTileManager manager = UniTileManager.Reset();
		TileLayer layer = target as TileLayer;
		if(layer == null) return;
		
		if(layer.useUvSpace) {
			if(layer.material != null && layer.material.mainTexture != null) {
				tileSizeTemp = new Vector2(layer.tileUvSize.x * layer.material.mainTexture.width, layer.tileUvSize.y * layer.material.mainTexture.height);
				layer.tileSize = tileSizeTemp;
				
				overlapTemp = new Vector2(layer.overlapUv.x * layer.material.mainTexture.width, layer.overlapUv.y * layer.material.mainTexture.height);
				layer.overlap = overlapTemp;
				
				borderSizeTemp = new Vector2(layer.borderSizeUv.x * layer.material.mainTexture.width, layer.borderSizeUv.y * layer.material.mainTexture.height);
				layer.borderSize = borderSizeTemp;
			}
		}
		
		m_Object = new SerializedObject (target);
		material = m_Object.FindProperty ("material");
		//this.GetTileset(layer.material);
		
		//layer.transform.position = new Vector3(0,0,layer.transform.position.z);
		
		
		if(manager.selectionTexture==null) {
			manager.selectionTexture = new Texture2D(1,1);
			manager.selectionTexture.SetPixel(0,0,new Color(0.75f,0.75f,0.25f,0.4f));
			manager.selectionTexture.Apply();
		}
		
		//this.texture.filterMode = FilterMode.Point;
		
		
		if(layer.material == null) {
			manager.selectedTilesWidth = 1;
			manager.selectedTiles = new int[1];
			manager.selectedTiles[0]=0;
		} else {
		
			int tilesX = (int)Mathf.Floor((float)layer.material.mainTexture.width / (layer.tileSize.x + layer.borderSize.x));
			int tilesY = (int)Mathf.Floor((float)layer.material.mainTexture.height / (layer.tileSize.y + layer.borderSize.y));
			
			if(!manager.tilesPicked) {
				if(manager.selectionCheck!=new Vector2(tilesX, tilesY)) {
					manager.selectionCheck = new Vector2(tilesX, tilesY);
					manager.selectedTilesWidth = 1;
					manager.selectedTiles = new int[1];
					manager.selectedTiles[0]=0;
				} else {
				
					int tileX1 = (int)Mathf.Floor((float)manager.selectedTile % (float)tilesX);
					int tileY1 = (int)Mathf.Floor((float)manager.selectedTile / (float)tilesX);
					
					int tileX2 = (int)Mathf.Floor((float)manager.selectedTileEnd % (float)tilesX);
					int tileY2 = (int)Mathf.Floor((float)manager.selectedTileEnd / (float)tilesX);
					
					int tileXMin=Mathf.Min(tileX1, tileX2);
					int tileYMin=Mathf.Min(tileY1, tileY2);
					int tileXMax=Mathf.Max(tileX1, tileX2);
					int tileYMax=Mathf.Max(tileY1, tileY2);
					
					manager.selectedTilesWidth = tileXMax - tileXMin + 1;
					manager.selectedTiles = new int[(tileXMax - tileXMin + 1) * (tileYMax - tileYMin + 1)];
					if(manager.selectedTile>=0) {
						
						for(int i = tileXMin; i<= tileXMax; i++) {
							for(int j = tileYMin; j<= tileYMax; j++) {
								int x = i-tileXMin;
								int y = j-tileYMin;
								manager.selectedTiles[x + y * manager.selectedTilesWidth] = i + j * tilesX;
							}
						}
						
					} else {
						manager.selectedTiles[0]=-1;
					}
				}
			} else {
				if(manager.selectionCheck!=new Vector2(tilesX, tilesY)) {
					manager.selectionCheck = new Vector2(tilesX, tilesY);
					manager.tilesPicked = false;
					manager.selectedTilesWidth = 1;
					manager.selectedTiles = new int[1];
					manager.selectedTiles[0]=0;
				}
			}
		}
		manager.activeLayer = layer;
		manager.lastLayer = layer;
		manager.marker.Init();
	}
	
	void OnDisable() {
		//DestroyImmediate(this.texture);
		if(UniTileManager.instance.marker!=null) {
			DestroyImmediate(UniTileManager.instance.marker.meshFilter.sharedMesh);
			DestroyImmediate(UniTileManager.instance.marker.gameObject);
		}
		UniTileManager.instance.activeLayer = null;
		
	}
	
	
	void OnSceneGUI() {
		TileLayer layer = target as TileLayer;
		if(layer == null) return;
		UniTileManager manager = UniTileManager.instance;
		if(manager == null) return;
		
		if(layer.tileSpacing == new Vector2(-1, -1) || layer.tileSpacing == new Vector2(0, 0)) layer.tileSpacing = layer.tileSize;
		
		if(EditorWindow.mouseOverWindow) {
			
			//if(Event.current.type!=EventType.repaint && Event.current.type!=EventType.layout)  Debug.Log(Event.current);
			Vector3 pos = GetCoordinates();
			UniTileManager.mousePosition = manager.marker.transform.localPosition = new Vector3(Mathf.Floor(pos.x/layer.tileSpacing.x) * layer.tileSpacing.x, Mathf.Floor(pos.y/layer.tileSpacing.y) * layer.tileSpacing.y, pos.z - 1);
			//UniTileManager.mousePosition = new Vector3(Mathf.Floor(pos.x/layer.tileSize.x) * layer.tileSize.x, Mathf.Floor(pos.y/layer.tileSize.y) * layer.tileSize.y, pos.z - 1);
			HandleUtility.Repaint ();
			
			if(Event.current.type==EventType.mouseDown) {
				if(Event.current.button==0) {
					prevPos=new Vector2(-1,-1);
					if(Event.current.modifiers.ToString()=="Alt") {
						manager.marker.gameObject.active = false;
						Vector3 coords = GetCoordinates();
						manager.pickStart = new Vector2(coords.x, coords.y);
						manager.pickStart.x = Mathf.Floor(coords.x / layer.tileSpacing.x);
						manager.pickStart.y = Mathf.Floor(coords.y / layer.tileSpacing.y);
						
						
						if(manager.pickStart.x>=layer.layerSize.x - 1) manager.pickStart.x=layer.layerSize.x - 1;
						if(manager.pickStart.y>=layer.layerSize.y - 1) manager.pickStart.y=layer.layerSize.y - 1;
						if(manager.pickStart.x<0) manager.pickStart.x=0;
						if(manager.pickStart.y<0) manager.pickStart.y=0;
						
						manager.picking = true;
						this.Pick();
					} else {
						this.drawing = true;
						this.Draw();
					}
				}
			}
			if(Event.current.type==EventType.mouseMove) {
				if(this.drawing) this.Draw();
				if(manager.picking) this.Pick();
				
				
			}
			if(Event.current.type==EventType.dragPerform || Event.current.type==EventType.DragExited || Event.current.type==EventType.dragUpdated || Event.current.type==EventType.mouseDrag) {
				if(this.drawing) {
					this.Draw();
					Event.current.Use();
				}
				if(manager.picking) {
					this.Pick();
					Event.current.Use();
				}
			}
			if(Event.current.type==EventType.mouseUp || Event.current.type==EventType.ignore) {
				this.drawing = false;
				if(manager.picking) {
					
					if(layer.tiles==null || layer.tiles.Length!=(int)layer.layerSize.x * (int)layer.layerSize.y) {
						layer.tiles = new int[(int)layer.layerSize.x * (int)layer.layerSize.y];
						
						for(int i=0;i<layer.tiles.Length;i++) {
							layer.tiles[i] = -1;
						}
					}
					
					this.Pick();
					int xMin=(int)Mathf.Min(manager.pickStart.x, manager.pickEnd.x);
					int yMin=(int)Mathf.Min(manager.pickStart.y, manager.pickEnd.y);
					int xMax=(int)Mathf.Max(manager.pickStart.x, manager.pickEnd.x);
					int yMax=(int)Mathf.Max(manager.pickStart.y, manager.pickEnd.y);
					
					
					manager.selectedTiles = new int[(xMax - xMin + 1) * (yMax - yMin + 1)];
					manager.selectedTilesWidth = (xMax - xMin + 1);
					for(int i=xMin;i<=xMax;i++) {
						for(int j=yMin;j<=yMax;j++) {
							int x = i-xMin;
							int y = (yMax - yMin) - (j-yMin);
							manager.selectedTiles[x + y * manager.selectedTilesWidth] = layer.tiles[i+j*(int)layer.layerSize.x];
						}
					}
					
					if(manager.selectedTiles.Length==1) {
						manager.selectedTile = manager.selectedTileEnd = manager.selectedTiles[0];
						manager.tilesPicked = false;
					} else {
						manager.selectedTile = manager.selectedTileEnd = manager.selectedTiles[0];
						manager.tilesPicked = true;
					}
					
					manager.marker.Init();
					
					manager.marker.gameObject.active = true;
					manager.picking = false;
				}
			}
			if(Event.current.type==EventType.scrollWheel && Event.current.modifiers.ToString()=="Alt") {
				if(Event.current.delta.y < 0) {
					manager.selectedTile--;
					if(manager.selectedTile<-1) manager.selectedTile = -1;
				}
				if(Event.current.delta.y > 0) {
					manager.selectedTile++;
					int tilesX = (int)Mathf.Floor((float)layer.material.mainTexture.width / (layer.tileSize.x + layer.borderSize.x));
					int tilesY = (int)Mathf.Floor((float)layer.material.mainTexture.height / (layer.tileSize.y + layer.borderSize.y));
					if(manager.selectedTile>=tilesX*tilesY - 1) manager.selectedTile = tilesX*tilesY - 1;
				}
				
				
				manager.selectedTileEnd=manager.selectedTile;
				manager.tilesPicked = false;
				
				manager.selectedTiles = new int[1];
				manager.selectedTiles[0]=manager.selectedTile;
				manager.marker.Init();
				
				HandleUtility.Repaint();
				this.Repaint();
				Event.current.Use();
			}
		}
		
	}
	
	
	private void Pick() {
		TileLayer layer = target as TileLayer;
		if(layer == null) return;
		UniTileManager manager = UniTileManager.instance;
		if(layer == manager) return;
		
		manager.selectedTemplate = - 1;
		
		Vector3 coords = GetCoordinates();
		manager.pickEnd = new Vector2();
		manager.pickEnd.x = Mathf.Floor(coords.x / layer.tileSpacing.x);
		manager.pickEnd.y = Mathf.Floor(coords.y / layer.tileSpacing.y);
		if(manager.pickEnd.x>=layer.layerSize.x - 1) manager.pickEnd.x=layer.layerSize.x - 1;
		if(manager.pickEnd.y>=layer.layerSize.y - 1) manager.pickEnd.y=layer.layerSize.y - 1;
		if(manager.pickEnd.x<0) manager.pickEnd.x=0;
		if(manager.pickEnd.y<0) manager.pickEnd.y=0;
	}
	
	private Vector3 GetCoordinates() {
		Plane p = new Plane((this.target as MonoBehaviour).transform.TransformDirection(Vector3.forward), (this.target as MonoBehaviour).transform.position);
		Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
		
        Vector3 hit = new Vector3();
        float dist;
		
		if (p.Raycast(ray, out dist))
        	hit = ray.origin + ray.direction.normalized * dist;
		
		return (this.target as MonoBehaviour).transform.InverseTransformPoint(hit);
	}
	
	private void Draw() {
		TileLayer layer = target as TileLayer;
		if(layer == null) return;
		UniTileManager manager = UniTileManager.instance;
		if(layer == manager) return;
		
		Vector3 pos = GetCoordinates();
		UniTileManager.mousePosition = new Vector3(Mathf.Floor(pos.x/layer.tileSpacing.x) * layer.tileSpacing.x, Mathf.Floor(pos.y/layer.tileSpacing.y) * layer.tileSpacing.y, pos.z - 1);
		HandleUtility.Repaint();
		Undo.RegisterSceneUndo("Draw tile");
		
		
		Vector2 gridPos = new Vector2();
		gridPos.x = Mathf.Floor(pos.x / layer.tileSpacing.x);
		gridPos.y = Mathf.Floor(pos.y / layer.tileSpacing.y);
		
		if(gridPos!=prevPos) {
			
			prevPos = gridPos;
			
			//if(manager.selectedTile == -1) {
			//	this.DrawTile(-1, gridPos);
			//} else {
				
				for(int i=0;i<manager.selectedTiles.Length;i++) {
					int x=i%manager.selectedTilesWidth;
					int y=(int)Mathf.Floor((float)i/(float)manager.selectedTilesWidth);
					this.DrawTile(Event.current.modifiers.ToString() == "Control"?-1:manager.selectedTiles[i], new Vector2(gridPos.x+x, gridPos.y-y));
				}
				
			//}
			
		}
		
	}
	
	private void DrawTile(int tile, Vector2 pos) {
		TileLayer layer = target as TileLayer;
		if(layer == null) return;
		if(layer.material == null) return;
		
		if(pos.x<0 || pos.y<0 || pos.x>=layer.layerSize.x || pos.y>=layer.layerSize.y) return;
		
		if(layer.tiles==null || layer.tiles.Length!=(int)layer.layerSize.x * (int)layer.layerSize.y) {
			layer.tiles = new int[(int)layer.layerSize.x * (int)layer.layerSize.y];
			
			for(int i=0;i<layer.tiles.Length;i++) {
				layer.tiles[i] = -1;
			}
		}
		layer.tiles[(int)pos.x + (int)pos.y * (int)layer.layerSize.x] = tile;
		
		int groupX = (int)Mathf.Floor((float)pos.x/(float)layer.groupSize.x);
		int groupY = (int)Mathf.Floor((float)pos.y/(float)layer.groupSize.y);
		
		this.RedrawGroup(groupX, groupY);
		
	}
	
	private void RedrawGroup(int groupX, int groupY) {
		TileLayer layer = target as TileLayer;
		if(layer == null) return;
		if(layer.parent != null) layer = layer.parent;
		
		GameObject g;
		Transform t = layer.transform.FindChild("group_"+groupX+"_"+groupY);
		Mesh m;
		if(t==null) {
			g=new GameObject("group_"+groupX+"_"+groupY);
			t=g.transform;
			t.transform.parent=layer.transform;
			t.localPosition=new Vector3(groupX * layer.tileSpacing.x * layer.groupSize.x, groupY * layer.tileSpacing.y * layer.groupSize.y);
			t.localScale = new Vector3(1, 1, 1);
			Quaternion quaternion = t.localRotation;
			quaternion.eulerAngles = new Vector3(0, 0, 0);
			t.localRotation = quaternion;
			MeshRenderer r = g.AddComponent<MeshRenderer>();
			MeshFilter f = g.AddComponent<MeshFilter>();
			m = f.sharedMesh = new Mesh();
			r.material = layer.material;
		} else {
			g = t.gameObject;
			MeshFilter f = t.GetComponent<MeshFilter>();
			DestroyImmediate(f.sharedMesh);
			m = f.sharedMesh = new Mesh();
		}
		
		//ArrayList temp = new ArrayList();
		List<Hashtable> temp = new List<Hashtable>();
		
		List<TileLayer> layers = new List<TileLayer>();
		layers.Add(layer);
		for(int i=0; i<layer.transform.childCount; i++) {
			TileLayer child = layer.transform.GetChild(i).GetComponent<TileLayer>();
			if(child != null) {
				layers.Add(child);
			}
		}
		
		layers.Sort(delegate(TileLayer l1, TileLayer l2) {
			return (l2==layer?0f:l2.transform.localPosition.z).CompareTo(l1==layer?0f:l1.transform.localPosition.z);
		});
		
		for(int c=0; c<layers.Count; c++) {
			TileLayer lyr = layers[c];
			Hashtable skip = new Hashtable();
			for(int i=groupX*(int)lyr.groupSize.x;i<Mathf.Min(lyr.layerSize.x, groupX*(int)lyr.groupSize.x+lyr.groupSize.x);i++) {
				for(int j=groupY*(int)lyr.groupSize.y;j<Mathf.Min(lyr.layerSize.y, groupY*(int)lyr.groupSize.y+lyr.groupSize.y);j++) {
					UniTileTile tile = lyr.GetTile(i, j);
					int id = lyr.GetTileId(i, j);
					if(id!=-1) {
						
						if(skip[new Vector2(i, j)] == null) {
							Hashtable h=new Hashtable();
							h["x"]=i;
							h["y"]=j;
							h["z"]=(lyr==layer?0:lyr.transform.localPosition.z);
							h["tile"]=id;
						
							int sizeX = 1;
							int sizeY = 1;
							
							if(tile!=null && tile.resizable) {
								while(skip[new Vector2(i, j + sizeY)]==null && lyr.GetTileId(i, j + sizeY) == id && j+sizeY<(groupY + 1) * lyr.groupSize.y) {
									skip[new Vector2(i, j + sizeY)] = true;
									sizeY++;
								}
								
								if(sizeY == 1) {
									while(skip[new Vector2(i + sizeX, j)]==null && lyr.GetTileId(i + sizeX, j) == id && i+sizeX<(groupX + 1) * lyr.groupSize.x) {
										skip[new Vector2(i + sizeX, j)] = true;
										sizeX++;
									}
								} else {
									sizeX = 1;
									bool columnOk = true;
									int column = 1;
									while(columnOk && i+column<(groupX + 1) * lyr.groupSize.x) {
										columnOk = true;
										for(int k=0;k<sizeY;k++) {
											if(skip[new Vector2(i + column, j + k)]!=null || lyr.GetTileId(i + column, j + k) != id) {
												columnOk = false;
												break;
											}
										}
										if(columnOk) {
											column++;
										}
									}
									sizeX = column;
									for(int k=0;k<sizeX;k++) {
										for(int l=0;l<sizeY;l++) {
											skip[new Vector2(i + k, j + l)] = true;
										}
									}
								}
								
								
							}
							
							h["sizeX"] = sizeX;
							h["sizeY"] = sizeY;
						
							temp.Add(h);
						}
					}
				}
			}
		}
		
		
		
		if(temp.Count==0) {
			DestroyImmediate(m);
			DestroyImmediate(g);
		} else {
		
			Vector3 [] vertices = new Vector3[4 * temp.Count];
			int [] triangles = new int[6 * temp.Count];
			Vector2 [] uv = new Vector2[4 * temp.Count];
			
			for(int i=0;i<temp.Count;i++) {
				Hashtable h=temp[i] as Hashtable;
				int x = (int)((int)h["x"]*layer.tileSpacing.x) - (int)(groupX * layer.tileSpacing.x * layer.groupSize.x);
				int y = (int)((int)h["y"]*layer.tileSpacing.y) - (int)(groupY * layer.tileSpacing.y * layer.groupSize.y);
				int sizeX = (int)h["sizeX"];
				int sizeY = (int)h["sizeY"];
				
				Rect vRect = new Rect();
				Rect uvRect = new Rect();
				
				int columns = (int)(layer.material.mainTexture.width / (layer.tileSize.x + layer.borderSize.x * 2f));
				
				if(sizeX == 1 && sizeY == 1) {
					vRect.x = (int)x - layer.overlap.x;
					vRect.y = (int)y - layer.overlap.y;
					vRect.xMax = layer.tileSpacing.x + (int)x  + layer.overlap.x;
					vRect.yMax = layer.tileSpacing.y + (int)y + layer.overlap.y;
					
					uvRect.x = Mathf.Floor((int)h["tile"] % columns) * (layer.tileSize.x + layer.borderSize.x * 2f) + layer.borderSize.x - layer.overlap.x;
					uvRect.y = layer.material.mainTexture.height - (Mathf.Floor((int)h["tile"] / columns) * (layer.tileSize.x + layer.borderSize.y * 2f) + layer.borderSize.y + layer.overlap.y + layer.tileSize.y);
					uvRect.width = layer.tileSize.x + layer.overlap.x;
					uvRect.height = layer.tileSize.y + layer.overlap.y;
					
					
					Texture2D texture = (Texture2D)layer.material.mainTexture;
					TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(AssetDatabase.GetAssetPath(texture));
					if(importer.isReadable && (importer.textureFormat == TextureImporterFormat.ARGB32 || importer.textureFormat == TextureImporterFormat.RGB24)) {
						
						// Find empty columns left
						int start = Mathf.Max(0, (int)Mathf.Round(uvRect.x));
						int end = Mathf.Min(texture.width, (int)Mathf.Round(uvRect.xMax));
						for(int j=start; j<end; j++) {
							int min = Mathf.Max(0, (int)Mathf.Round(uvRect.y));
							int max = Mathf.Min(texture.height - min, (int)Mathf.Round(uvRect.height));
							Color[] pixels = texture.GetPixels(j, min, 1, max);
							bool allTrans = true;
							for(int k=0;k<pixels.Length;k++) {
								if(pixels[k].a != 0) {
									allTrans = false;
									break;
								}
							}
							if(!allTrans) break;
							else {
								uvRect.xMin++;
								vRect.xMin+=layer.tileSpacing.x / layer.tileSize.x;
							}
						}
						
						// Find empty columns right
						start = Mathf.Min(texture.width, (int)Mathf.Round(uvRect.xMax)) - 1;
						end = Mathf.Max(0, (int)Mathf.Round(uvRect.x));
						for(int j=start; j>=end; j--) {
							int min = Mathf.Max(0, (int)Mathf.Round(uvRect.y));
							int max = Mathf.Min(texture.height - min, (int)Mathf.Round(uvRect.height));
							Color[] pixels = texture.GetPixels(j, min, 1, max);
							bool allTrans = true;
							for(int k=0;k<pixels.Length;k++) {
								if(pixels[k].a != 0) {
									allTrans = false;
									break;
								}
							}
							if(!allTrans) break;
							else {
								uvRect.xMax--;
								vRect.xMax-=layer.tileSpacing.x / layer.tileSize.x;
							}
						}
					
						// Find empty rows down
						start = Mathf.Max(0, (int)Mathf.Round(uvRect.y));
						end = Mathf.Min(texture.height, (int)Mathf.Round(uvRect.yMax));
						for(int j=start; j<end; j++) {
							int min = Mathf.Max(0, (int)Mathf.Round(uvRect.x));
							int max = Mathf.Min(texture.width - min, (int)Mathf.Round(uvRect.width));
							Color[] pixels = texture.GetPixels(min, j, max, 1);
							bool allTrans = true;
							for(int k=0;k<pixels.Length;k++) {
								if(pixels[k].a != 0) {
									allTrans = false;
									break;
								}
							}
							if(!allTrans) break;
							else {
								uvRect.yMin++;
								vRect.yMin+=layer.tileSpacing.y / layer.tileSize.y;
							}
						}
					
						// Find empty rows up
						start = Mathf.Min(texture.height, (int)Mathf.Round(uvRect.yMax)) - 1;
						end = Mathf.Max(0, (int)Mathf.Round(uvRect.y));
						
						for(int j=start; j>=end; j--) {
							int min = Mathf.Max(0, (int)Mathf.Round(uvRect.x));
							int max = Mathf.Min(texture.width - min, (int)Mathf.Round(uvRect.width));
							Color[] pixels = texture.GetPixels(min, j, max, 1);
							bool allTrans = true;
							for(int k=0;k<pixels.Length;k++) {
								if(pixels[k].a != 0) {
									allTrans = false;
									break;
								}
							}
							if(!allTrans) break;
							else {
								uvRect.yMax--;
								vRect.yMax-=layer.tileSpacing.y / layer.tileSize.y;
							}
						}
					}
					
				} else {
					vRect.x = (int)x - layer.overlap.x;
					vRect.y = (int)y - layer.overlap.y;
					vRect.xMax = layer.tileSpacing.x * (float)sizeX + (int)x  + layer.overlap.x;
					vRect.yMax = layer.tileSpacing.y * (float)sizeY + (int)y + layer.overlap.y;
					
					//vRect.xMax = layer.tileSize.x * 1 + (int)x  + layer.overlap.x;
					//vRect.yMax = layer.tileSize.y * 1 + (int)y + layer.overlap.y;
					
					uvRect.x = Mathf.Floor((int)h["tile"] % columns) * (layer.tileSize.x + layer.borderSize.x * 2f) + (layer.borderSize.x - layer.overlap.x) / sizeX;
					uvRect.y = layer.material.mainTexture.height - (Mathf.Floor((int)h["tile"] / columns) * (layer.tileSize.x + layer.borderSize.y * 2f) + (layer.borderSize.y + layer.overlap.y) / sizeY + layer.tileSize.y);
					uvRect.width = layer.tileSize.x + layer.overlap.x;
					uvRect.height = layer.tileSize.y + layer.overlap.y;
				}
				
				
				
				
				vertices[(i*4) + 0] = new Vector3(vRect.x, vRect.yMax, (float)h["z"]);
				vertices[(i*4) + 1] = new Vector3(vRect.xMax, vRect.yMax, (float)h["z"]);
				vertices[(i*4) + 2] = new Vector3(vRect.x, vRect.y, (float)h["z"]);
				vertices[(i*4) + 3] = new Vector3(vRect.xMax, vRect.y, (float)h["z"]);
				
				// Triangles
				triangles[(i*6) + 0] = (i*4) + 0;
				triangles[(i*6) + 1] = (i*4) + 1;
				triangles[(i*6) + 2] = (i*4) + 2;
				triangles[(i*6) + 3] = (i*4) + 1;
				triangles[(i*6) + 4] = (i*4) + 3;
				triangles[(i*6) + 5] = (i*4) + 2;
				
				// UVS
				uv[(i*4) + 0] = new Vector2(uvRect.x / (float)layer.material.mainTexture.width, uvRect.yMax / (float)layer.material.mainTexture.height);
				uv[(i*4) + 1] = new Vector2(uvRect.xMax / (float)layer.material.mainTexture.width, uvRect.yMax / (float)layer.material.mainTexture.height);
				uv[(i*4) + 2] = new Vector2(uvRect.x / (float)layer.material.mainTexture.width, uvRect.y / (float)layer.material.mainTexture.height);
				uv[(i*4) + 3] = new Vector2(uvRect.xMax / (float)layer.material.mainTexture.width, uvRect.y / (float)layer.material.mainTexture.height);
				
				
			}
			
			m.vertices = vertices;
			m.triangles = triangles;
			m.uv = uv;
			
			m.RecalculateNormals();
		}
	}
	
	private void ClearMap() {
		TileLayer layer = target as TileLayer;
		if(layer == null) return;
		layer.tiles=null;
		this.ClearGroups(layer);
	}
	
	private void RebuildMap(Vector2 prevLayerSize) {
		RebuildMap(target as TileLayer, prevLayerSize);
	}
	
	private void RebuildMap(TileLayer layer, Vector2 prevLayerSize) {
		
		if(layer == null) return;
		this.ClearGroups(layer);
		
		int [] newTiles = new int[(int)layer.layerSize.x * (int)layer.layerSize.y];
		
		for(int i=0;i<layer.layerSize.x;i++) {
			for(int j=0;j<layer.layerSize.y;j++) {
				newTiles[i+j*(int)layer.layerSize.x]=-1;
				if(layer.tiles!=null && layer.tiles.Length>0) {
					if(i<prevLayerSize.x) {
						if(j<prevLayerSize.y) {
							newTiles[i+j*(int)layer.layerSize.x]=layer.tiles[i+j*(int)prevLayerSize.x];
						}
					}
				}
				
			}
		}
		
		layer.tiles=newTiles;
		
		for(int i=0;i<=Mathf.Floor(layer.layerSize.x/layer.groupSize.x);i++) {
			for(int j=0;j<=Mathf.Floor(layer.layerSize.y/layer.groupSize.y);j++) {
				this.RedrawGroup(i,j);
			}
		}
		
	}
	
	private void ClearGroups(TileLayer layer) {
		if(layer == null) return;
		for(int i=0;i<layer.transform.childCount;i++) {
			
			TileLayer child = layer.transform.GetChild(i).GetComponent<TileLayer>();
			if(child != null) {
				ClearGroups(child);
			}
			
			Transform t = layer.transform.GetChild(i);
			if(t.gameObject.name.Substring(0,6)=="group_") {
				MeshFilter f = t.gameObject.GetComponent<MeshFilter>();
				if(f!=null && f.sharedMesh!=null) DestroyImmediate(f.sharedMesh);
				DestroyImmediate(t.gameObject);
				i--;
			}
			
		}
	}
	
	private void GetTileset(Material mat) {
		TileLayer layer = target as TileLayer;
		if(mat == null) {
			layer.tileset = null;
			return;
		}
		string matPath = AssetDatabase.GetAssetPath(mat);
		string tsPath = matPath.Substring(0, matPath.Length-4) + ".Tileset.prefab";
		layer.tileset = (UniTileTileset)AssetDatabase.LoadAssetAtPath(tsPath, typeof(UniTileTileset));
		if(layer.tileset == null) {
			Object prefab = EditorUtility.CreateEmptyPrefab(tsPath);
			GameObject goTemp = new GameObject();
			GameObject go = EditorUtility.ReplacePrefab(goTemp, prefab);
			DestroyImmediate(goTemp);
			layer.tileset = go.AddComponent<UniTileTileset>();
		}
	}
	
	
}
