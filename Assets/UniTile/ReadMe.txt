UniTile
=======
Copyright Sven Magnus 2011 - http://www.mudloop.com/
Included shader by Jessy VanDivner.
Included tileset by Jose Antonio Borba Bandera - http://www.el-sato.com/
The tileset can not be used in your own projects without written permission.

Thank you for purchasing UniTile, a tile-based 2D map editor for Unity.

1. Getting started
==================

It is a good idea to experiment with the included demo-scene before you start working on your own maps.
After you open the demo scene, you should set the current viewport's camera to "Back" with Unity's camera widget at the top right of the scene.


Drawing
-------
IMPORTANT: Always use Unity's Hand tool when drawing maps. It's at the top left of the screen, and it can also be activated by pressing Q.

Select any of the layer GameObjects. You'll see a tileset in the inspector, where you can select one or multiple tiles.
Once you've selected a tile, you can start drawing in the map.


Picking
-------
You can pick one or multiple tiles from your map by holding Alt and then clicking anywhere on the map, or clicking and dragging.


Creating a new map
------------------
Start by creating an empty scene. Then drag the UniTileManager prefab from the UniTile/Prefabs folder into your scene.
The UniTile manager has the EditorOnly tag so it will only exist in the editor, not in your builds.
In the inspector you'll see a button labeled "Add new layer". Press it to add a new layer to the scene, then select it in the Hierarchy panel.
Assign the material property of the new layer, and set the size of the layer and the individual tiles. If your tiles have borders around them, set the correct values as well. The default group size should be ok for most cases. Click "Apply".
Make sure you have the Hand tool selected, and you should be able to start drawing your map!

If you have multiple layers in your map, you should put them at different z-positions using the Inspector's Transform panel.

The tileset will be scaled to fit within the Inspector. If it's too small, try resizing the inspector, or collapsing the Transform and Properties groups.


Templates
---------
You can save the currently selected tiles (whether you selected them straight from the tileset or picked them from the map) as a template.
To do this, you need to have a layer selected, and open the "Choose a template" popup box in the inspector. Select the "Add current selection as a template" option.
Once you've added one or more templates, you can select them from this list.

If you want to rename or remove templates, you can do so by selecting the UniTileManager object in the Hierarchy panel. It should have a popup box that sais "Choose a template to edit".
Select one, and a textfield and a button will appear for renaming and deleting the template.


2. More information
===================

UniTile works by dynamically creating new GameObjects, (tile groups), which contain a mesh that displays a number of tiles.
These tile groups are added and removed automatically as needed so you don't have to worry about them.

Tile Layer properties:
- Use UV Space: When checked, UniTile will use UV coordinates rather than pixel coordinates. This is useful if you plan to resize your texture later.
- Tile Size: The size of the individual tiles in your tileset
- Tile Output Size : The dimensions of a tile in Unity units. Usually this will be the same as Tile Size.
- Group Size: The amount of tiles that will be grouped together in the tile groups. The default values should be fine for most cases.
- Layer Size: The size of the current layer. Usually, all the layers in your map will have the same size, but if you want to you can have different sizes.
- Tile Border Size: You can add a border to the tiles in your tileset to fix edge bleeding. If you have one pixel on each side of your tiles, set the Tile Border Size to (1,1).

Tile Layer buttons:
- Apply: This will apply any changes that you made in the above properties to the layer. So if you change the Layer's size, it will only resize the layer once you press Apply.
- Revert: If you changed any of the properties, you can press Revert instead of Apply to go back to the current state. Deselecting the GameObject has the same effect.
- Clear layer: This will clear the entire layer. You can still use Undo to get your map back.

- Use scrollview: this checkbox will show your tileset in a scrollview.


Tile properties
---------------

A new feature in 1.1 is the ability to add tile properties to individual tiles in your tileset. You can do this by double-clicking a tile in the inspector.
You will see a popup with the following properties:

- Name: You can give your tiles a name. If you only need to add one string value to your tiles, this can be useful for this.
- Value: A numeric (float) value.
- Prefab: You can assign any prefab to this property. This is useful for instantiating colliders or other things.
- Prefab Offset: This is only visible if you selected a prefab. Here you can assign an offset relative to position of the tile in your map. By default, it will be at the center of a tile.
- Box Collider: Check this if you want to automatically add box colliders to your map
- Box Layer: Here you can choose a Unity layer for the box colliders for this tile.
- Box Tag: Here you can choose a tag for the box colliders for this tile.
- Box Depth: The depth (the z coordinate) of generated boxes
- Properties: If you change this number to anything higher than 0, you will get a list of Key/Value pairs where you can assign custom properties

You can access the properties with the API (see below).


Instantiating
-------------

If you assign prefabs or box colliders to your tiles, you can instantiate them by pressing the "Create Objects" button in the layer's inspector. This will remove any previous instances and box colliders.
The box colliders are grouped together wherever possible for performance.

You can also choose to instantiate everything by code with the API (see below).


Object layer
------------

A new feature in UniTile 1.2 is the object layer. To create an object layer, go to your UniTileManager and press "Add object layer".
In your new object layer, You can assign prefabs to the "Prefabs" array, and the inspector will show a button for each prefab. If you then press one of those buttons, a new instance will be added to the scene.
If you have Unity's hand tool selected, you can drag the object around in the scene. There's a checkbox for snapping the objects to the grid, and you can define an offset for this.
You can control-click objects to delete them.


Texture Padder
--------------

I have included a script that can create borders around tiles in a tileset. This is useful to prevent edge bleeding (where you can see the edges of one tile in another).
Your texture should have Read/Write enabled and be ARGB32, RGB24 or Alpha8 (but Unity will tell you if it's not correct).

You can access this in two ways:
- By clicking the Padder button on a layer
- By selecting a tile in the Project broswer, and then choosing the UniTile/Texture Padder option from the menu
	
If you select it from your layer, it will prefill some of the fields with data from your layer.

Options:
- Tile size: The size of the tiles in your tileset
- Border size: The size of the border that will be applied to each side of each tile
- Texture size: The size of the resulting texture

Once you press the save button, you will get a dialog from which you can save your texture.


3. API
======

The TileLayer class now has a number of methods for accessing tile information and properties.
It is recommended to access these methods at the start of your level, and store any values you need later.
Most of these methods require the x and y coordinates of a tile. These coordiantes are not in world or camera space, but the position of the tiles in the layer.
An example: The tile at the bottom left of your map is accessed with (0, 0), the tile next to it with (1, 0) and the tile above it with (0, 1).

- int GetTileId(int x, int y)
Gets the number of the tile at x, y

- UniTileTile GetTile(int x, int y)
Returns an object with the tile properties for the tile at x, y. You can also access the individual properties with the methods below, but if you need to access a lot of different properties, getting this object will be more performant.

- GameObject GetPrefab(int x, int y)
Returns a reference to the prefab assigned to the tile at x, y

- string GetName(int x, int y)
Returns the name property assigned to the tile at x, y

- float GetValue(int x, int y)
Returns the value property assigned to the tile at x, y

- string GetProperty(int x, int y, string key)
Returns a property from the key/value pairs of the tile at x,y. This is slower than getting the name or value properties, so only use this if you need more properties.

- bool HasBox(int x, int y)
- int GetBoxLayer(int x, int y)
- string GetBoxTag(int x, int y)
- float GetBoxDepth(int x, int y)
These methods return information used for the automatic generation of box colliders. Usually you will not need to access these yourself.

- void InstantiatePrefabs()
Creates instances of the assigned prefabs, and also creates box colliders for the tiles that have the "Box Collider" checkbox checked. You can do this in the editor by pressing the "Create Objects" button, but if you prefer you can do this by code by calling this method.


That's it. If you have any questions, feedback or feature requests, please contact me at info@mudloop.com