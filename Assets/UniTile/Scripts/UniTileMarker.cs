using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class UniTileMarker : MonoBehaviour {
	
	public MeshRenderer meshRenderer;
	public MeshFilter meshFilter;
	
	public void Awake() {
		if(UniTileManager.instance.activeLayer == null) {
			DestroyImmediate(this.meshFilter.sharedMesh);
			DestroyImmediate(this.gameObject);
		}
	}
	
	public void Init() {
		if(this.meshFilter.sharedMesh!=null) DestroyImmediate(this.meshFilter.sharedMesh);
		
		UniTileManager manager = UniTileManager.instance;
		TileLayer layer = manager.activeLayer;
		
		if(layer.material==null || layer.material.mainTexture == null) return;
		
		if(layer.tileSpacing == new Vector2(-1, -1)) layer.tileSpacing = layer.tileSize;
		
		this.meshRenderer.material = layer.material;
		
		Mesh m = new Mesh();
		
		int count = manager.selectedTiles.Length;
		
		Vector3 [] vertices = new Vector3[4 * count];
		int [] triangles = new int[6 * count];
		Vector2 [] uv = new Vector2[4 * count];
		
		for(int i=0;i<count;i++) {
			int tileX = i % manager.selectedTilesWidth;
			int tileY = (int)Mathf.Floor(i / manager.selectedTilesWidth);
			int x = (int)((tileX)*layer.tileSpacing.x);
			int y = (int)((-tileY)*layer.tileSpacing.y);
			
			// Triangles
			triangles[(i*6) + 0] = (i*4) + 0;
			triangles[(i*6) + 1] = (i*4) + 1;
			triangles[(i*6) + 2] = (i*4) + 2;
			triangles[(i*6) + 3] = (i*4) + 1;
			triangles[(i*6) + 4] = (i*4) + 3;
			triangles[(i*6) + 5] = (i*4) + 2;
			
			if(manager.selectedTiles[i] != -1) {
				
				vertices[(i*4) + 0] = new Vector3(0 + (int)x, layer.tileSpacing.y + (int)y, 0);
				vertices[(i*4) + 1] = new Vector3(layer.tileSpacing.x + (int)x, layer.tileSpacing.y + (int)y, 0);
				vertices[(i*4) + 2] = new Vector3(0 + (int)x, 0 + (int)y, 0);
				vertices[(i*4) + 3] = new Vector3(layer.tileSpacing.x + (int)x, 0 + (int)y, 0);
				
				int columns = (int)(layer.material.mainTexture.width / (layer.tileSize.x + layer.borderSize.x * 2f));
				float uvx = Mathf.Floor((int)manager.selectedTiles[i] % columns) * (layer.tileSize.x + layer.borderSize.x * 2f) + layer.borderSize.x;
				float uvy = Mathf.Floor((int)manager.selectedTiles[i] / columns) * (layer.tileSize.x + layer.borderSize.y * 2f) + layer.borderSize.y;
				float uvx2 = uvx+layer.tileSize.x;
				float uvy2 = uvy+layer.tileSize.y;
				
				// UVS
				uv[(i*4) + 0] = new Vector2(uvx / (float)layer.material.mainTexture.width, 1f - uvy / (float)layer.material.mainTexture.height);
				uv[(i*4) + 1] = new Vector2(uvx2 / (float)layer.material.mainTexture.width, 1f - uvy / (float)layer.material.mainTexture.height);
				uv[(i*4) + 2] = new Vector2(uvx / (float)layer.material.mainTexture.width, 1f - uvy2 / (float)layer.material.mainTexture.height);
				uv[(i*4) + 3] = new Vector2(uvx2 / (float)layer.material.mainTexture.width, 1f - uvy2 / (float)layer.material.mainTexture.height);
			}
			
			
		}
		
		m.vertices = vertices;
		m.triangles = triangles;
		m.uv = uv;
		
		m.RecalculateNormals();
		
		this.meshFilter.mesh = m;
		
	}
}
