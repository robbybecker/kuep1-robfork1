// Copyright 2011 Sven Magnus

using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

[AddComponentMenu("UniTile/TileLayer")]

public class TileLayer : MonoBehaviour {
	
	public TileLayer parent;
	
	public bool useUvSpace = false;
	public bool useScrollView = false;
	public Vector2 tileUvSize = new Vector2(0.1f, 0.1f);
	public Vector2 tileSize = new Vector2(32, 32);
	public Vector2 tileSpacing = new Vector2(-1, -1);
	public Vector2 overlap = new Vector2(0, 0);
	public Vector2 overlapUv = new Vector2(0, 0);
	public Vector2 groupSize = new Vector2(10, 10);
	public Vector2 layerSize = new Vector2(100, 50);
	public Vector2 borderSize = new Vector2(0, 0);
	public Vector2 borderSizeUv = new Vector2(0, 0);
	public UniTileTileset tileset;
	
	
	public Material material;
	
	// Multidemensional arrays are not serialized so we need to be creative
	[HideInInspector] public int[] tiles;
	
	public int GetTileId(int x, int y) {
		if(this.tiles == null || this.tiles.Length <= x + y * (int)this.layerSize.x) return -1;
		if(x<0 || x>=layerSize.x || y<0 || y>=layerSize.y) return -1;
		return this.tiles[x + y * (int)this.layerSize.x];
	}
	
	public UniTileTile GetTile(int x, int y) {
		if(this.tiles == null || this.tiles.Length <= x + y * (int)this.layerSize.x) return null;
		if(this.tileset == null) return null;
		if(x<0 || x>=layerSize.x || y<0 || y>=layerSize.y) return null;
		int tile = this.tiles[x + y * (int)this.layerSize.x];
		if(tile == -1) return null;
		if(this.tileset.tiles == null) return null;
		if(tile >= this.tileset.tiles.Length) return null;
		return this.tileset.tiles[tile];
	}
	
	public GameObject GetPrefab(int x, int y) {
		UniTileTile tile = GetTile(x, y);
		if(tile == null) return null;
		return tile.prefab;
	}
	
	public string GetName(int x, int y) {
		UniTileTile tile = GetTile(x, y);
		if(tile == null) return null;
		return tile.name;
	}
	
	public float GetValue(int x, int y) {
		UniTileTile tile = GetTile(x, y);
		if(tile == null) return 0;
		return tile.value;
	}
	
	public string GetProperty(int x, int y, string key) {
		UniTileTile tile = GetTile(x, y);
		if(tile == null) return null;
		for(int i=0; i<tile.properties.Length; i++) {
			if(tile.properties[i].key == key) {
				return tile.properties[i].value;
			}
		}
		return null;
	}
	
	public bool HasBox(int x, int y) {
		UniTileTile tile = GetTile(x, y);
		if(tile == null) return false;
		return tile.boxCollider;
	}
	
	public int GetBoxLayer(int x, int y) {
		UniTileTile tile = GetTile(x, y);
		if(tile == null) return 0;
		return tile.boxLayer;
	}
	
	public string GetBoxTag(int x, int y) {
		UniTileTile tile = GetTile(x, y);
		if(tile == null) return null;
		return tile.boxTag;
	}
	
	public float GetBoxDepth(int x, int y) {
		UniTileTile tile = GetTile(x, y);
		if(tile == null) return 0;
		return tile.boxDepth;
	}
	
	public void InstantiatePrefabs() {
		if(this.tileset == null) return;
		if(this.tileset.tiles == null) return;
		
		Transform t = this.transform.FindChild("Instances");
		if(t!=null) {
			DestroyImmediate(t.gameObject);
		}
		t = this.transform.FindChild("BoxColliders");
		if(t!=null) {
			DestroyImmediate(t.gameObject);
		}
		GameObject instances = new GameObject("Instances");
		Transform instancesTransform = instances.transform;
		instancesTransform.parent = this.transform;
		instancesTransform.localPosition = new Vector3(0, 0, 0);
		instancesTransform.localScale = new Vector3(1, 1, 1);
		instancesTransform.localRotation = new Quaternion(0,0,0,0);
		
		GameObject boxes = new GameObject("BoxColliders");
		Transform boxesTransform = boxes.transform;
		boxesTransform.parent = this.transform;
		boxesTransform.localPosition = new Vector3(0, 0, 0);
		boxesTransform.localScale = new Vector3(1, 1, 1);
		boxesTransform.localRotation = new Quaternion(0,0,0,0);
		
		GameObject prefab;
		GameObject instance;
		GameObject box;
		BoxCollider collider;
		Hashtable skip = new Hashtable();
		
		if(this.tileSpacing == new Vector2(-1,-1) || this.tileSpacing == new Vector2(0,0)) this.tileSpacing = this.tileSize;
		
		for(int i = 0; i < this.layerSize.x; i++) {
			for(int j = 0; j < this.layerSize.y; j++) {
				UniTileTile tile = this.GetTile(i, j);
				if(tile!=null) {
					prefab = tile.prefab;
					if(prefab != null) {
						
#if UNITY_EDITOR
						if(Application.isPlaying) {
							instance = (GameObject)Instantiate(prefab);
						} else {
							instance = (GameObject)EditorUtility.InstantiatePrefab(prefab);
						}
#else
						instance = (GameObject)Instantiate(prefab);
#endif
						Vector3 scale = instance.transform.localScale;
						instance.transform.parent = instancesTransform;
						instance.transform.localScale = scale;
						instance.transform.localRotation = new Quaternion(0,0,0,0);
						instance.transform.localPosition = new Vector3(i * this.tileSpacing.x + tile.prefabOffset.x, j * this.tileSpacing.y + tile.prefabOffset.y, 0);
					}
					if(tile.boxCollider) {
						if(skip[new Vector2(i, j)] == null) {
							box = new GameObject();
							box.layer = tile.boxLayer;
							box.tag = tile.boxTag;
							collider = box.AddComponent<BoxCollider>();
							collider.material = tile.boxMaterial;
							box.transform.parent = boxesTransform;
							box.transform.localScale = new Vector3(1, 1, 1);
							box.transform.localRotation = new Quaternion(0,0,0,0);
							box.transform.localPosition = new Vector3(i * this.tileSpacing.x/* + this.tileSize.x/2f*/, j * this.tileSpacing.y/* + this.tileSize.y / 2f*/, 0);
							
							int sizeX = 1;
							int sizeY = 1;
							while(skip[new Vector2(i, j + sizeY)]==null && this.HasBox(i, j + sizeY) && this.GetBoxDepth(i, j) == this.GetBoxDepth(i, j + sizeY) && this.GetBoxTag(i, j) == this.GetBoxTag(i, j + sizeY) && this.GetBoxLayer(i, j) == this.GetBoxLayer(i, j + sizeY)) {
								skip[new Vector2(i, j + sizeY)] = true;
								sizeY++;
							}
							
							if(sizeY == 1) {
								while(skip[new Vector2(i + sizeX, j)]==null && this.HasBox(i + sizeX, j) && this.GetBoxDepth(i, j) == this.GetBoxDepth(i + sizeX, j) && this.GetBoxTag(i, j) == this.GetBoxTag(i + sizeX, j) && this.GetBoxLayer(i, j) == this.GetBoxLayer(i + sizeX, j)) {
									skip[new Vector2(i + sizeX, j)] = true;
									sizeX++;
								}
							} else {
								sizeX = 1;
								bool columnOk = true;
								int column = 1;
								while(columnOk) {
									columnOk = true;
									for(int k=0;k<sizeY;k++) {
										if(skip[new Vector2(i + column, j + k)]!=null || !this.HasBox(i + column, j + k) || this.GetBoxDepth(i, j) != this.GetBoxDepth(i + column, j + k) || this.GetBoxTag(i, j) != this.GetBoxTag(i + column, j + k) || this.GetBoxLayer(i, j) != this.GetBoxLayer(i + column, j + k)) {
											columnOk = false;
											break;
										}
									}
									if(columnOk) {
										column++;
									}
								}
								sizeX = column;
								for(int k=0;k<sizeX;k++) {
									for(int l=0;l<sizeY;l++) {
										skip[new Vector2(i + k, j + l)] = true;
									}
								}
							}
							
							
							collider.size = new Vector3(this.tileSpacing.x * sizeX, this.tileSpacing.y * sizeY, tile.boxDepth);
							collider.center = new Vector3(this.tileSpacing.x * sizeX / 2f, this.tileSpacing.y * sizeY / 2f);
						}
					}
				}
			}
		}
	}
	
}