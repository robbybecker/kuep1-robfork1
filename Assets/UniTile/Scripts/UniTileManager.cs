// Copyright 2011 Sven Magnus

using UnityEngine;
using System.Collections;

public class UniTileManager : MonoBehaviour {
	
	
	private static UniTileManager _instance;
	
	public static UniTileManager instance {
		get {
			if(_instance==null) {
				GameObject g = GameObject.Find("UniTileManager");
				if(g == null) {
					g = new GameObject("UniTileManager");
					_instance = g.AddComponent<UniTileManager>();
				} else {
					_instance = g.GetComponent<UniTileManager>();
				}
				g.tag = "EditorOnly";
			}
			return _instance;
		}
	}
	
	public static UniTileManager Reset() {
		// Removes the reference to the manager
		// This seems to be necessary due to a problem where a new instance is actually created when undoing something, while the reference was still pointing to the old one (which was no longer in the scene).
		// (Gave me quite a headache xD)
		_instance = null;
		return instance;
	}
	
	[HideInInspector] public int selectedTemplate = -1;
	[HideInInspector] public UniTileTemplate[] templates = new UniTileTemplate[0];
	
	[HideInInspector] public int[] selectedTiles = new int[0];
	[HideInInspector] public int selectedTilesWidth;
	[HideInInspector] public int selectedTile = 0;
	[HideInInspector] public int selectedTileEnd = 0;
	[HideInInspector] public bool tilesPicked;
	[HideInInspector] public Vector2 selectionCheck; // used to check if we can keep the current selection when switching between layers
	[HideInInspector,System.NonSerialized] public TileLayer activeLayer;
	[HideInInspector,System.NonSerialized] public ObjectLayer activeObjectLayer;
	[HideInInspector] public TileLayer lastLayer;
	[HideInInspector] public Texture2D selectionTexture;
	[HideInInspector] public UniTileMarker _marker;
	
	public UniTileMarker marker {
		get {
			if(this._marker == null && activeLayer != null) {
				GameObject go = new GameObject("Marker");
				go.transform.parent = activeLayer.transform;
				go.transform.localScale = new Vector3(1, 1, 1);
				Quaternion quaternion = go.transform.localRotation;
				quaternion.eulerAngles = new Vector3(0, 0, 0);
				go.transform.localRotation = quaternion;
				go.tag = "EditorOnly";
				this._marker = go.AddComponent<UniTileMarker>();
				this._marker.meshRenderer = go.AddComponent<MeshRenderer>();
				this._marker.meshFilter = go.AddComponent<MeshFilter>();
				this._marker.Init();
			}
			return this._marker;
		}
	}
	
	public static Vector3 mousePosition;
	
	[HideInInspector] public int layerCount = 0;
	[HideInInspector] public int objectLayerCount = 0;
	[HideInInspector] public int templateCount = 0;
	
	[System.NonSerialized,HideInInspector] public Vector2 pickStart;
	[System.NonSerialized,HideInInspector] public Vector2 pickEnd;
	[System.NonSerialized,HideInInspector] public bool picking;
	
	void OnDrawGizmos() {
		
		Gizmos.color = new Color (1f,1f,1f,1f);
		
		if(activeLayer != null) {
			
			if(activeLayer.tileSpacing == new Vector2(-1, -1)) activeLayer.tileSpacing = activeLayer.tileSize;
			
			Transform trans = activeLayer.transform;
			
			Gizmos.DrawLine(trans.TransformPoint(new Vector3(0,0,0)), trans.TransformPoint(new Vector3(activeLayer.tileSpacing.x * activeLayer.layerSize.x, 0, 0)));
			Gizmos.DrawLine(trans.TransformPoint(new Vector3(0,0,0)), trans.TransformPoint(new Vector3(0, activeLayer.tileSpacing.y * activeLayer.layerSize.y, 0)));
			Gizmos.DrawLine(trans.TransformPoint(new Vector3(0, activeLayer.tileSpacing.y * activeLayer.layerSize.y, 0)), trans.TransformPoint(new Vector3(activeLayer.tileSpacing.x * activeLayer.layerSize.x, activeLayer.tileSpacing.y * activeLayer.layerSize.y, 0)));
			Gizmos.DrawLine(trans.TransformPoint(new Vector3(activeLayer.tileSpacing.x * activeLayer.layerSize.x, activeLayer.tileSpacing.y * activeLayer.layerSize.y,0)), trans.TransformPoint(new Vector3(activeLayer.tileSpacing.x * activeLayer.layerSize.x, 0, 0)));
			
			if(picking) {
				Gizmos.color = new Color (1f,1f,0.5f,1f);
				Vector3 start = new Vector3(Mathf.Min(pickStart.x, pickEnd.x) * activeLayer.tileSpacing.x, Mathf.Min(pickStart.y, pickEnd.y) * activeLayer.tileSpacing.y, 0);
				Vector3 end = new Vector3(Mathf.Max(pickStart.x, pickEnd.x) * activeLayer.tileSpacing.x + activeLayer.tileSpacing.x, Mathf.Max(pickStart.y, pickEnd.y) * activeLayer.tileSpacing.y + activeLayer.tileSpacing.y, 0);
				Gizmos.DrawLine(trans.TransformPoint(new Vector3(start.x, start.y,0)), trans.TransformPoint(new Vector3(start.x, end.y)));
				Gizmos.DrawLine(trans.TransformPoint(new Vector3(start.x, start.y,0)), trans.TransformPoint(new Vector3(end.x, start.y)));
				Gizmos.DrawLine(trans.TransformPoint(new Vector3(start.x, end.y,0)), trans.TransformPoint(new Vector3(end.x, end.y)));
				Gizmos.DrawLine(trans.TransformPoint(new Vector3(end.x, start.y,0)), trans.TransformPoint(new Vector3(end.x, end.y)));
			} else {
				Gizmos.color = new Color (1f,1f,1f,1f);
				Vector3 start = mousePosition + new Vector3(0, activeLayer.tileSpacing.y, 0);
				Vector3 end = start + new Vector3(this.selectedTilesWidth * activeLayer.tileSpacing.x, -this.selectedTiles.Length / this.selectedTilesWidth * activeLayer.tileSpacing.y, 0);
				Gizmos.DrawLine(trans.TransformPoint(new Vector3(start.x, start.y,0)), trans.TransformPoint(new Vector3(start.x, end.y)));
				Gizmos.DrawLine(trans.TransformPoint(new Vector3(start.x, start.y,0)), trans.TransformPoint(new Vector3(end.x, start.y)));
				Gizmos.DrawLine(trans.TransformPoint(new Vector3(start.x, end.y,0)), trans.TransformPoint(new Vector3(end.x, end.y)));
				Gizmos.DrawLine(trans.TransformPoint(new Vector3(end.x, start.y,0)), trans.TransformPoint(new Vector3(end.x, end.y)));
			}
			
			Gizmos.color = new Color (1f,1f,1f,0.05f);
			for(int i=0;i<=activeLayer.layerSize.y;i++) {
				Gizmos.DrawLine(trans.TransformPoint(new Vector3(0, (float)i * activeLayer.tileSpacing.y, 0)), trans.TransformPoint(new Vector3(activeLayer.tileSpacing.x * activeLayer.layerSize.x, (float)i * activeLayer.tileSpacing.y, 0)));
			}
			for(int i=0;i<=activeLayer.layerSize.x;i++) {
				Gizmos.DrawLine(trans.TransformPoint(new Vector3((float)i * activeLayer.tileSpacing.x, 0, 0)), trans.TransformPoint(new Vector3((float)i * activeLayer.tileSpacing.x, activeLayer.tileSpacing.y * activeLayer.layerSize.y, 0)));
			}
		}
	}
	
}


[System.Serializable]
public class UniTileTemplate {
	public string name;
	[HideInInspector] public int[] selectedTiles = new int[0];
	[HideInInspector] public int selectedTilesWidth;
	[HideInInspector] public int selectedTile = 0;
	[HideInInspector] public int selectedTileEnd = 0;
	[HideInInspector] public bool tilesPicked;
	[HideInInspector] public Vector2 selectionCheck;
	
	public void Init() {
		UniTileManager.instance.templateCount++;
		this.selectedTiles = UniTileManager.instance.selectedTiles;
		this.selectedTilesWidth = UniTileManager.instance.selectedTilesWidth;
		this.selectionCheck = UniTileManager.instance.selectionCheck;
		this.selectedTile = UniTileManager.instance.selectedTile;
		this.selectedTileEnd = UniTileManager.instance.selectedTileEnd;
		this.tilesPicked = UniTileManager.instance.tilesPicked;
		this.name = "Template "+UniTileManager.instance.templateCount;
	}
	
	public void Use() {
		UniTileManager.instance.selectedTiles = this.selectedTiles;
		UniTileManager.instance.selectedTilesWidth = this.selectedTilesWidth;
		UniTileManager.instance.selectionCheck = this.selectionCheck;
		UniTileManager.instance.selectedTile = this.selectedTile;
		UniTileManager.instance.selectedTileEnd = this.selectedTileEnd;
		UniTileManager.instance.tilesPicked = this.tilesPicked;
		
		
		/*if(UniTileManager.instance.selectedTiles.Length==1) {
			UniTileManager.instance.selectedTile = UniTileManager.instance.selectedTileEnd = UniTileManager.instance.selectedTiles[0];
			UniTileManager.instance.tilesPicked = false;
		} else {
			UniTileManager.instance.tilesPicked = true;
		}*/
	}
}
