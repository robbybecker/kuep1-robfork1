using UnityEngine;
using System.Collections;

public class UniTileTileset : MonoBehaviour {
	public UniTileTile [] tiles;
}

[System.Serializable]
public class UniTileTile {
	public GameObject prefab;
	public Vector2 prefabOffset;
	public string name;
	public float value;
	public UniTileProperty [] properties;
	public bool boxCollider;
	public int boxLayer;
	public PhysicMaterial boxMaterial;
	public string boxTag = "Untagged";
	public float boxDepth = 64;
	public bool resizable;
}

[System.Serializable]
public class UniTileProperty {
	public string key;
	public string value;
}