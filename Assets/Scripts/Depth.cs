//////////////////////////////////////////////
////Depth Script - Owen
////Manages the depth of objects
//////////////////////////////////////////////

using UnityEngine;
using System.Collections;

public class Depth : MonoBehaviour {

    private float zPos;     // The depth of the object

    private Screen screen;  // The Screen

    public Camera cam;      // The Camera

    public float tweak = 0f;// Modidifier to correct the zPos
	

	void Start () 
    {
        if (cam == null)                // if the camera has not been set
            cam = Camera.mainCamera;    // set the camer to MAIN CAMERA

        zPos = calculateZPos();         // calculate the correct depth

        // set the object to the correct depth
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, zPos);
	}
	
	// Update is called once per frame
	void Update ()
    {
        // calculate the correct depth, apply the tweek which is set in Unity
        zPos = calculateZPos() + tweak;
        // move to object to the correct depth
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, (zPos));

    }

    //the is used to calculate the correct depth
    float calculateZPos()
    {
        Vector3 screenPos = cam.WorldToScreenPoint(this.transform.position);    // Where is the object on the screen?
        float screenY = screenPos.y;                                            // Extract the Y value - How high or low up the screen is it
        return ((Screen.height - screenY) / Screen.height) * 100 * -1;          // Calculate the depth based on the Y value
    }
}
