//images
var tutImage1: Texture2D;
var tutImage2: Texture2D;
var tutImage3: Texture2D;
var tutArray: Array;
var dialogueCounter: int = 0;
var leftEdge : int= 0;

private var screenWidth : int = Screen.width;
private var screenHeight: int = Screen.height;
private var tweenObject:  GameObject ;


function Start(){
	//dummy game object to hold x pos for itween
    tweenObject = new GameObject();
    tweenObject.transform.localPosition.x = leftEdge;
	//tweenObject.transform.localPosition.y = textY;
}

function Update () {
	
	tutArray = new Array(tutImage1, tutImage2, tutImage3);
	
	//if finger is released to left or right, move the dialogue forward or back depending on where it was released
	if (Input.GetMouseButtonUp(0) && dialogueCounter + 2 <= tutArray.length  ){
		dialogueCounter++;
		var position :float = 0 - (screenWidth)*dialogueCounter;
		animateTo(position);
	} else if (Input.GetMouseButtonUp(0) && dialogueCounter + 2 > tutArray.length){
		Application.LoadLevel("titleScreen");
	}
}

function OnGUI (){	
	leftEdge =  tweenObject.transform.localPosition.x;
	for (var i=0; i<tutArray.length; i++){
		Debug.Log(tutArray[i]);
		GUI.DrawTexture(Rect(leftEdge+(screenWidth*i),0,screenWidth, screenHeight), tutArray[i] );
	}
}

function animateTo (xValue : float) {

	iTween.MoveTo(tweenObject, {"x": xValue, "time": 0.5});
	//Ani.Mate.To(this, 1, {"leftEdge": xValue , "easing": Ani.Easing.Elastic, "direction":Ani.Easing.Out});
}