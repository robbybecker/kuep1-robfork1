///////////////
///This governs the Heros health
///Damage can be allpild and health can be added
///Use broadcast message to achieve this
//////////////


using UnityEngine;
using System.Collections;

public class HeroHealth : MonoBehaviour {

    // the heros starting health
    //will currently be reset each scene
    public int StartHealth = HealthLevel.maxHealth;
	public int Health;

	
    public AudioClip damageSound;
	
	private bool alive = true;
	
	GameObject target;
	
	void Start()
	{
		Health = StartHealth;
		HealthLevel.kuHealth = Health;
		
	}
	
	void Update()
	{
		if (Health <= 0)
        	    die();
	}
	
    // apply damage to the hero
    // use broadcast message on the attacking object to use this
    void takeDamage(int damage)
    {
        if(alive)
		{
			//subtract the damage form the heros health
       		Health -= damage;
			HealthLevel.kuHealth = Health;
		
		
        	audio.PlayOneShot(damageSound);

        	Debug.Log("Health: " + Health);
			Lumos.Event("Damage Taken", damage);
        	// check if he is still alive
        	if (Health <= 0)
        	    die();
		}
    }

    // give the hero health
    // use broadcast message on the health pack to use this
    void getHealth(int healthPackValue)
    {
        // add the value of the health pack
        Health += healthPackValue;
		HealthLevel.kuHealth = Health;
    }

    // this can be used to kill the player
    // when we decide what happend when the player dies, the code will go here
    private void die()
    {
        Debug.Log("He dead!");
		Lumos.Event("Ku Death");
		BroadcastMessage("stopDoingStuff");		// runs the die function in hero.cs
		BroadcastMessage("Die");	// play the death animation, recieved in herAnimation.cs
		
		alive = false;
		
		StartCoroutine(countDownToRestart());
		
    }
	
	IEnumerator countDownToRestart()
	{
		
		Debug.Log("Countdown begun");
		target = GameObject.FindGameObjectWithTag("theTarget");
		yield return new WaitForSeconds(1);
		
		Debug.Log("Countdown Complete");
		target.BroadcastMessage("HeroDied");
		
	}
	
	void OnGUI()
	{
		float barWidth = (HealthLevel.maxHealth - (HealthLevel.maxHealth - HealthLevel.kuHealth)) * 50;
		float ScreenPos = (Screen.width / 2) - (barWidth / 2);
		GUI.Box(new Rect(ScreenPos, 5f, barWidth, 20f), "Health");	
	}
}
