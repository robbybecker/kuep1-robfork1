using UnityEngine;
using System.Collections;

public class SparkoBotAnimation : MonoBehaviour {
	
	private bool isDead = false;
	PackedSprite ps;
	
	enum Directions { West = 1, East = 2, North = 3, South = 4 }
	
	private Directions direction = Directions.South;
	
	private bool Available = true;
	// Use this for initialization
	void Awake () 
	{
		// get a reference to the PS script
        ps = this.GetComponent<PackedSprite>();
	}
	
	
	void animateMove(float angle)
	{
		if(Available)
		{
			if(angle > 0)
			{
				if(angle < 45f)
				{
					//right
					ps.DoAnim("moveLeft");
					transform.eulerAngles = new Vector3(0f, 180f, 0f);
				}
				else if( angle < 135f)
				{
					// up
					ps.DoAnim("moveUp");
					transform.eulerAngles = new Vector3(0f, 0f, 0f);
				}
				else
				{
					// left	
					ps.DoAnim("moveLeft");
					transform.eulerAngles = new Vector3(0f, 0f, 0f);
				}
				
			}
			else
			{
				if(angle > -45f)
				{
					// right	
					ps.DoAnim("moveLeft");
					transform.eulerAngles = new Vector3(0f, 180f, 0f);
				}
				else if(angle > -135f)
				{
					// down	
					ps.DoAnim("moveDown");
					transform.eulerAngles = new Vector3(0f, 0f, 0f);
				}
				else
				{
					// left	
					ps.DoAnim("moveLeft");
					transform.eulerAngles = new Vector3(0f, 0f, 0f);
				}
			}
		}
		
	}
	
	void animateAttack(float angle)
	{
		if(Available)
		{
			Available = false;
			if(angle > 0f)
			{
				if(angle < 45f)
				{
					//right
					ps.DoAnim("attackLeft");
					transform.eulerAngles = new Vector3(0f, 180f, 0f);
				}
				else if( angle < 135f)
				{
					// up
					ps.DoAnim("attackUp");
					transform.eulerAngles = new Vector3(0f, 0f, 0f);
				}
				else
				{
					// left	
					ps.DoAnim("attackLeft");
					transform.eulerAngles = new Vector3(0f, 0f, 0f);
				}
				
				
			}
			else
			{
				if(angle > -45f)
				{
					// right	
					ps.DoAnim("attackLeft");
					transform.eulerAngles = new Vector3(0f, 180f, 0f);
				}
				else if(angle > -135f)
				{
					// down	
					ps.DoAnim("attackDown");
					transform.eulerAngles = new Vector3(0f, 0f, 0f);
				}
				else
				{
					// left	
					ps.DoAnim("attackLeft");
					transform.eulerAngles = new Vector3(0f, 0f, 0f);
				}
			}
		
			ps.SetAnimCompleteDelegate(attackDone);
			
		}
		
	}
	
	
	
	
	
	// here are the delages
	
	void noDelegate(SpriteBase sprite)
	{
		// nothing to see here
	}
	
	void attackDone(SpriteBase sprite)
	{
		Available = true;
		
		ps.SetAnimCompleteDelegate(noDelegate);
		
		
	}
	
	void Die()
	{
		if (!isDead){
			ps.PlayAnim("death");	
			isDead=true;
		}
	}
}
