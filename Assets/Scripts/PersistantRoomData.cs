/// <summary>
/// This script is for persistant objects that hold data about  scene
/// FOr example 
/// - the player enters a room with a blocked path.
/// - they go east to solve a puzzle 
/// - When they re enter the first room this object knows that they have solved the puzzle and unblocks the door
/// It can also be used for dialogue, combat or anything else
/// </summary>


using UnityEngine;
using System.Collections;

public class PersistantRoomData : MonoBehaviour {
	
	public static PersistantRoomData roomData;
	public bool triggered = false;

	// Use this for initialization
	void Awake () 
	{
		if(roomData == null)
			roomData = this;
		else
			Destroy(this.gameObject);
		
		// the object will continue to exist form room to room
		DontDestroyOnLoad(this.gameObject);
	}
	
	public void Done()
	{
		Destroy(this.gameObject);
	}
	
	
}
