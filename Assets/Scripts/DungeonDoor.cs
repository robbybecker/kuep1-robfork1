/// <summary>
/// This is for the doors in the dungeon
/// </summary>


using UnityEngine;
using System.Collections;

public class DungeonDoor : MonoBehaviour {

	private PackedSprite packedSprite;	// the sprite
	private Vector3 startPos;			// the initial position of the door
	
	public GameObject blocker;			// the object that blocks the player form moving throught the door, must be set in editor
	
	// Use this for initialization
	void Awake () 
	{
		packedSprite = this.GetComponent<PackedSprite>();	// access the sprite
		startPos = transform.position;						// set the start position
	}
	
	// Update is called once per frame
	public void Open() 
	{
        //audio.Play();               // play the gate open sound
		packedSprite.PlayAnim("open");   // play the gate open animation
		// move the sprite so the player will now go "through" the door
		transform.position = new Vector3(startPos.x, startPos.y, -70f);
		
		// destroy the barrier so the player can move through the door
		if(blocker != null)
			GameObject.Destroy(blocker);
	}
}
