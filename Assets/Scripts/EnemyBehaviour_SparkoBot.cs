using UnityEngine;
using System.Collections;

public class EnemyBehaviour_SparkoBot : MonoBehaviour {

    public GameObject Hero;    // the hero

    public float range; // the range that will triger a state change
    public float damage;// the mount of damage the enemy does

    // these are the states the enemy has
    public enum Behaviors { idle = 1, alert = 2, agro = 3, asleep = 4}

    public Behaviors currentBehavior = Behaviors.idle;  // stores the current state

    Vector3 moveVector;         // the vector to move towards
    public float moveSpeed;     // the move speed
    public float attackRange;   // the distance he can attack from

    // this variable, when ture, will make the enemy perssitant, 
    // they will keep persueing Ku, no matter how far away he gets
    public bool longRangeAfterWake = false;

    // these boosl are used to determine what stage of the attack process the enemy is in

    public bool beginAttacking; // true at the very start of the attack
    public bool stillAttacking; // true threwout the attack
    public bool stopAttacking;  // true ot the very end of the attack
	public bool attackUnderway;	// true during ongoing attack
	
	private bool alive = true;	// is the enemy alive
	
	private CharacterController charController;
	
	
	public float angle;

    void Awake()
    {
		charController = GetComponent("CharacterController")as CharacterController;
        // reset the attack bools
        beginAttacking = false;
        stillAttacking = false;
        stopAttacking = false;
		attackUnderway = false;
		

        //audio.Play();
    
        // find the hero
        if (Hero == null)
            Hero = GameObject.FindGameObjectWithTag("Hero");

        // set the range
        if (range == 0)
            range = 10f;

        

        if (moveSpeed == 0)
            moveSpeed = 2f;

        if (attackRange == 0)
            attackRange = 2f;

    }

    // Update is called once per frame
	void Update () 
    {
		if(alive && IsPaused.isPaused == false)
		{
        	CheckYoSelf();  // check the enviroment, to see if a state change is necessary
        	
        	// preform the behavoiur apropriate to your state
        	switch (currentBehavior)
        	{
        	    case Behaviors.idle:					
        	        beIdle();
        	        break;

            	case Behaviors.alert:
                	beAlert();
                	break;

            	case Behaviors.agro:
                	beAgro();
                	break;
			}
		}
	}

    void CheckYoSelf()
    {
        
        // this function will preform enviromental checks
        // to determine what state should be active

        // distance to the hero
        float distance = Vector3.Distance(Hero.transform.position, this.transform.position);


        
            if (distance < range)   // if hero is in range
            {
                currentBehavior = Behaviors.agro;   // change state to agro
            }
			else
        	{
            	currentBehavior = Behaviors.idle;
        	}
        
        
    }


    // Here are the diferent behaviours

    void beIdle()
    {
		//Debug.Log("Idle called");
        if (stillAttacking)
            stillAttacking = false;
		if (attackUnderway)
			attackUnderway = false;
    }

    void beAlert()
    {
        
    }

    void beAgro()
    {
        if (longRangeAfterWake)
            range = 100;
        
        // get direction
        moveVector = Hero.transform.position - this.transform.position;
		angle = VectorToAngle(moveVector);
		
		
        // chekc if the hero is in range
        if (moveVector.magnitude > attackRange)
        {
            // if the hero is out of range

            // move closer
            moveVector.Normalize();             // normalise
            moveVector *= moveSpeed;            // apply speed 
            moveVector *= Time.deltaTime;       // apply time

            //transform.Translate(moveVector);    // move towards the hero
			charController.Move(moveVector);
			
			
			// animate
			BroadcastMessage("animateMove", angle);
        }
        else   // if the hero is in attack range
        {
            // if we are not currently attacking
            if (!attackUnderway)
            {
				attackUnderway = true;
                //begin attacking
                beginAttacking = true;
                
                // start the attack loop
                StartCoroutine(Attack());

                // this is set to true, so we can tell the the attack is ongoing
                // it is set to false once the hero moves back to an idle state
                stillAttacking = true;
            }

        }

    }

    // this is the attack loop
    IEnumerator Attack()
    {
		
		
        // set to false, as the attack has now begun
        beginAttacking = false;

        // this send a message to EnemyAttack
        BroadcastMessage("Fire", angle);

        yield return new WaitForSeconds(2); // after 2 seconds
        if(attackUnderway && alive)                  // if we are still attacking and alive
            StartCoroutine(Attack());       // continue the attack
    }
	
	void stopDoingStuff()
	{
		alive = false; 	
	}
	
	float VectorToAngle(Vector3 vector)
	{
		float angleRaw = (float)Mathf.Atan2(vector.y, vector.x);	
		
		while (angleRaw < -(float)Mathf.PI)     // while the angle is less than -180
            angleRaw += (float)(Mathf.PI * 2);  // add 360

        while (angleRaw > (float)Mathf.PI)      // while the angle is greater than 180
            angleRaw -= (float)(Mathf.PI * 2);  // subtract 360
		
		return angleRaw * Mathf.Rad2Deg;
	}

}
