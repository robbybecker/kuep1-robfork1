enum SoundType {Sfx, Music}
var sType: SoundType = SoundType.Sfx; // define the sound type in the Inspector

private var iniVol: float;
private var gVol: float;

function Start(){
    iniVol = audio.volume; // save the local volume
}

function Update(){
    switch (sType){ // get the correct type volume
        case SoundType.Sfx: 
            gVol = sliderLabel.sfxVolume;
            break;
        case SoundType.Music: 
            gVol = sliderLabel.musicVolume;
            break;
    }
    audio.volume = iniVol * gVol;
}