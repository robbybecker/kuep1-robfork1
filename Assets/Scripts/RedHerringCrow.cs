using UnityEngine;
using System.Collections;

public class RedHerringCrow : MonoBehaviour {
	
	public int Damage = 1;
	public float Speed = 1f;
	
	
	public bool enabled = false;
	
	public Transform target;
	public Transform exit;
	
	public GameObject bots;
	
	void Start()
	{

		
		enabled= false;
	}
	
	// TODO: steer around obsticales
	
	void Go()
	{
		enabled = true;	
	}
	
	void Update()
	{
		if(enabled)
		{
			if(target != null)
				transform.Translate((target.position - transform.position).normalized * Speed * Time.deltaTime);
			
			
		}
		
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject == target.gameObject)
		{
			target = exit;
			bots.BroadcastMessage("Go");
			Destroy(other.gameObject);
			
		}
		
	}
	

}
