/// <summary>
/// This is teh health system of the ememies 
/// </summary>

using UnityEngine;
using System.Collections;

public class EnemyDamage : MonoBehaviour {

    public int Health = 10; 			// the health of the enemy
	private int MaxHealth;				// the full health of the enemy
	public AudioClip[] damageSound;
    //public AudioClip damageSound;
	public AudioClip[] dieSound; 			//NOTE FROM RALPH : I've added this mainly for the helikin die sound. 
	private bool isDead= false; 		//check if he's alreadty dead, to stop repeating die behaviour
	public bool ShowHealth = false;		// shuld a health bar be displayed
	
	public bool destroyColider = true;
	
	void Start()
	{
		MaxHealth = Health;	
	}
	
	
    // This is called from the HERO class
    // It applies damage to the enemy
    public void TakeDamage(int Damage)
    {
        Debug.Log("Taking damage");
		Lumos.Event("Damage Done", Damage);
		BroadcastMessage("Hurt", SendMessageOptions.DontRequireReceiver);
        Health -= Damage;   // apply damage to the enemy
        CheckIfDead();      // check if the enemy is dead
		
		if (Health > 0 ){
			Debug.Log("Damagesound length="+damageSound.Length);
			audio.PlayOneShot(damageSound[Random.Range(0, damageSound.Length-1)]); 
        //audio.PlayOneShot(damageSound);
		
		}
    }

    // checks to see if the enemy is dead
    private void CheckIfDead()
    {
        if (Health <= 0)    // if his health is less than zero
        {
			if (!isDead){
				ShowHealth = false;
				if(destroyColider)
					this.gameObject.collider.enabled = false; 	// remove colision form the object
	            BroadcastMessage("Die");						// play the die animation in AnimationInterface_Helikin
				BroadcastMessage("stopDoingStuff");				// stop doing stuff in EnemyBehavior
				audio.Stop();
				
				//audio.PlayOneShot(dieSound); 					//NOTE FROM RALPH: play death sound
				audio.PlayOneShot(dieSound[Random.Range(0, dieSound.Length-1)]);
				// remove the enemy form any game object group
				if(transform.parent != null)
					transform.parent = null;
			}
        }
    }
	
	// HealthBar
	
	void OnGUI()
	{
		if(ShowHealth)
		{
			float barWidth = (MaxHealth - (MaxHealth - Health)) * 50;
			float ScreenPos = (Screen.width / 2) - (barWidth / 2);
			GUI.Box(new Rect(ScreenPos, Screen.height - 40f, barWidth, 20f), "Enemy Health");
		}
	}


}
