/////////////////////////////////////////////////////
/// Target Script
/// Processes player INPUT
/////////////////////////////////////////////////////


using UnityEngine;
using System.Collections;

public class Target : MonoBehaviour 
{
    public GameObject hero;     	// this is the hero object
	private Hero heroScript;    	// this is the heros script
	public bool heroAlive = true;	// is the hero alive
	RaycastHit hit;					// the ray that is casted in to the scene
	GravityGun gravGun;				// the grav gun script
	bool gravGunning = false;		// is he grav gunning

    public void Awake()
    {
		heroAlive = true;
        // gain access to the hero's code
        heroScript = hero.GetComponent<Hero>();
		gravGun = this.GetComponent<GravityGun>();
	}

    public void Update()
    {
        // if the game is running on an tablet
		
		if(Input.multiTouchEnabled)
		{
			if(Input.touchCount == 0)
				noTouch();
			else if(Input.touchCount == 1)
				oneFingerTouch();
			else if(Input.touchCount == 2)
				twoFingerTouch();
		}
		
		// if the game is running on a PC
		else
		{
			if (Input.GetButtonDown("Fire1") & !gravGunning)
        	{
            	oneFingerTouch();
        	}
			else if(gravGunning && Input.GetButton("Fire1"))
			{
				gravityGunActive();
			}
			else if(gravGunning)
			{
				gravGunning = false;
				gravGun.Deactivate();
				heroScript.moveComplete();
			}
		}

   }

   
	public void noTouch()
	{
		//heroScript.determineAction("noInput", null);	
	}
	
	public void oneFingerTouch()
	{
		if(heroAlive)
		{
			// Reset the rayHit
        	hit.Equals(null);
        	// cast the ray
        	Ray ray = Camera.allCameras[0].ScreenPointToRay(Input.mousePosition);

        	// see it the ray hits anything
        	if (Physics.Raycast(ray, out hit, 10000))
        	{
				string whatIHit = hit.collider.gameObject.tag;
				// diferent cases for what the ray hits
            	//Debug.Log(whatIHit);
				
				// ENEMY
				if (whatIHit == "Enemy")
				{
					// move the target
                	transform.position = new Vector3(hit.point.x, hit.point.y, 0.0f);
					Debug.Log("touching Enemy");
					heroScript.determineAction("enemy", hit.collider.gameObject);
				}
				// GROUND
            	else if (whatIHit == "Targetable")
            	{
					
					// move the target
                	transform.position = new Vector3(hit.point.x, hit.point.y, 0.0f);
                	// tell the hero we have touched the ground
                	heroScript.determineAction("ground", null);
				}

            	// Interactive
            	else if (whatIHit == "Interactive")
            	{
					Debug.Log("Interact!");
					
                	// move the target
                	transform.position = new Vector3(hit.point.x, hit.point.y, 0.0f);
                	// get him talking
                	heroScript.determineAction("interact", hit.collider.gameObject);
            	}
				
				// Moveabel
				else if (whatIHit == "Movable")
				{
					// move the target
					transform.position = new Vector3(hit.point.x, hit.point.y, 0.0f);
					heroScript.determineAction("moveObject", hit.collider.gameObject);
					gravGun.Activate(this.gameObject, hit.collider.gameObject);
					gravGunning = true;
				}

            	// FINGER IS HELD
            	else
            	{
                	// tell the hero th efinger is held
                	heroScript.determineAction("held", null);
            	}
			}
		}
		else
		{
			// if the hero is dead, a touch will relead the level
			ReloadLevel();
		}
	}
	
	public void twoFingerTouch()
	{
		heroScript.determineAction("freeAttack", null);
	}
	
	void gravityGunActive()
	{
		hit.Equals(null);
        // cast the ray
        Ray ray = Camera.allCameras[0].ScreenPointToRay(Input.mousePosition);
		if (Physics.Raycast(ray, out hit, 10000))
        {
			transform.position = new Vector3(hit.point.x, hit.point.y, 0.0f);
		}
	}
	
	void HeroDied()
	{
		// this is fired form Hero Health
		heroAlive = false;	
	}
	
	
	void ReloadLevel()
	{
		Application.LoadLevel(Application.loadedLevel);	
	}
	
    
}
