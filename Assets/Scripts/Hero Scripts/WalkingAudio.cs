using UnityEngine;
using System.Collections;

public class WalkingAudio : MonoBehaviour {
	
	public AudioClip[] GrassSteps; // contains grass step sounds	
	public AudioClip[] StoneSteps; // contains stone step sounds
	public AudioClip[] GravelSteps; // contains gravel step sounds
	
	public static bool isMoving = false; // is the character moving?
	
	public int firstFootfall; // the frame number where the first foot hits the ground
	
	public int secondFootfall; // the frame number where the second foot hits the ground
	
	public int framesInAnimation; // the amount of frames in the animation
	
	public enum GroundType {Grass, Stone, Gravel}; // what kind of surface is the character on
	
	private int currentFrame; // the current frame
	
	AudioClip[][] AudioArrayHolder = new AudioClip[3][]; // create an array to hold the three different audio arrays
	
	private int surface;	// 
	
	public GroundType ground;
	
	public float audioMax;
	
	public float audioMin;
	
	// Use this for initialization
	void Start () {
		currentFrame = 1;
		AudioArrayHolder[0] = GrassSteps;
		AudioArrayHolder[1] = StoneSteps;
		AudioArrayHolder[2] = GravelSteps;
		
		if (ground == null){
		ground = GroundType.Stone;
		}
		
	}
	
	// Update is called once per frame
	void Update () {
		
		
		//what kind of ground is the character on?
		switch (ground){
			
		case GroundType.Grass:
			surface = 0;
		break;
			
		case GroundType.Stone:
			surface = 1;
		break;
			
		case GroundType.Gravel:
			surface = 2;	
		break;
		}
		
		
		// if the character is moving play the sounds
		if (isMoving){
			
			currentFrame++;
			
			//play the first footfall on the relevant frame
			if (currentFrame == firstFootfall){
				audio.PlayOneShot(AudioArrayHolder[surface][Random.Range(0, (AudioArrayHolder[surface].Length-1))]);	
				audio.volume = Random.Range(0.4F,0.6F );
				Debug.Log("playing footf1");
			}
			
			//play the second footfall on the relevant frame
			if (currentFrame == secondFootfall){
				audio.PlayOneShot(AudioArrayHolder[surface][Random.Range(0, (AudioArrayHolder[surface].Length-1))]);	
				audio.volume = Random.Range(0.4F,0.6F );
				Debug.Log("playing footf2");
			}
			// if we have reached the end of the animation end go back to the start
			if (currentFrame == framesInAnimation) {
				currentFrame = 1;	
			}
		}
	}
	
}
