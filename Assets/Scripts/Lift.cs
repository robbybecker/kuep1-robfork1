/// <summary>
/// This is for the lifts and elivators in the game
/// </summary>

using UnityEngine;
using System.Collections;

public class Lift : MonoBehaviour {

	public Transform Destination;	// where the lift is going to
	public Transform theHero;		// the hero
	
	public float Speed = 2.0f;		// how fast the lift should move
	
	public bool Active = false;		// is it moving
	public bool LoadOnArive = false;// should a new level be loaded when it gets where its going
	public string LevelToLoad;		// the level to be loaded
	
	void Update()
	{
		// if the lift is active, move it
		if (Active)
		{
			Move();
		}
	}
	
	void Move()
	{
		// this moves the lift
		
		//if the lift has not arrived, move it
		if (Vector3.Distance(Destination.position, transform.position) > 2.0f)
			this.transform.Translate((Destination.position - transform.position).normalized * Speed * Time.deltaTime);	
		else
		{
			//the lift has arrived
			if(theHero != null)
				theHero.parent = null; 				// remove the hero
			
			if(LoadOnArive)
				Application.LoadLevel(LevelToLoad);	// load the level
			
			this.enabled = false;					// disable the lift
		}
	}
	
	// check if the herp has entered
	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Hero")
		{
			// if the hero enters, activate the lift
			theHero = other.transform;
			theHero.parent = transform;
			Active = true;
		}
	}
	
}
