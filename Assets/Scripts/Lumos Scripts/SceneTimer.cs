using UnityEngine;
using System.Collections;

public class SceneTimer : MonoBehaviour {
	
	private float startTime;
	
	// Use this for initialization
	void Start () {
		startTime = Time.time;
		Debug.Log("Lumos Call Start");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnDisable () {
		Lumos.Event("Scene Timer", Time.time - startTime);
	}
}
