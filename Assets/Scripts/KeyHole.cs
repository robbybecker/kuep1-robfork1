/// <summary>
/// This is a component for objects that act like a key hole
/// An Object that need somthing droped on it to trigger an event
/// </summary>

using UnityEngine;
using System.Collections;

public class KeyHole : MonoBehaviour {
	
	public GameObject Key;				// the object that triggers the event, must be set in the editor
	public GameObject ThingToTrigger;	// the object that the KeyHold activates
	public string MessageToSend;
	
	public bool PanToTarget = false;	// should the camera pan
	
	private bool KeyInLock = false;
	
	
	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject == Key)
		{
			KeyInLock = true;
		}
	}
	
	void OnTriggerExit(Collider other)
	{
		if(other.gameObject == Key)
		{
			KeyInLock = false;	
		}
	}
	
	void Dropped()
	{
		if(KeyInLock)
		{
		Debug.Log("Key Is in the Hole");	
			ThingToTrigger.BroadcastMessage(MessageToSend);
			if(PanToTarget)
				Camera.mainCamera.BroadcastMessage("StartPan", ThingToTrigger);
		}
	}
	
	
}
