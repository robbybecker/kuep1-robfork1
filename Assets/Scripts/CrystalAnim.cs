// this is the animation controll for the morigans crtysat

using UnityEngine;
using System.Collections;

public class CrystalAnim : MonoBehaviour {
	
	PackedSprite ps;				// the sprite
	public GameObject PowerStrip; 	// the power strip that leads form the crystal to the morigan
	
	// Use this for initialization
	void Start () 
	{
		ps = this.GetComponent<PackedSprite>();		// access the sprite
	}
	
	void Die()
	{
		ps.PlayAnim(0);							// play the death animation
		PowerStrip.BroadcastMessage("Switch");	// change the assets of the powerstrip
	}
	
	
}
