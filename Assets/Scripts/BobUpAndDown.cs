/// <summary>
/// This class makes an object bob up and down 
/// </summary>


using UnityEngine;
using System.Collections;

public class BobUpAndDown : MonoBehaviour {
	
	public float BobAmount = 1f;
	
	// Update is called once per frame
	void Update () 
	{
		transform.position = new Vector3(this.transform.position.x, 7.5f + (Mathf.PingPong(Time.time, BobAmount)), this.transform.position.z);
	}
}
