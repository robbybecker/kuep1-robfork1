using UnityEngine;
using System.Collections;

public class TurtleHitBox : MonoBehaviour {

	public void TakeDamage(int damage)
	{
		SendMessageUpwards("Hit");
	}
}
