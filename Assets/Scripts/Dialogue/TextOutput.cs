//////////////////////////////
/// This is a set of experiments that will form the basis of our dialogue system
/// Th eobjective is to create a system that 
/// 1 - reads in a XML File
/// 2 - creates a dictionary
/// 3 - outputs the contents of that dictionary to GUI emelemts
/// ////////////////////////


using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;



public class TextOutput : MonoBehaviour 
{
	public TextAsset xmlFileName; 
    public string textToDisplay;            // the string that will output in each step
    public int currentLineId;               // the ID of the line to output
    XmlDocument theXML = new XmlDocument(); // the XML file that containe the dialogue for the scene 
	public object varToUse; 				// variable to manage which section of text to use

    // the dictionary the will hold the contents of the XML
    public Dictionary<int, string> theDictionary = new Dictionary<int, string>(); // Dictionary <, Line Number, Line>
    public bool Display;
	
	public bool oldDude;
	private int sectionNumber;
    

    void Awake()
    {
		if (oldDude){
				sectionNumber = GameVars.OldGuyDialogue;
				Debug.Log("sectionNum = "+sectionNumber);
		} else {
			sectionNumber = 0;	
		}
       readInXML(xmlFileName);    // read in the XML
        writeDictionary();                      // build a Dictionary out of the XML content
        currentLineId = 1;
        textToDisplay = theDictionary[currentLineId];       // retrive the line of dialogue to output form the dictionary
        Display = false;	
		//StartTalking();
    }
	
	

    
    
    void readInXML(TextAsset path)
    {
        theXML.LoadXml(path.text);  // load in the contents of the file
    }


    
    
    void writeDictionary()
    {
		XmlNode root = theXML.FirstChild; 				// get the root element
		XmlNode theSection = root.ChildNodes[sectionNumber];		//selects the appropriate section TO DO- Add variable that can be set in the editor
		XmlNodeList theLines = theSection.ChildNodes; 	// selects all child nodes of the section
        for (int i=0; i<theLines.Count; i++ ){  		//NOTE FOR OWEN - I changed this so there is no need for the id attribute (less chance for errors with doubled numbers etc)
			
			string line = theLines[i].InnerText;		//get the text contained in the current element
			theDictionary.Add( ( i+1 ) , line);			//add it to the dictionary
		}
		
		/*foreach (XmlNode dialogueLine in theLines)                  // step through each of them
        {
            int i = int.Parse(dialogueLine.Attributes["id"].Value); // retrieve the line ID, convert it to an interger
            string line = dialogueLine.InnerText;                   // retrieve the string to be spoken

            theDictionary.Add(i, line);                             // add the id and dialogue line to the dictionary
        }*/
		
		for (int i = 1; i <= theDictionary.Count; i++)
		{
			Debug.Log("Trying to build the array");
			BroadcastMessage("BuildArray", theDictionary[i]);
		}
		BroadcastMessage("ArrayComplete");
    }
    
    void nextLine()
    {
        //this is to access the next line of dialogue

        
        currentLineId++;                                    // go to the next line
        if (currentLineId <= theDictionary.Count)           // look to see if there is another line
            textToDisplay = theDictionary[currentLineId];   // if there is, set it to the output variable
        else
        {
            Display = false; ;          // if there are no more lines in the dictionary, outout this instead
        }
    }

    /*void Update()
    {
        //TODO: Move this to target
        if (Input.anyKeyDown && Display)   // press any button
            nextLine();         // go to next line
    }
 
    void OnGUI()
    {
        if(Display)
            GUI.Box(new Rect(10, 10, 400, 50), textToDisplay);        // Display the text
        
    }
    */
}