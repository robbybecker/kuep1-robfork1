//images
var bgImage: Texture;
//Positioning
var dialoguePos: Rect;
var textX: float;
var textY: int;
var textWidth: int;
var textHeight: int;
private var textPadding: Rect;
var nextButtonPlace: Rect;
var prevButtonPlace: Rect;
var firstOptionPlace: Rect;
var secondOptionPlace: Rect;
private var screenWidth : int = Screen.width;
private var screenHeight: int = Screen.height;
//Array traversing
var dialogueArray = new Array(); // = [[0, "1 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porta mattis dolor, sed varius lorem volutpat ac. Vestibulum sed odio magna.", false],[1, "2 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porta mattis dolor, sed varius lorem volutpat ac. Vestibulum sed odio magna. ", false], [2, "3 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porta mattis dolor, sed varius lorem volutpat ac. Vestibulum sed odio magna. ", true], [2, "4 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porta mattis dolor, sed varius lorem volutpat ac. Vestibulum sed odio magna. ", true] ];
var dialogueCounter: int = 0;
//Styles
var textStyle:GUIStyle;
var nextButtonStyle :GUIStyle;
var prevButtonStyle :GUIStyle;
var buttonStyle : GUIStyle;

private var yDestination : float;
public var initializeAnimation : boolean = false;
private var exitAnimation : boolean = false;
private var enterAnimation : boolean = false;
private var origTextPlace : Rect;
private var followFinger : boolean = false;
private var textOffset : float;
private var springSpeed : float = 0;
private var showNav : boolean = true;
private var arrayComplete : boolean = false;
private var touched : boolean = false;
private var finishedAnimating :boolean = false;
//debug
//private var testString : String = "not set in Arraycomplete";

private var tweenObject:  GameObject ;

function BuildArray (lineOfText) 
{
	
	dialogueArray.Add(lineOfText);

}

function ArrayComplete()
{
	arrayComplete = true;
	print("The Array is Complete");
	//print(dialogueArray);
	//testString = "set in Arraycomplete";
}


    // Use this for initialization
function Start () 
    {
        tweenObject = new GameObject();
        tweenObject.transform.localPosition.x = textX;
    	tweenObject.transform.localPosition.y = textY;
    }

function Awake(){
	//set destination positions for dialogue/text
	textPadding = Rect(textX, textY, textWidth, textHeight);
	yDestination = dialoguePos.y;
	dialoguePos.y = screenHeight;
	origTextPlace = textPadding;
}

function Update () {
	//animate the dialogue in (488) sets the speed
	if (dialoguePos.y > yDestination - 488 * Time.deltaTime && initializeAnimation && arrayComplete){
		dialoguePos.y -= 488 * Time.deltaTime;
			
	} else if (initializeAnimation){
		dialoguePos.y = yDestination;
		initializeAnimation = false;
		IsPaused.isPaused = true; 
		finishedAnimating = true;
	}
	
	//animate the dialogue out
	if (dialoguePos.y < screenHeight + 488 * Time.deltaTime && exitAnimation){
		dialoguePos.y += 488 * Time.deltaTime;
	} else if (exitAnimation){
		dialoguePos.y = screenHeight;
		IsPaused.isPaused = false; 
		exitAnimation = false;
		Debug.Log("init:"+initializeAnimation);
		//Destroy(this);
	}
	
	//text follows finger if held down 
	if (Input.GetMouseButtonDown(0) && Rect(origTextPlace.x, screenHeight - textHeight - textY - dialoguePos.y ,textWidth,textHeight).Contains(Input.mousePosition) ){
		textOffset = Input.mousePosition.x - textX;
		followFinger = true;
		showNav = false;
	}
	
	//if space above text is tapped initialise exit animation
	if (Input.GetMouseButtonDown(0) && Rect(0, screenHeight -(screenHeight-dialoguePos.height), dialoguePos.width, screenHeight - dialoguePos.height).Contains( Input.mousePosition ) && finishedAnimating){
		
			exitAnimation = true;
		
		}
	
	//if finger is released to left or right, move the dialogue forward or back depending on where it was released
	if (Input.GetMouseButtonUp(0) && followFinger){
		followFinger = false;
		showNav = true;
		
		//textX = Mathf.Lerp(textX, origTextPlace.x, 0.5); //Time.deltaTime * springSpeed
		//var testLerp = Mathf.Lerp(textX, origTextPlace.x, 0.5);
		//Debug.Log(testLerp);
		if (Rect(0, screenHeight - dialoguePos.y - dialoguePos.height, 270, dialoguePos.height).Contains(Input.mousePosition) && dialogueCounter + 2 <= dialogueArray.length ){ 
			dialogueCounter++;
		}
		if (Rect(screenWidth - 270, screenHeight - dialoguePos.y - dialoguePos.height, 270, dialoguePos.height ).Contains(Input.mousePosition) && dialogueCounter - 1 >=0 ){
			dialogueCounter--;
		}
		var position :float = origTextPlace.x - (screenWidth)*dialogueCounter;
		animateTo(position);
		//Ani.Mate.To(this, 1, {"textX": position , "easing": Ani.Easing.Elastic, "direction":Ani.Easing.Out});
	}
	//make sure the text stays in the same position relation to the users finger
	if (followFinger){
		tweenObject.transform.localPosition.x = Input.mousePosition.x - textOffset;
		textX = tweenObject.transform.localPosition.x;
	}
	
}

function DisplayText()
{
	Debug.Log("I'm trying");
	Debug.Log(""+dialoguePos.y);
	initializeAnimation = true;
}

function OnGUI (){
	if (!followFinger){
	//saving xPos/yPos for iTween
	 textX = tweenObject.transform.localPosition.x;
     textY = tweenObject.transform.localPosition.y;
	}
	//set group for ease of animation
	GUI.BeginGroup(dialoguePos);
		if (arrayComplete){
			//background image
			GUI.DrawTexture(Rect(0,0,dialoguePos.width, dialoguePos.height), bgImage, ScaleMode.ScaleAndCrop);
			//position each text item
			for (var i=0; i<dialogueArray.length; i++){
				var position : float;
		        if (i == 0){
			       	position = textX ;//- (screenWidth)*dialogueCounter;
			      } else {
			       position = ( (i * screenWidth )+textX );//-(screenWidth)*dialogueCounter;
			      }
		       GUI.Label(Rect(position, textY, textWidth, textHeight) ,dialogueArray[i].ToString() ,textStyle);
		       
		    }
	    }
		//add buttons
		/*if (dialogueArray[dialogueCounter][2]==true){
			if (GUI.Button(firstOptionPlace, "Yes", buttonStyle)){
				Debug.Log("First Button Clicked");	
			}
			if (GUI.Button(secondOptionPlace, "No", buttonStyle)){
				Debug.Log("Second Button Clicked");	
			}
		}*/
		//if there is a next text item show the next button
		if (dialogueCounter +2 <= dialogueArray.length && showNav){
			if (GUI.Button(nextButtonPlace, "", nextButtonStyle)){
				dialogueCounter++;
				var position1:float = origTextPlace.x - (screenWidth)*dialogueCounter;
				animateTo(position1);
			}
		}
		//if there's aprev text item to show, show the previous button
		if (dialogueCounter - 1 >=0 && showNav){
			if (GUI.Button(prevButtonPlace, "", prevButtonStyle)){
				dialogueCounter--;
				var position2:float = origTextPlace.x - (screenWidth)*dialogueCounter;
				animateTo(position2);
			}
		}
	GUI.EndGroup(); 
}

function animateTo (xValue : int) {
	iTween.MoveTo(tweenObject, {"x": xValue, "time": 0.5});
}

function UpdateFunction(val:int){
	textX = val;
	Debug.Log("update "+val);
}

function BeginDialogue(){

}