using UnityEngine;
using System.Collections;

public class Morrigan : MonoBehaviour 
{

	public enum Phase { Alseep, Laser, Blast, Crows };
	public Phase currentPhase = Phase.Laser;
	
	private MorriganAttack ma;
	public Collider MyCollider;
	
	private EnemyDamage healthScript;
	public GameObject theHero;
	void Start()
	{
		ma = GetComponent<MorriganAttack>();	
		healthScript = MyCollider.GetComponent<EnemyDamage>();
		MyCollider.enabled = false;
		
		if (theHero == null)
			theHero = GameObject.FindGameObjectWithTag("Hero");
	}
	
	void Update()
	{
		switch(currentPhase)
		{
		case Phase.Alseep:
			if(Vector3.Distance(transform.position, theHero.transform.position) < 20)
				currentPhase = Morrigan.Phase.Laser;
			else
				Debug.Log(Vector3.Distance(transform.position, theHero.transform.position));
			break;
		case Phase.Laser:
			ma.Laser();
			break;
		case Phase.Blast:
			ma.Blast();
			break;
		case Phase.Crows:
			ma.Crow();
			break;
		}
		
		if (healthScript.Health < 20)
			currentPhase = Morrigan.Phase.Crows;
		else if(healthScript.Health < 40)
			currentPhase = Morrigan.Phase.Blast;
	}
}
