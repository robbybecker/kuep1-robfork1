using UnityEngine;
using System.Collections;

public class SparkoBotSpawner : MonoBehaviour {
	
	public bool activate = false;
	public GameObject Holder;
	
	public Collider liftCollider;
	public GameObject theLift;
	
	void Awake()
	{
		Holder.SetActiveRecursively(false);	
		Debug.Log( Holder.transform.childCount);
	}
	
	void Go()
	{
		activate = true;	
	}
	
	void Update()
	{
		if(activate)
		{
			
			Activate();
			
		}	
		
		if (Holder.transform.childCount == 0)
		{
			// all the botts are dead
			liftCollider.enabled = true;
			theLift.GetComponent<PackedSprite>().DoAnim(0);
		}
	}
	
	void Activate()
	{
		
		Holder.SetActiveRecursively(true);
		Holder.active = true;
		if(Vector3.Distance(transform.position, Holder.transform.position) > 1)
			MoveEm();
		else
			ActivateEm();
			
	}
	
	
	void MoveEm()
	{
		Debug.Log("Activating");
		Holder.transform.position += (transform.position	- Holder.transform.position) * Time.deltaTime * 5;
	}
	
	void ActivateEm()
	{
		Holder.SetActiveRecursively(true);
		activate = false;
	}
}
