using UnityEngine;
using System.Collections;

public class LaserReciever : MonoBehaviour 
{
	PackedSprite packedSprite;
	
	void Awake()
	{
		// get a reference to the PS script
        packedSprite = this.GetComponent<PackedSprite>();	
		
	}
	
	void Animate()
	{audio.Play();
		// this is called from the laser emiter
		packedSprite.PlayAnim("on");
		
	}
	
}
