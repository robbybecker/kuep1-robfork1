using UnityEngine;
using System.Collections;

public class TurtleAnimation : MonoBehaviour {

	PackedSprite packedSprite;
	
	void Awake () 
	{
		packedSprite = this.GetComponent<PackedSprite>();
	}
	
	void WeAreHit()
	{
		packedSprite.DoAnim("down");	
	}
	
	void WeHaveRecovered()
	{
		packedSprite.DoAnim("walk");	
	}
}
