using UnityEngine;
using System.Collections;

public class Turtle : MonoBehaviour {

	
	public GameObject wayPoint1;		// waypoint, the turtle moves between these
	public GameObject wayPoint2;		// waypoint
	
	
	private GameObject currentWaypoint;	// the current destination, used to figure what destination to switch to
	
	private Vector3 destination;		// the position of the current destination
	public float speed = 0.1f;			// the movement speed
	
	private bool walking = true;		// should the dood me moving
	private bool notRecovering = false;		// RALPH: bool for audio/pause logic
	
	public GameObject LaserEmiter;		// the thing that shoots the lasers
	
	public AudioClip prisomPop;			// sound of the prisim popping up
	
	// Use this for initialization
	void Start () 
	{
		// set up the destination
		
		currentWaypoint = wayPoint1;
		destination = currentWaypoint.transform.position;
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!IsPaused.isPaused){
		if(walking){
			Walk();
			if(!audio.isPlaying && !notRecovering)
				audio.Play();
			} else {
				audio.Stop();	
			}
		}
	}
	
	private void Walk()
	{
		// calculate the moveVector
		Vector3 moveVector = destination - this.transform.position;
		
		// see if we have arived at the waypoint
		if(moveVector.magnitude < 0.5f)
			changeWaypoint();		// if we have arived, change waypoint 
		else
		{
			// apply speed and move the dood
			moveVector.Normalize();	
			moveVector *= speed;
		
			transform.Translate(moveVector);
		}
	}
	
	private void changeWaypoint()
	{
		if(currentWaypoint == wayPoint1)
		{
			currentWaypoint = wayPoint2;
			destination = currentWaypoint.transform.position;
		}
		else if(currentWaypoint == wayPoint2)
		{
			currentWaypoint = wayPoint1;
			destination = currentWaypoint.transform.position;
		}
	}
	
	// these functions are used to change states
	// they broadcast messages to the animation class
	void Hit()
	{
		
		audio.Stop();
		audio.PlayOneShot(prisomPop);
		walking = false;
		if (!notRecovering){
			BroadcastMessage("WeAreHit");
		}
		StartCoroutine("Recover");
		
		LaserEmiter.BroadcastMessage("checkSolution");
		Lumos.Event("hit turtle", 1);
	}
	
	IEnumerator Recover()
	{
		
		yield return new WaitForSeconds(2);
		walking = true;
		
		BroadcastMessage("WeHaveRecovered");
		
	}
	
	void doNotRecover()
	{
		notRecovering = true;
		StopCoroutine("Recover");	
		
	}
}
