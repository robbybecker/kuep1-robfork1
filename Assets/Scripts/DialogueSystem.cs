using UnityEngine;
using System.Collections;

public class DialogueSystem : MonoBehaviour 
{
    public TextAsset sampleText;
    public GameObject Ku;
    public string lineOfDialogue;
    public bool ShowText = true;
    public Rect textBox;

    public bool nextLineTest = false;

    public float tweek;

  

	// Use this for initialization
	void Start () 
    {
        

        textBox = new Rect(10f, 10f, 100f, 20f);
        lineOfDialogue = "This is the first line";


	}

    void Update()
    {
        if (nextLineTest)
            nextLine();
    }
	
    public void nextLine()
    {
        nextLineTest = false;



        textBox = calculateBoxPosition(Ku);
        lineOfDialogue = "This is the next line";

    }

    public void OnGUI()
    {
        if(ShowText)
            GUI.Box(textBox, lineOfDialogue);
        
    }

    Rect calculateBoxPosition(GameObject speaker)
    {
        Vector3 screenPosition = Camera.mainCamera.WorldToScreenPoint(speaker.transform.position);

        screenPosition.y += tweek;

        return new Rect(screenPosition.x, screenPosition.y, 100, 50);
    }

    
}
