using UnityEngine;
using System.Collections;

public class MorriganCrow : MonoBehaviour {
	
	public int Damage = 1;
	public float Speed = 1f;
	
	public AudioClip crowSound;
	
	private Vector3 startPos;
	private Quaternion startRot;
	private bool flightSound =true;
	public bool enabled = false;
	

	
	void Start()
	{
		startPos = transform.position;	
		startRot = transform.rotation;
		
		enabled= false;
	}
	
	// TODO: steer around obsticales
	
	void Enable()
	{
		enabled = true;	
	}
	
	void Update()
	{
		if(enabled){
			transform.Translate(Vector3.down * Speed * Time.deltaTime);
			if (flightSound){
				audio.PlayOneShot(crowSound);
				flightSound = false;
			}
		}
		if(transform.position.y < -30f){
			Reset();
		}
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Hero")
		{
			other.BroadcastMessage("takeDamage", Damage);
		}
		else if(other.tag == "Debris")
			Reset();
	}
	
	void Reset()
	{
		transform.position = startPos;
		transform.rotation = startRot;
		flightSound=true;
		enabled = false;
	}
}
