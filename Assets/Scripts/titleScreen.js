var logo:Texture2D;
var titleScreenImage: Texture2D;
var titleSound: AudioClip;
var startScreenSound: AudioClip;
var buttonStyle:GUIStyle;
private var textureAlpha:Color;
private var fade:float = 1;
private var fade1:float = 1;
private var showLogo = false;
private var screenWidth = Screen.width;
private var screenHeight = Screen.height;
var bgImageX: int = 0;
var bgImageY: int ;
var bgImageWidth: int = 1024;
var bgImageHeight: int = 1556;
var button1Rect: Rect = Rect(192, 574, 100, 60);
var button2Rect: Rect = Rect(690, 574, 100, 60);;
private var imageRect:Rect;
private var logoRect:Rect = Rect(screenWidth/2-549/2,10,549, 532 );
private var showButtons:boolean = false;
var startTime : float;


function Awake()
{
	startTime = Time.time;
    StopCoroutine("FadeCoroutine");
    StartCoroutine("FadeCoroutine");
}

function Update () {
imageRect = Rect(bgImageX, bgImageY, bgImageWidth, bgImageHeight);
	if(bgImageY != -388){
		bgImageY++;
	}
	if(Input.GetMouseButtonUp(0)){
		fade = 1;
		fade1 = 1;
		if (showButtons){
			Lumos.Event("Skipped title", 1);
		}
		showButtons = true;
		
	}
	 //Ani.Mate.To(this, 5, {"bgImageY": -388 , "easing": Ani.Easing.Quadratic, "direction":Ani.Easing.Out});
	 //Debug.Log("bgy"+bgImageY);
}

function OnGUI(){
 if (fade > 0)
    {
        GUI.color = Color.white;
        GUI.color.a = fade;
        GUI.DrawTexture(imageRect, titleScreenImage, ScaleMode.ScaleAndCrop);
        GUI.color = Color.white;
        
    }
    if (fade1 > 0){
    	GUI.color = Color.white;
        GUI.color.a = fade1;
        GUI.DrawTexture(logoRect, logo, ScaleMode.ScaleAndCrop);
        GUI.color = Color.white;
    }
    if (showButtons){
    	if (GUI.Button(button2Rect, "Start", buttonStyle ) ){
    		//load first scene	
    		Lumos.Event("Time to start Game on intro", Time.time - startTime);
			Application.LoadLevel("start 1");
		}
    	
    	if (GUI.Button(button1Rect, "Tutorial", buttonStyle ) ){
    		Lumos.Event("Did Tutorial", 1);
    		//load tutorial scene	
    		Application.LoadLevel("tutorial");
    	}
    }
   
}

function FadeCoroutine()
{
    while(true)
    {
        fade = 0;
        fade1 = 0;
        var time : float = 0;
        audio.Play();
        while (time < 2)
        {
            yield;
            fade += Time.deltaTime / 2;
            time += Time.deltaTime;
        }
        fade = 1;
        time = 0;
        
        yield WaitForSeconds(4);
            while (time < 5)
        {
            yield;
            fade1 += Time.deltaTime / 5;
            time += Time.deltaTime;
        }
        
        yield WaitForSeconds(0.5);
        showButtons = true;
        yield WaitForSeconds(13000);
        
    }
}
