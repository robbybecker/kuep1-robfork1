using UnityEngine;
using System.Collections;

public class MorriganTarget : MonoBehaviour {
	
	public GameObject theHero;
	public bool Active = false;
	public float Speed = 5f;
	
	public enum Phase { Laser, ResetLaser, Blast, Crows, none }
	public Phase currentPhase = Phase.none;
	
	public AudioClip laserSound;
	private PackedSprite ps;
	
	void Start () 
	{
		if(theHero == null)
			theHero = GameObject.FindGameObjectWithTag("Hero");
		ps = GetComponent<PackedSprite>();
		ps.Hide(true);
	}
	
	// Update is called once per frame
	void Update () 
	{
		switch (currentPhase)
		{
		case Phase.Laser:
			TargetHero();
			break;
		case Phase.ResetLaser:
			transform.position = transform.parent.position;
			currentPhase = Phase.none;
			audio.Stop();
			break;
		default:
			audio.clip = null;
			break;
		}
	}
	
	void TargetHero()
	{	
		Vector3 moveVector = (theHero.transform.position - transform.position).normalized;
		moveVector *= Speed * Time.deltaTime;
		
		//play the sound of the laser
		audio.clip = laserSound;
		audio.loop = true; 
		audio.Play();
		
		transform.position += moveVector;
	}
	
	public void SnapToHero()
	{
		transform.position = theHero.transform.position;
		
	}
	
	public void PlayAnimation(string AnimationName)
	{
		ps.Hide(false);
		ps.DoAnim(AnimationName);
	}
}
