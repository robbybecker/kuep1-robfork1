using UnityEngine;
using System.Collections;

public class MorriganAnimation : MonoBehaviour {

	
	private SuperSprite sprite;
	
	void Start()
	{
		sprite = GetComponent<SuperSprite>();
	}
	
	public void Hurt()
	{
		sprite.DoAnim("Hurt");
	}
	
	void Die()
	{
		sprite.PlayAnim("Death");
	}
}
