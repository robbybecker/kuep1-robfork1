using UnityEngine;
using System.Collections;

public class Gate : MonoBehaviour {

	private PackedSprite packedSprite;
	private Vector3 startPos;
	
	// Use this for initialization
	void Awake () 
	{
		packedSprite = this.GetComponent<PackedSprite>();
		startPos = transform.position;
	}
	
	// Update is called once per frame
	public void OpenGate () 
	{
        audio.Play();               // play the gate open sound
		packedSprite.PlayAnim(0);   // play the gate open animation
		transform.position = new Vector3(startPos.x, startPos.y, -70f);
	}
}
