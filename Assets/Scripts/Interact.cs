/// <summary>
/// This is a script to drop on interactive objects
/// Set the thingToCall in the editor to the name of the function
/// For the interactivity
/// </summary>

using UnityEngine;
using System.Collections;

public class Interact : MonoBehaviour {
	
	public enum Interactions {PanToMe = 0, StartTalking = 1, ShowImage = 2}
	public Interactions thingToDo;
	
	void DoStuff()
	{
		// TODO: Do other interactive stuff from here
		Debug.Log("Yo Dawg!");
		
		if(thingToDo != null)
			BroadcastMessage(thingToDo.ToString());
	}
}
