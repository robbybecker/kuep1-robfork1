using UnityEngine;
using System.Collections;

public class CameraCode : MonoBehaviour {

    public GameObject target;

    private float zPos;             // this is how far back the camera shouls be from the game


    // these are the limits for the camera movement of a given scene
    // beyond these, the camera will stop following the hero
    // they should eb set in the editor

    public float leftLimit = -14f;  
    public float rightLimit = 20f;  
    public float topLimit = 18f;
    public float botomLimit = -10f;
	
	public enum States { follow = 0, panTo = 1, panBack = 2}
	public States state = CameraCode.States.follow; 
	
	private GameObject panTarget; // the thing to pan to
    
	public float PanSpeed = 60f;
     
	// Use this for initialization
	void Start () 
    {
        // set up teh zposition
        zPos = transform.position.z;
		
		
		
		
	}
	
	// Update is called once per frame
	void Update () 
    {
        switch(state)
		{
		case States.follow :
				Follow();
			break;
		case States.panTo :
				Pan();
			break;
		case States.panBack :
				PanBack();
			break;
		}
                                                                    
        
	}
	
	void Follow()
	{
		transform.position = new Vector3(
            Mathf.Clamp(target.transform.position.x, leftLimit, rightLimit),    // this clamps the x axis 
            Mathf.Clamp(target.transform.position.y, botomLimit, topLimit),     // this clamps the y axis
            zPos);  
	}
	
	void Pan()
	{
		
		//Vector3 movementVector = new Vector3(panTarget.transform.position.x, panTarget.transform.position.y, this.transform.position.z) - this.transform.position;
		
		Vector3 movementVector = (panTarget.transform.position - transform.position).normalized;
		movementVector = new Vector3(movementVector.x, movementVector.y, 0f);
		movementVector *= PanSpeed * Time.deltaTime;
		
		if(Vector3.Distance(new Vector3(transform.position.x, transform.position.y, 0f), new Vector3(panTarget.transform.position.x, panTarget.transform.position.y, 0f)) > 1f)
		{
			//movementVector *= Time.deltaTime;
			//transform.Translate(movementVector.x, movementVector.y, 0f);
			
			transform.position += movementVector;
	  
		}
		else
			state = CameraCode.States.panBack;
		// when you get to the thing
		// move back to the hero
		// when you get to the hero
		// resume following
	}
	
	void PanBack()
	{
		Vector3 movementVector = new Vector3(target.transform.position.x, target.transform.position.y, this.transform.position.z) - this.transform.position;
		
		if(Vector3.Magnitude(movementVector) > 1f)
		{
			movementVector *= Time.deltaTime;
			transform.Translate(movementVector.x, movementVector.y, 0f);
		}
		else
			state = CameraCode.States.follow;
		// when you get to the thing
		// move back to the hero
		// when you get to the hero
		// resume following
	
	}
	
	public void StartPan(GameObject thingToPanTo)
	{
		state = CameraCode.States.panTo;
		panTarget = thingToPanTo;
	}
	
	// this shake function can be called to initiate a camera shake
	void Shake(float shakeDuration)
	{
		iTween.ShakePosition(Camera.mainCamera.gameObject, Vector3.right, shakeDuration);
	}
}
