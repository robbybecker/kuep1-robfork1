using UnityEngine;
using System.Collections;

public class Trigger : MonoBehaviour {

    // these are the triggers used in simple puzzles 


    public GameObject puzzleManager;    // the object that manages the puzzle, has the solustion 
	public GameObject sprite;           // the sprite
	
	
	public Material TriggerOff;         // this is the texture for the unactivated trigger
	public Material TriggerOn;          // this is the texture for the activated trigegr
	
    PuzzleManager001 puzzleScript;      // this is the code for the puzzle manager
    
    // Use this for initialization
	void Awake () 
    {
        if (puzzleManager != null)
            puzzleScript = puzzleManager.GetComponent<PuzzleManager001>();  // assign the puzzleManager script
        else
            Debug.Log("PuzzleManager not set in the editor");

	}

    void OnTriggerEnter(Collider other)
    {
        // this is run whenever an object steps on the trigger
        if (other.tag == "Hero")                        // if the object is the hero
        {
            puzzleScript.CheckSolution(this.gameObject);// check if the puzzle is solved
			ActivateTrigger();                          // and turn on the trigger
        }
    }
	
	void ActivateTrigger()
	{
        audio.Play();                        // play the sound
        sprite.renderer.material = TriggerOn;// switch the material
	}
	
	void Reset()
	{
		Debug.Log("Trigger resetting");
		sprite.renderer.material = TriggerOff;
	}
}
