using UnityEngine;
using System.Collections;

public class VaultComputer : MonoBehaviour {
	
	private bool showImage = false;
	
	public Texture2D ImageToShow;
	
	void Activate()
	{
		showImage = true;
	}
	
	void OnGUI()
	{
		if(showImage)
		{
			if(GUI.Button(new Rect	(10, 10, ImageToShow.width, ImageToShow.height), ImageToShow))
			{	
				showImage = false;
				Debug.Log("GAME OVER");
			}
		}
	}
}
