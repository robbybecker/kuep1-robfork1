using UnityEngine;
using System.Collections;

public class TrollWeapon : MonoBehaviour 
{
	private bool Attacking = false;	// is the weapon being used
	
	public int Damage = 2;			// the amound of damage applied
	
	void useTheWeapon(float attackAngle)
	{
		// this is called from TrollBehavious
		
		Attacking = true;			// the weapon is being used and will apply damage
		
		moveTheWeapon(attackAngle);
	}
	
	void moveTheWeapon(float Angle)
	{
		if (Angle <=0f)
		{
			if(Angle >= -90f)
			{
				// South East
				
				transform.localPosition = new Vector3(0.357f, -0.306f, 0f);
			}
			else
			{
				// South West 
				
				transform.localPosition = new Vector3(-0.372f, -0.378f, 0f);
			}
		}
		else
		{
			if(Angle <= 90f)
			{
				// North East
				
				transform.localPosition = new Vector3(0.451f, 1.278f, 0f);
			}
			else
			{
				// North West
				
				transform.localPosition = new Vector3(-0.453f, 1.278f, 0f);
				
			}
		}
	}
	
	void OnTriggerEnter(Collider other)
	{
		if (Attacking && other.gameObject.tag == "Hero")
        {
            other.gameObject.BroadcastMessage("takeDamage", Damage);
			Attacking = false;
        }
	}
	
	void putAwayTheWeapon()
	{
		// called from TrollBehavior
		Attacking = false;								// weapon will no longer aply damage
		
		transform.position = transform.parent.position;	// position is reset
		
	}
	
}
