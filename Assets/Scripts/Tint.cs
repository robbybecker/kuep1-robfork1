/// <summary>
/// NOTE: Shader mus be set to particles/Alpha blended
/// </summary>


using UnityEngine;
using System.Collections;

public class Tint : MonoBehaviour {
	
	public Color tint;
	
	// Use this for initialization
	void Start () {
		renderer.material.SetColor("_TintColor", tint);
	}
	
	
}
