//////////////////
//This code is for bullits shot by enemies
/////////////////



using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {


    public GameObject Hero;     // Our Hero, what it should be aiming for
    public float Speed;         // How fast does the bullit travel

    private Vector3 direction;  // The direction the bullit moves in 

    public int Damage = 1;      // How much damage the projectile inflicts
    
    // Use this for initialization
	void Awake () 
    {
        if (Hero == null)                                           // if the hero has net been set in the editor
            Hero = GameObject.FindGameObjectWithTag("Hero");        // find the hero

        Speed = 5f;                                                 // set the initial speed - can be changed in the editor

        direction = Hero.transform.position - transform.position;   // Calculate the direction of the hero
        direction.Normalize();                                      // Normalise the direction

        StartCoroutine(Die());                                      // Begin the countdown to death
	}
	
	// Update is called once per frame
	void Update () 
    {
		if(!IsPaused.isPaused)
        	transform.Translate(direction * Speed * Time.deltaTime);    // Move the bullit 
	}

    void OnTriggerEnter(Collider other)                             // When the bullit hits somthing
    {
        if (other.gameObject.tag == "Hero")                         // If it is the hero
        {
            // Hero Damage code goes here
            Debug.Log("You are being hit");
            other.gameObject.BroadcastMessage("takeDamage", Damage);// Apply damage to the hero          
        }
    }


    IEnumerator Die()
    {
        // this is the countdown to death
        yield return new WaitForSeconds(2); // after 2 seconds
        Destroy(this.gameObject);           // die
    }
}
