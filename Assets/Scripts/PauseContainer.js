private var screenWidth: int = Screen.width;
private var screenHeight:int = Screen.height;
var buttonBG: Texture;
var buttonWidth: int;
var buttonHeight: int;
var background:Texture;
var followFinger:boolean;
private var startPos: int ;
static var menuPos:Rect;
static var objectiveText:String;
var objectiveStyle: GUIStyle;
var healthStyle: GUIStyle;
var currentHealth:int;
var maxHealth: int;
var GUIZindex : int = 0;
//static var isPaused = false;
private var tweenObject:  GameObject ;


function Awake() {
	startPos = 0-screenWidth+buttonWidth-50;
	menuPos = Rect(startPos, 0, screenWidth, screenHeight);
	objectiveText = "Find the temple where the tech lies";
	currentHealth = 87;
	maxHealth = 100;
	DontDestroyOnLoad(this); //persist the pause menu throughout the game
	
}

function Start() {
	//dummy game object to hold x pos for itween
    tweenObject = new GameObject();
    tweenObject.transform.localPosition.x = startPos;
    DontDestroyOnLoad(tweenObject); // persist the tweenobject throughout the game
}

function Update () {
	menuPos = Rect(startPos, 0, screenWidth, screenHeight);
	//Debug.Log("ispaused"+IsPaused.isPaused);
	//when the user clicks on the button, the pause menu follows the finger, and the game is paused
	if ( Input.GetMouseButtonDown(0) && Rect(menuPos.x+screenWidth-buttonWidth,screenHeight/2 - buttonHeight/2,buttonWidth,buttonHeight).Contains(Input.mousePosition) ){
		followFinger = true;
	}
	
	//when the user releases, check where, and either hide the menu and unPause the game, or reset the position
	if (Input.GetMouseButtonUp(0) && followFinger){
		followFinger = false;
		if (Input.mousePosition.x <= screenWidth/2){
			iTween.MoveTo(tweenObject, {"x": 0-screenWidth+buttonWidth-50, "time": 0.5});
			//Ani.Mate.To(this, 0.5, {"startPos": 0-screenWidth+buttonWidth-50 , "easing": Ani.Easing.Quadratic, "direction":Ani.Easing.Out });
			IsPaused.isPaused = false; 
		}
		if (Input.mousePosition.x > screenWidth/2){
			iTween.MoveTo(tweenObject, {"x": 0, "time": 0.5});
			//Ani.Mate.To(this, 0.5, {"startPos": 0 , "easing": Ani.Easing.Quadratic, "direction":Ani.Easing.Out});
		}
	}	
	
	//if follow finger is true set the right hand side of the menu to the current finger/mouse position
	if (followFinger){
		if (Input.mousePosition.x < screenWidth-buttonWidth/2 && Input.mousePosition.x > 0){
			tweenObject.transform.localPosition.x = Input.mousePosition.x-screenWidth+buttonWidth/2;
			startPos = tweenObject.transform.localPosition.x;
			IsPaused.isPaused = true;
		}
	}
}

function OnGUI (){
	if (!followFinger){
		startPos = tweenObject.transform.localPosition.x;
	}
	GUI.depth = GUIZindex;
	GUI.BeginGroup(menuPos);
		GUI.DrawTexture( Rect(0,0 ,screenWidth+1,768), background );
		GUI.DrawTexture( Rect(screenWidth-buttonWidth,screenHeight/2 - buttonHeight/2,buttonWidth,buttonHeight), buttonBG );
		GUI.Label( Rect(583, 138, 340, 71), objectiveText, objectiveStyle );
		GUI.Label( Rect(726, 638, 193, 45), HealthLevel.kuHealth + " / " + HealthLevel.maxHealth, healthStyle );
	GUI.EndGroup();	
} 