//////////////////////////////////
// Code for a melle attack
// 
//////////////////////////////////



using UnityEngine;
using System.Collections;

public class EnemyAttack_SparkoBot: MonoBehaviour 
{
    GameObject Hero;
	public float Range; 
	public AudioClip attackSound;
	
	void Start()
	{
		if(Hero == null)
			Hero = GameObject.FindGameObjectWithTag("Hero");
	}


    // this can be called to fire the projectile
    void Fire(float angle)
    {
        if(Vector3.Distance(Hero.transform.position, transform.position) < Range)
		{
			audio.PlayOneShot(attackSound);
			Hero.BroadcastMessage("takeDamage", 1);
			BroadcastMessage("animateAttack", angle);
		}
		
    }

}
