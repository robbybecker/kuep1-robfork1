//////////////////
/// This is th eobject that is used to apply Attacks
/// When Ku Attacks, it will be moved to the apropriate localion
/// If it collides with an enemy, it will apply damage
///
//////////////////

using UnityEngine;
using System.Collections;

public class HeroWeapon : MonoBehaviour {


    public bool Attacking;      // is the weapon attacking?         
    
    public int Damage = 2;

    public void Awake()
    {
        Attacking = false;              // the weapon is not attacking
        
    }

    public void DoAttack()    // this is the attack code. It is called from hero class
    {
        Attacking = true;   // set attacking to true, this means the weapon is active and can apply damage
        
    }

    void OnTriggerEnter(Collider other) 
    {
        // this code is run whenever the weapon enters another object
		
		
		
        // if the hero is attacking and the object is an enemy
        if (Attacking && other.gameObject.tag == "Enemy")
        {
            other.gameObject.BroadcastMessage("TakeDamage", Damage);
            //TODO: access the enemy damage script on the other Object. apply damage
            Debug.Log("We are hitingthe enemy");
        }
    }

    void attackCompleteResetWeapon()
    {
        // a message is sent form HeroAnimation to Heor when the attack is complete. Hero then calls thei code
        if (Attacking)          // if the weapon is attacking
            Attacking = false;  // stop attacking

        transform.position = transform.parent.position; // move the weapon back to the start position 
    }
}
