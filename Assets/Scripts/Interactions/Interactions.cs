/// <summary>
/// This is a collection of simple interactions
/// </summary>


using UnityEngine;
using System.Collections;

public class Interactions : MonoBehaviour {
	
	// Pans the main camera to the object and back again
	public void PanToMe()
	{
		Camera.mainCamera.BroadcastMessage("StartPan", this.gameObject);
	}
	
	// display dialogue
	public void StartTalking()
	{
		BroadcastMessage("DisplayText");
	}
}
