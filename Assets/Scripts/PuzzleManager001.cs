using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PuzzleManager001 : MonoBehaviour {

    public List<GameObject> triggers = new List<GameObject>();  // the list of triggers
    public List<GameObject> solution = new List<GameObject>();  // the solution, in order
	
	public GameObject Gate;

    public bool solvable = true;        // is the puzzle still solvable
    public bool solved = false;			// has the puzzle been solved

    int solutionStep = 0;               // where are we in the solution

    public GameObject goal;             // this is the object that will be destroyed when the puzzle is solved

    void Awake()
    {
        // make sure the goal is set
        if (goal == null)
            Debug.Log("Goal must be set in the editor");
    }

    public void CheckSolution(GameObject trigger)
    {
        if (!solved && solvable)
        {
            if (solution[solutionStep] == trigger)  // if the correct trigger has been activated
            {
                solutionStep++;                     // move to the next step of the solution
                if (solutionStep == solution.Count) // see if the puzzle has been solved
                {
                    Solved();                       // run the Solved code
                }
            }
			
            else                                    // if the wrong trigger was activated
            {
                solvable = false;                   // set solvable to false
				
            }
        }
		if(!solvable )
		{
			solutionStep++;
			if (solutionStep == solution.Count) // see if the puzzle has been solved
            {
                audio.Play();                     // wrong combo entered
            }
		}
		
    }

    void Solved()
    {
        // this is the code that is run when the puzzle has been solved
        solved = true;  // the puzzle has been solved

        Destroy(goal);  // remove the object blocking the way

		Gate.GetComponent<Gate>().OpenGate();   // play the animation
    }
	
	
	//this should reset the puzzle
	public void Reset()
	{
		for (int i = 0; i < triggers.Count; i++) 
		{
			Debug.Log("Reseting");
			triggers[i].BroadcastMessage("Reset");
		}
		
		solutionStep = 0;
		solvable = true;
	}
}
