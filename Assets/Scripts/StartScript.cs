/////////////
// This object exists at the start of the game before any of the levels are loaded
// It can be used to set up the game before it starts
/////////////


using UnityEngine;
using System.Collections;

public class StartScript : MonoBehaviour {
	
	
	public string FirstLevel;
	
    void Start()
    {
        
		// Load up the first area
        if(FirstLevel != null)
			Application.LoadLevel(FirstLevel);
		else
			Debug.Log("Level not set in editor");
    }
}
