/// <summary>
/// This is for the computer in the R&D room
/// </summary>

using UnityEngine;
using System.Collections;

public class ButtonComputer : MonoBehaviour {
	
	public GameObject ThingToActivate;	// this is the powerstrip
	public string messageToSend;		// this is the function to call in the power strip
	
	public GameObject offState;			// this is the computer's own sprite
	public GameObject onState;			// this is the sprite for on
	
	void Activate()
	{
		
		ThingToActivate.BroadcastMessage(messageToSend);	// call the function in the onject we want to effect
		
		// switch sprites on this object
		offState.active = true;	
		onState.active = false;
		
		// call the shake function in the camera
		Camera.mainCamera.BroadcastMessage("Shake", 1.2f);
		
		// this code is for a persistant object that can be carried between rooms
		// the object can let the game know if certain events have happened
		if(GameObject.FindGameObjectWithTag("Persistent") != null)
		{
			Debug.Log("Found it");
			PersistantRoomData.roomData.triggered = true;
			Camera.mainCamera.BroadcastMessage("Shake", 1.2f);
		}
		else
			Debug.Log("Persistant object missing");
		
		
	}
}
