using UnityEngine;
using System.Collections;

public class MorriganAttack : MonoBehaviour {

	public GameObject myTarget;
	public GameObject theHero;
	MorriganTarget targetScript;
	
	// Laser Vars
	private float LaserTimer = 0.0f;
	public float LaserCooldown = 1.0f;
	private float LaserFireTimer = 1.0f;
	public float LaserDuration = 1.0f;
	public AudioClip laserSound;
	
	public int LaserDamage = 1;
	
	private LineRenderer lr;
	
	
	public GameObject Shield;
	private int ShieldPower = 2;
	public Collider MyCollider;
	
	
	// Blast Vars
	private float BlastTimer = 3.0f;
	public float BlastCooldown = 3.0f;
	public float BlastDelay = 1.0f;
	public float BlastRadius = 3.0f;
	
	public int BlastDamage = 2;
	public AudioClip blastCharge;
	public AudioClip blastHit;
	
	// Crow Vars
	
	public GameObject Crows;
	private float CrowTimer = 3.0f;
	public float CrowCoolDown = 5.0f;
	public AudioClip crowSection;
	public AudioClip crowAttack1;
	public AudioClip crowAttack2;
	
	void Start()
	{
		if(myTarget == null)
			Debug.Log("myTarget Must be set in the editor");
		else 
			targetScript = myTarget.GetComponent<MorriganTarget>();
		lr = GetComponent<LineRenderer>();
		lr.SetVertexCount(0);
	}
	
	
	
	public void Laser()
	{
		if(LaserTimer > 0)
			LaserTimer -= Time.deltaTime;
		if(LaserTimer < 0)
			LaserTimer = 0;
		
		
		if(LaserTimer == 0)
		{
			FireLaser();
			if(LaserFireTimer > 0)
				LaserFireTimer -=Time.deltaTime;
			if(LaserFireTimer < 0)
				LaserFireTimer = 0;
			if(LaserFireTimer == 0)
			{
				LaserTimer = LaserCooldown;
				LaserFireTimer = LaserDuration;
				StopLaser();
			}
		}
	}
	
	void FireLaser()
	{
		targetScript.currentPhase = MorriganTarget.Phase.Laser;
		lr.SetVertexCount(2);
		lr.SetPosition(0, transform.position);
		lr.SetPosition(1, myTarget.transform.position);
		
		Debug.DrawRay(transform.position, myTarget.transform.position - transform.position, Color.red);
		
		RaycastHit hit;
		
		if(Physics.Raycast(transform.position, myTarget.transform.position - transform.position, out hit, Vector3.Distance(transform.position, myTarget.transform.position)))
		{
			if(hit.collider.tag == "CrystalCover")
			{
				Debug.Log("Crystal HIT!");
				hit.collider.BroadcastMessage("Open");
				hit.collider.enabled = false;
				
				ShieldPower --;
				if(ShieldPower == 0)
				{
					Destroy(Shield);
					MyCollider.enabled = true;
				}
					
			}
				
			else if(hit.collider.tag == "Hero")
			{
				Debug.Log("Hitting the Hero");
				hit.collider.BroadcastMessage("takeDamage", LaserDamage);
			}
			
		}
		
	}
	
	void StopLaser()
	{
		targetScript.currentPhase = MorriganTarget.Phase.ResetLaser;
		lr.SetVertexCount(0);	
	}
	
	public void Blast()
	{
		if(targetScript.currentPhase != MorriganTarget.Phase.Blast)
			targetScript.currentPhase = MorriganTarget.Phase.Blast;
		if(BlastTimer > 0)
			BlastTimer -= Time.deltaTime;
		if(BlastTimer < 0)
			BlastTimer = 0;
		
		
		if(BlastTimer == 0)
		{
			FireBlast();
			BlastTimer = BlastCooldown;
		}
	}
	
	void FireBlast()
	{
		audio.clip = null;
		audio.PlayOneShot(blastCharge);
		Debug.Log("fired");
		targetScript.SnapToHero();
		targetScript.PlayAnimation("charge");
		StartCoroutine(CheckBlast());
	}
	
	IEnumerator CheckBlast()
	{
		
		yield return new WaitForSeconds(BlastDelay);
		audio.PlayOneShot(blastHit);
		Debug.Log("Landed");
		Debug.Log(Vector3.Distance(theHero.transform.position, myTarget.transform.position));
		if(Vector3.Distance(theHero.transform.position, myTarget.transform.position) < BlastRadius)
		{
			// Hero Has been hit
			theHero.BroadcastMessage("takeDamage", BlastDamage);
		}
	}
	
	public void Crow()
	{
		audio.clip = crowSection;
		audio.Play();
		if(CrowTimer > 0)
			CrowTimer -= Time.deltaTime;
		if(CrowTimer < 0)
			CrowTimer = 0;
		
		
		if(CrowTimer == 0)
		{
			Crows.BroadcastMessage("Enable");
			BlastTimer = BlastCooldown;
		}
		
		
	}
	
}
