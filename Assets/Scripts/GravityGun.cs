/// <summary>
/// This is the code for Kú's grav gun
/// </summary>

using UnityEngine;
using System.Collections;

public class GravityGun : MonoBehaviour {

	bool Active = false;		// is the grav gun being used?
	GameObject ThingToMove;		// the object being moved
	GameObject TheTarget;		// the players finger 
	
	
	public void Activate(GameObject theTarget, GameObject thingToMove)
	{
		Active = true;				// the grav gunis being used
		TheTarget = theTarget;		// the finger
		ThingToMove = thingToMove;	// the object being moved
	}
	
	public void Deactivate()
	{
		ThingToMove.BroadcastMessage("Dropped");	// let the object know it has been dropped
		Active = false;								// the grav gun is no longer in use
		TheTarget = null;							// remove the refference to the tatger
		ThingToMove = null;							// clear the Object being moved
	}
	
	void Update()
	{
		// the the gun is in use and it is bing ued on an object
		if(Active && TheTarget != null && ThingToMove != null)
		{
			// move the object
			ThingToMove.transform.position = new Vector3(TheTarget.transform.position.x, TheTarget.transform.position.y, 0f);	
		}
	}
	
}
