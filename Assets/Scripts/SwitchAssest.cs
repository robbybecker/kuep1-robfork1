using UnityEngine;
using System.Collections;

public class SwitchAssest : MonoBehaviour {

	public GameObject Asset1;
	public GameObject Asset2;
	
	public void Switch()
	{
		
		Debug.Log("INteracting");
		Asset1.SetActiveRecursively(false);
		Asset2.SetActiveRecursively(true);
	}
}
