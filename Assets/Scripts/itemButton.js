var equipped : boolean;
var buttonTexture : Texture;
var buttonTextureDisabled : Texture;
var buttonX : int;
var buttonY : int;
var buttonWidth : int;
var buttonHeight : int; 
var dragDelay : int = 4;
var theSkin : GUISkin;
var guiDepth : int ;
var xOffset : int ;
var yOffset : int ;
//popup vars
var popUpWidth : float;
var bgImage:Texture;
var headerText : String;
var bodyText:String;
private var headerObj:GameObject;
private var bodyObj:GameObject;
var lineHeight:int = 24;
var bodySize:int;
var headerSize:int = 24;
var headerStyle:GUIStyle;
var bodyStyle:GUIStyle;
private var initialButtonX :int;
private var initialButtonY :int;
private var clickLength:int = 0;
private var showInfo : boolean = false;
private var followMouse : boolean = false;  
private var clickedInside : boolean;
private var bodyTextHeight:float;
private var popUpContent:GUIContent;

function Awake(){
	initialButtonX = buttonX+PauseContainer.menuPos.x;
	initialButtonY = buttonY+PauseContainer.menuPos.y;
}

function Update () {
	//make sure the user clicked inside the bounds of the object
	if (Input.GetMouseButtonDown(0) && Rect(buttonX-PauseContainer.menuPos.x, Screen.height - buttonHeight - buttonY ,buttonWidth,buttonHeight).Contains(Input.mousePosition) && !equipped){
		clickedInside = true;
	} else if (Input.GetMouseButtonDown(0)&& !Rect(buttonX-PauseContainer.menuPos.x, Screen.height - buttonHeight - buttonY ,buttonWidth,buttonHeight).Contains(Input.mousePosition) && !equipped ){
		clickedInside = false;
		showInfo = false;
	}
	//if the user clicks and drags, the button will follow the mouse while dragged (after the dragDelay number is exceeded)
	if (Input.GetMouseButton(0) && clickedInside && !equipped){
			//while the click is active increase the length of the click
			clickLength++;
			//if the length of the click is over the specified time make the icon follow the mouse, and get rid of info if it is there or ELSE dont drag
			if (clickLength>dragDelay) {
				followMouse = true;
				showInfo = false;
			} else {
				followMouse = false;
			}
		
		
	}
	
	//when the user releases do the following
	if (Input.GetMouseButtonUp(0) && Rect(buttonX-PauseContainer.menuPos.x,Screen.height - buttonHeight - buttonY,buttonWidth,buttonHeight).Contains(Input.mousePosition) && !equipped){
		
		//stop the drag
		followMouse = false;
		
		//if the user taps the button, show the info about the item ELSE IF the user was dragging, stop the drag and return the icon to its original pos ElSE if the info was up, get rid of it. 
		if (clickLength<dragDelay && !showInfo){
			showInfo = true;
			followMouse = false;
			clickLength = 0;
		} else if (clickLength>dragDelay){
			clickLength = 0;	
			if ( Rect(dropArea.placement.x ,Screen.height - dropArea.placement.height - dropArea.placement.y,dropArea.placement.width,dropArea.placement.height).Contains(Input.mousePosition) ){
				dropArea.secondImage = buttonTexture;
				dropArea.buttonX = dropArea.buttonX - xOffset;
				dropArea.buttonY = dropArea.buttonY - yOffset;
				dropArea.buttonWidth = buttonWidth;
				dropArea.buttonHeight = buttonHeight;
				equipped = true;
			}	
			buttonX = initialButtonX;
			buttonY = initialButtonY;		
			//TO DO: implement drop functionality
		} else {
			showInfo = false;
		}
	}
	//if drag is initiated, change the x/y of the button to follow the mouse. 
	if (followMouse){
		buttonX=Input.mousePosition.x - buttonWidth/2;
		buttonY= Screen.height - (Input.mousePosition.y + buttonHeight/2);
	}
	
}

function OnGUI (){
	GUI.depth = guiDepth;
	GUI.skin = theSkin;
	if (equipped) {
		GUI.DrawTexture( Rect(buttonX+PauseContainer.menuPos.x,buttonY,buttonWidth,buttonHeight), buttonTextureDisabled );
	} else {
		GUI.DrawTexture( Rect(buttonX+PauseContainer.menuPos.x,buttonY,buttonWidth,buttonHeight), buttonTexture );
	}
	if (showInfo){
		
		/*GameObject go = new GameObject("GUIText");
		headerObj = go.AddComponent(typeof(GUIText)); 
		//headerObject = new GUIText();
		headerObj.text = headerText;
		headerObj.font = headerFont;*/
		popUpContent = GUIContent(bodyText);
		bodyTextHeight = bodyStyle.CalcHeight(popUpContent, popUpWidth - 70);
		//bodyTextHeight = GUI.skin.GetStyle("bodyStyle").CalcSize(GUIContent(bodyText)).y;
		//Debug.Log("bodytextheight="+bodyTextHeight);
		GUI.Box (Rect(buttonX+73,buttonY,popUpWidth, bodyTextHeight+117),"" );
		
		
		GUI.DrawTexture(Rect(buttonX+100,buttonY+9,popUpWidth, 10 ), bgImage, ScaleMode.ScaleAndCrop, true, 0.0f);
		GUI.Label( Rect(buttonX+122,buttonY+39, popUpWidth, buttonHeight ), headerText, headerStyle );
		GUI.Label( Rect(buttonX+122,buttonY+74, popUpWidth-80, 500 ), bodyText, bodyStyle );
		GUI.DrawTexture(Rect(buttonX+100,buttonY+74+bodyTextHeight+15,popUpWidth, 10 ), bgImage, ScaleMode.ScaleAndCrop, true, 0.0f);
		
	}
	
}

/*
GUIContent content = new GUIContent(textToMeasure);
Vector2 size = textGUIStyle.CalcSize(content);
size = new Vector2(size.x, msgEntryStyle.CalcHeight(content, size.x));*/