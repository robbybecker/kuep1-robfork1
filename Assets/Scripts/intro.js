var titleImage: Texture2D;
var logo:Texture;
var titleSound: AudioClip;

private var screenWidth:int = Screen.width;
private var screenHeight:int = Screen.height;
private var textureAlpha:Color;
private var fade : float = 1;

function Update () {
}


function Awake()
{
    StopCoroutine("FadeCoroutine");
    StartCoroutine("FadeCoroutine");
}

function OnGUI () {
	
	if (Input.GetMouseButtonUp(0)){
		Lumos.Event("Skipped Intro");
		Application.LoadLevel("titleScreen");
	}
    
    if (fade > 0)
    {
        GUI.color = Color.white;
        GUI.color.a = fade;
        GUI.DrawTexture(Rect(0,0,1024,768), titleImage, ScaleMode.ScaleAndCrop);
        GUI.color = Color.white;
    }
}



function FadeCoroutine()
{
    while(true)
    {
        fade = 0;
        var time : float = 0;
        audio.Play();
        while (time < 2)
        {
            yield;
            fade += Time.deltaTime / 2;
            time += Time.deltaTime;
        }
        fade = 1;
        time = 0;
        
        yield WaitForSeconds(5);
            while (time < 5)
        {
            yield;
            fade -= Time.deltaTime / 2;
            time += Time.deltaTime;
        }
        fade = 0;
        //yield WaitForSeconds(3);
		Application.LoadLevel("titleScreen");
		
        
        
    }
}

