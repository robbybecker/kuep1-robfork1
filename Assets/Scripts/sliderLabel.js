static var sfxVolume: float = 0.7;
static var musicVolume: float = 0.3;

enum SliderType {Sfx, Music}// is the slider for sfx or music
var sliderType: SoundType = SoundType.Sfx; // define the sound type in the Inspector

var theSkin : GUISkin;
var boxStyle : GUIStyle;
var showBox :boolean = false;

//defines slider
var theSliderArea : Rect;
var sliderValue : float = 0.5;
var sliderHeight : int = 506;
var sliderWidth : int = 77;
var sliderX : int = 42;
var sliderY : int = 87;
var guiDepth : int = 0;


private var iniVol: float; // initiall vol
private var gVol: float; // new vol

	


function Update() {

	 

	theSliderArea = Rect (sliderX+PauseContainer.menuPos.x, sliderY, sliderWidth, sliderHeight);
	//Debug.Log("sliderarea="+theSliderArea);
	//Debug.Log("mouse="+Input.mousePosition);
	if (Input.GetMouseButtonDown(0) && Rect(sliderX+PauseContainer.menuPos.x, Screen.height - sliderHeight - sliderY, sliderWidth, sliderHeight).Contains(Input.mousePosition)){
		showBox = true;
	}
	if (Input.GetMouseButtonUp(0)){
		showBox=false;
	}
}

function sliderBox (sliderValue : float) {
	GUI.skin = theSkin;
	sliderValue = GUI.VerticalSlider (theSliderArea, sliderValue, 1, 0);	
	GUI.skin = null;
	if (showBox){
		GUI.Box ( Rect (sliderX+77+PauseContainer.menuPos.x, 440 - sliderValue * 440.0+88.0, 100, 63 ), Mathf.RoundToInt(sliderValue*100)+ "%", boxStyle ); //music vol box	
	}
	return sliderValue;
	
}

function OnGUI () {
	GUI.depth = guiDepth;
	//sliderBox ();
	
	switch (sliderType){ // get the correct type volume
        case SoundType.Sfx: 
            sfxVolume=sliderBox (sfxVolume);
            //Debug.Log("sliderval="+sliderValue);
            break;
        case SoundType.Music: 
            musicVolume =sliderBox (musicVolume);;
            //Debug.Log("sliderval="+sliderValue);
            break;
    }
}



