//////////////////////////////////
// This code is for the an enemy to fire a projectile
// 
//////////////////////////////////



using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour 
{

    // this is the projectile to be fired
    // it should be set in the editor
    public Object Projectile;	// the porjectile to fire
    public AudioClip fireSound;	// the sounds to make

    void Awake()
    {
        // check to see if the projectile has been set

        if (Projectile == null)
            Debug.Log("Projectile not set in editor");
            
    }


    // this can be called to fire the projectile
    void Fire()
    {
        // create the projectile
		if (IsPaused.isPaused ==false){
	        Instantiate(Projectile, transform.position, Quaternion.identity);
	        audio.PlayOneShot(fireSound);
		}
    }

}
