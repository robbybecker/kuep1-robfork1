using UnityEngine;
using System.Collections;

public class GuardianWeapon : MonoBehaviour {


    public bool Attacking;      // is the weapon attacking?         

    public int Damage = 10;

    public void Awake()
    {
        Attacking = false;              // the weapon is not attacking
        
    }

    public void Attack()    // this is the attack code. It is called from hero class
    {
        Attacking = true;   // set attacking to true, this means the weapon is active and can apply damage

    }

    void OnTriggerEnter(Collider other)
    {
        // this code is run whenever the weapon enters another object

        // if the hero is attacking and the object is an enemy
        if (Attacking && other.gameObject.tag == "Hero")
        {
            other.gameObject.BroadcastMessage("takeDamage", Damage);
            Debug.Log("we are hitting the Hero");
        }
    }

    void attackCompleteResetWeapon()
    {
        // a message is sent form HeroAnimation to Heor when the attack is complete. Hero then calls thei code
        if (Attacking)          // if the weapon is attacking
            Attacking = false;  // stop attacking

        transform.position = transform.parent.position; // move the weapon back to the start position 
    }
}
