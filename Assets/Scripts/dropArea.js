var dropAreaImage:Texture;
var plateX : int = 294;
var plateY : int = 84;
var plateHeight: int = 292;
var plateWidth:int = 624;
var kuX : int = 282;
var kuY : int = 112;
var kuHeight:int = 247;
var kuWidth:int = 447;
var kuImagePlace:Rect;
var emptyImage :Texture; 
var initialPlateImage : Texture;
var initButtonY = 585;
var initButtonX = 372;
var initButtonHeight = 100;
var initButtonWidth = 100;
var guiDepth : int = 1;
static var placement:Rect;
static var secondImage:Texture;
static var thirdImage: Texture;
static var plateImage:Texture;
static var firstFingerPlacement: Rect;
static var secondFingerPlacement: Rect;
static var buttonY : float;
static var buttonX : float;
static var buttonHeight : float;
static var buttonWidth : float;

function Awake() {
	buttonY = initButtonY;
	buttonX = initButtonX;
 	buttonHeight = initButtonHeight;
	buttonWidth = initButtonWidth;
	kuImagePlace = Rect(kuX+PauseContainer.menuPos.x, kuY, kuHeight, kuWidth);
	secondImage = emptyImage;
	thirdImage = emptyImage;
	plateImage = initialPlateImage;
}

function Update (){
	//firstFingerPlacement.height = initialSecondImagePlace.height - firstFingerPlacement.height;
}

function OnGUI () {
	firstFingerPlacement = Rect(buttonX+PauseContainer.menuPos.x, buttonY, buttonWidth, buttonHeight);
	secondFingerPlacement = Rect(buttonX+PauseContainer.menuPos.x+buttonWidth+20, buttonY, buttonWidth, buttonHeight);
	GUI.depth = guiDepth;
	placement = Rect(plateX+PauseContainer.menuPos.x, plateY, plateHeight, plateWidth);
	kuImagePlace = Rect(kuX+PauseContainer.menuPos.x, kuY, kuHeight, kuWidth);
	GUI.DrawTexture(placement, dropAreaImage);
	GUI.DrawTexture(kuImagePlace, plateImage);
	GUI.DrawTexture(firstFingerPlacement, secondImage);
	//GUI.DrawTexture(secondFingerPlacement, thirdImage);
}