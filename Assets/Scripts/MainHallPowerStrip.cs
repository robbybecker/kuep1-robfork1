using UnityEngine;
using System.Collections;

public class MainHallPowerStrip : MonoBehaviour {
	
	public GameObject stripOff;
	public GameObject stripOn;
	
	
	void Start()
	{
		if (PersistantRoomData.roomData != null)
		{
			if(PersistantRoomData.roomData.triggered == true)
			{
				BroadcastMessage("Switch");
				PersistantRoomData.roomData.Done();
			}
		}
	}
	
	
}
