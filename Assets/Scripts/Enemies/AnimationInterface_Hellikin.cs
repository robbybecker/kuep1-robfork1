/// <summary>
/// This class is used to play different animations
/// </summary>


using UnityEngine;
using System.Collections;

public class AnimationInterface_Hellikin : MonoBehaviour {
	
	PackedSprite packedSprite;	// the sprite
	
	// Use this for initialization
	void Awake () 
	{
		// get assecc to the spriteScript
		packedSprite = this.GetComponent<PackedSprite>();
	}
	
	// play death animation
	void Die()
	{
		Debug.Log("Trying to die");
		packedSprite.PlayAnim("death");
	}
}
