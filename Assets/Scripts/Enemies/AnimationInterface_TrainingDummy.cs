using UnityEngine;
using System.Collections;

public class AnimationInterface_TrainingDummy : MonoBehaviour {
	
	PackedSprite packedSprite;
	
	// Use this for initialization
	void Awake () 
	{
		// get assecc to the spriteScript
		packedSprite = this.GetComponent<PackedSprite>();
	}
	
	// play hit animation
	void Hit()
	{
		Debug.Log("Hit being called in animation interface");
		packedSprite.PlayAnim("Hit");
	}
}
