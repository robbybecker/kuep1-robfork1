/// <summary>
/// Bespoke damage class for the training dummy
/// </summary>

using UnityEngine;
using System.Collections;

public class EnemyDamage_TrainingDummy : MonoBehaviour 
{
	private bool isDead=false;
	
	public void TakeDamage(int Damage)
	{
		// play the hit animation
		if (!isDead){
			BroadcastMessage("Hit");
			playSound();
			isDead=true;
			Lumos.Event("Training Dummies Hit", 1);
		}
	}
	
	private void playSound()
	{
		audio.Play();	
	}
}
