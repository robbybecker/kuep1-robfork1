using UnityEngine;
using System.Collections;

public class TrollBehaviour : MonoBehaviour {

    GameObject Hero;    // the hero

    public float range; // the range that will triger a state change
    public float damage;// the mount of damage the enemy does

    // these are the states the enemy has
    public enum Behaviors { asleep = 1, wakeUp = 2, agro = 3, dead = 4 }

    public Behaviors currentBehavior;  // stores the current state

    Vector3 moveVector;         // the vector to move towards
    public float moveSpeed;     // the move speed
    public float attackRange;   // the distance he can attack from
	public float wakeUpRadius;	// the distance to wake him up

    private bool canAttack = true;		// is the troll able to attack
	private bool Alive = true;
	
	
	public GameObject Door;		// the door that will be opened
	
	public AudioClip StompSound;// the sound of him moving around
	public AudioClip AttackSound;
	public AudioClip Hurt;
	
	public AudioSource MusicPlayer;

    void Awake()
    {
    
        // find the hero
        if (Hero == null)
            Hero = GameObject.FindGameObjectWithTag("Hero");

        // set the range
        if (range == 0)
            range = 10f;

        // set the state
        if (currentBehavior == null)
            currentBehavior = Behaviors.asleep;

        if (moveSpeed == 0)
            moveSpeed = 2f;

        if (attackRange == 0)
            attackRange = 2f;

    } 

    // Update is called once per frame
	void Update () 
    {
        if(Alive && IsPaused.isPaused == false)
		{
        	// preform the behavoiur apropriate to your state
        	switch (currentBehavior)
        	{
            	case Behaviors.asleep:
                	beAsleep();
                	break;

            	case Behaviors.wakeUp:
                	beWakeUp();
                	break;

            	case Behaviors.agro:
                	beAgro();
                	break;

				case Behaviors.dead:
					beDead();
					break;
			
        	}
		} else if (audio.isPlaying && IsPaused.isPaused == true) {
				audio.Stop(); 
				Debug.Log("trying to stop audio");
			}
	}


    // Here are the diferent behaviours

    void beAsleep()
    {
		//Debug.Log("I am asleep");
		
		// get distance to the hero
        moveVector = Hero.transform.position - this.transform.position;
		
		if (moveVector.magnitude < wakeUpRadius)
			currentBehavior = Behaviors.wakeUp;

    }

    void beWakeUp()
	{
		// play wake animation
		
		//Debug.Log("WakeUp");
		
		BroadcastMessage("animateWakeUp");
		
		// change sound
		
		
		
		// this will be moved to the animation interface
		
		
	}
	
	void theSleeperHasAwoken()
	{
		// this will be triggered form the Animation Interface when the wake up animation is complete
		// it switches the behavior to agro
		
		audio.clip = StompSound;
		//audio.Play();
		
		MusicPlayer.audio.Play();
		
		currentBehavior = Behaviors.agro;
	}

    void beAgro()
    {
		
		
		if(canAttack)
		{
			//Debug.Log("I am agro");
        	// get direction
			if (!audio.isPlaying && IsPaused.isPaused == false){
				audio.Play();
			} 
        	moveVector = Hero.transform.position - this.transform.position;
	
	        // chekc if the hero is in range
    	    if (moveVector.magnitude > attackRange)
    	    {
        	    // if the hero is out of range
				
				
				
	
    	        // move closer
        	    moveVector.Normalize();             // normalise
        	    moveVector *= moveSpeed;            // apply speed 
        	    moveVector *= Time.deltaTime;       // apply time
				
				
				BroadcastMessage("animateWalk", getAngle(moveVector));
				
            	transform.Translate(moveVector);    // move towards the hero
        	}
        	else   // if the hero is in attack range
        	{
				
            	Attack();

        	}
		}

    }

    // this is the attack loop
    void Attack()
    {
		// play attack animation
		BroadcastMessage("animateAttack", getAngle(moveVector));
		
		canAttack = false;	// the troll cannot attack  - set back to true once the animation is done
		
		
		// use weapon
		StartCoroutine(ApplyDamage());
		
		
	}
	
	IEnumerator ApplyDamage()
	{
		audio.Stop(); //Stop the stomping sound while attacking
		audio.PlayOneShot(AttackSound);
		yield return new WaitForSeconds(0.5f);
		BroadcastMessage("useTheWeapon", getAngle(moveVector));
	}
	
	void trollCanAttackAgain()
	{
		// this will be called form the animation interface.
		// the troll will be able to attack again
		BroadcastMessage("putAwayTheWeapon");
		canAttack = true;
		
	}
	
	void beDead()
	{
		// pley the death animation	
	}
	
	
	// here are the functions
	
	float getAngle(Vector3 vector)
	{
		// 1 - converts a vector to an angle
        // 2 - makes sure that angle is between -180 and 180

        float angleRaw = (float)Mathf.Atan2(vector.y, vector.x); // get the angle

        
        while (angleRaw < -(float)Mathf.PI)     // while the angle is less than -180
            angleRaw += (float)(Mathf.PI * 2);  // add 360

        while (angleRaw > (float)Mathf.PI)      // while the angle is greater than 180
            angleRaw -= (float)(Mathf.PI * 2);  // subtract 360

        return angleRaw * Mathf.Rad2Deg;
	}
	
	void stopDoingStuff()
	{
		Alive = false;	
		Door.BroadcastMessage("Open");
		Camera.mainCamera.BroadcastMessage("StartPan", Door);
		audio.Stop();
		
	}

}
