/// <summary>
/// This is the code for the Laser Emiters
/// A puzzle element that fires lasers
/// </summary>


using UnityEngine;
using System.Collections;

public class LaserEmiter : MonoBehaviour {

	public GameObject source;		// the source of the laser
	public GameObject end;			// the destination of the laser
	private GameObject origionalEnd;// the holds the origainal destination, it is used to reset the object
	
	
	
	public GameObject turtle;
	public GameObject theRightSpot;
	public GameObject goal;
	
	public GameObject door;
	
	LineRenderer lineRenderer;
	
	private bool solved = false;
	
	public void Start()
	{
		origionalEnd = end;							// set up the origionalEnd, it is used to reset the emitter in Reset()
		lineRenderer = GetComponent<LineRenderer>();// access the line renderer
		lineRenderer.SetVertexCount(2);				// set the amount of pints onm the line
	}
	
	
	public void Update()
	{
		if(!solved)
		{
			lineRenderer.SetPosition(0, source.transform.position);	// set the start of the laser
			lineRenderer.SetPosition(1, end.transform.position);	// set the end of the laser
		}
		else
		{
			lineRenderer.SetPosition(0, source.transform.position);	// set the start of the laser
			lineRenderer.SetPosition(1, turtle.transform.position); // set the mispoint
			lineRenderer.SetPosition(2, goal.transform.position);	// set the endpoint
		}
			
	}
	
	
	// this is used by external objects to change the destination of the laser
	public void SwitchReciever(GameObject newReciever)
	{
		end = newReciever;	
	}
	
	// this is used by external objects to reset the laser
	public void Reset()
	{
		end = origionalEnd;
	}
	
	void checkSolution()
	{
		Debug.Log("checking solution");
		Debug.Log(Vector3.Magnitude(turtle.transform.position - theRightSpot.transform.position));
		if(Vector3.Magnitude(turtle.transform.position - theRightSpot.transform.position) < 2f)
		{
			solvePuzzle();
		}
	}
	
	void solvePuzzle()
	{
		
		lineRenderer.SetVertexCount(3);
		goal.BroadcastMessage("Animate"); // begin the pulsing animation
		turtle.BroadcastMessage("doNotRecover");
		if (!solved){ 
			door.BroadcastMessage("Open");
		}
		solved = true;
	}
}
