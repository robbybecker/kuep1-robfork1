using UnityEngine;
using System.Collections;

public class MorriganCrystal : MonoBehaviour {

	PackedSprite ps;
	public GameObject Crystal;
	
	
	void Start()
	{
		ps = this.GetComponent<PackedSprite>();
		Crystal.collider.enabled = false;
	}
	
	void Open()
	{
		ps.PlayAnim(0);	
		Crystal.collider.enabled = true;
		
	}
}
