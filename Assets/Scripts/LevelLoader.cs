//////////////////////////
/// This script load is attacked to a trigger
/// When a player enteres it loads a new level
/// This is set in the inspector
/////////////////////////


using UnityEngine;
using System.Collections;

public class LevelLoader : MonoBehaviour 
{
    // the name of the level to be loaded next
    // must be set in the inspector
    public string LevelToLoad;

    // this is a list of options for where the start position of the next level is
    public enum ScreenStartPosition { North, South, East, West }
    
    // this hold the next start position
    // it must be set in the inspector
    public ScreenStartPosition NextScreenStartPosition;

    // the game manager hold data across scenes
    public GameObject gameManager;


    void Awake()
    {
        // find the game manager
        if (gameManager == null)
            gameManager = GameObject.FindGameObjectWithTag("GameManager");
    }

    // this is called when somthing enters the LevelLoader
    void OnTriggerEnter(Collider other)
    {
		Debug.Log("In the loader");
        // check if the object is the hero
        if (other.gameObject.tag == "Hero")
        {
            // set the next scenes start posion in the game manager
			
            gameManager.BroadcastMessage("setStartPosition", NextScreenStartPosition.ToString());
			
            // Load the next level
            Application.LoadLevel(LevelToLoad);
        }
    }
	
}
