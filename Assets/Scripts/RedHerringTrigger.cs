using UnityEngine;
using System.Collections;

public class RedHerringTrigger : MonoBehaviour {
	
	public GameObject crow;
	
	
	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Hero")
		{
			crow.BroadcastMessage("Go");
			collider.enabled = false;
		}
	}
}
