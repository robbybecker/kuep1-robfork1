////////////////////
// Here be the code for the game manager
// It is used to hold persistant data across scenes
// Such as the player position, items, health
// It is created once in a hidden start scene, and then carried in to the rest of the game
///////////////////



using UnityEngine;
using System.Collections;


public class GameManager : MonoBehaviour {
	
	


    public GameObject heroStartPosition;    // where the hero should be at the start of a scene 
    public GameObject Hero;                 // our hero

    // This strig will hold the tag of the correct start position for the next scene 
    private string newStartPositionTag = "gameStart";


    void Awake()
    {
        // this makes the object persistant across scenes
        DontDestroyOnLoad(this.gameObject);
    }

    // this function is called form the level loader blocks
    // its function is to tell the game manager where the hero should be at the start of the next scene
    void setStartPosition (string newStartPosition)
    {
        newStartPositionTag = newStartPosition;
    }

    //this is called when ever the scene changes
    void OnLevelWasLoaded()
    {
		 Hero = GameObject.FindGameObjectWithTag("Hero");                            // find the hero
		if(Hero != null){
	        heroStartPosition = GameObject.FindGameObjectWithTag(newStartPositionTag);  // find his start position
			Hero.transform.position = new Vector3(heroStartPosition.transform.position.x, heroStartPosition.transform.position.y, 0.0f);             // put him there
		}
        
    }
	
	// still pretending to code
	// this is so awesome
	// I should probobly be careful
	
	
	
}
