// ----- Hero Script -----
// ------Owen ----- 6/7/11
// This handles player movement & attack



using UnityEngine;
using System.Collections;

public class Hero : MonoBehaviour {

    private bool alive = true;
	
	
	private CharacterController charController;		// the Character Controller
	
	
	private enum Action { Idle = 0, Walk = 1, Attack = 2, Die = 3, MoveObject = 4, DoStuff = 5 }
	
	private Action currentAction;			// what the player is doing now
	private bool Available = true;			// is the Hero available for a new action
	
	private Action actionToAnimate;
	
	public enum Direction { West = 1, NW = 2, North = 3, NE = 4, East = 5, SE = 6, South = 7, SW = 8, NONE = 9 }
	
	private Direction currentDirection;
	
	public enum Item { none = 0, Sword = 1, Hurley = 2}
	public Item currentItem = Item.Sword;
	
	private GameObject target;				// the target
	public GameObject heroWeapon;			// the weapon
	
	public float Deadzone = 2.5f;			// the deadzone
	
	private float horAxis = 0f;				// the horisontal movement
	private float vertAxis = 0f;			// the vertical movement
	
	private Vector3 targetPos;				// the position of the target
	private Vector3 moveVector;				// the movement vector
	private Vector3 diferenceVector;		// the diference between target and player
	
	public float Speed = 6f;				// how fast the hero moves
	public float atTargetDistance = 0.1f;
	
	public GameObject heroSprite;
	private HeroAnimation heroAnimation;	// the hero animation script
	
	public AudioClip attackSound;			// the sound he makes when he attacks
	
	public GameObject enemyToAttack;		// the targeted enemy
	public float attackRange = 2.5f;			// the range of the attack
	public int Damage = 1;
	
	public GameObject objectToMove;
	public GameObject InteractiveObject;
	
    void Start()
    {
        alive = true;

        // set up the character
        charController = GetComponent("CharacterController")as CharacterController;  // get the players character Controller, used to move the player                                                       // reset the lastAction

        
    	currentAction = Hero.Action.Idle;	// set the character to Idle at the stary of the scene
		actionToAnimate = currentAction;
		
		target = GameObject.FindGameObjectWithTag("theTarget");						// find the Target
		if (heroWeapon == null)
			heroWeapon = GameObject.FindGameObjectWithTag("HeroWeapon");
		
		heroAnimation = heroSprite.GetComponent<HeroAnimation>();
		
	}

    // This is where it all begins 
    // This takes in information from the target
    // what it is toughing and the game object

    

    public void determineAction(string contact, GameObject activeObject)
    {
		
		
        if(alive & !IsPaused.isPaused)
		{
			// what is the target touching?
        	switch (contact)
        	{
            	case "ground":
                	// the player is touching the ground
                	currentAction = Hero.Action.Walk;
					actionToAnimate = currentAction;
					
					break;
            
            	case "held":
                	// I am not sure if anything needs to be here
                	break;
				
				case "interact":
					// player is interacting
					currentAction = Action.DoStuff;
					InteractiveObject = activeObject;
					break;
					

            	case "enemy":
					// the player is attacking
					currentAction = Hero.Action.Attack;
					enemyToAttack = activeObject;
                	break;
				
				case "moveObject":
					currentAction = Hero.Action.MoveObject;
					objectToMove = activeObject;
					break;
				
				case "noInput":
                	// the player is not touching the screen
					currentAction = Hero.Action.Idle;
					break;
			}
			applyAction(currentAction);
		}
    }
	
	void Update()
	{
		applyAction(currentAction);	
	}
	
	void applyAction(Action actionToApply)
	{
		if(Available)
		{
			
			switch (actionToApply)
			{
			case Action.Walk:
				// move the game object
				moveHero();
				break;
			
			case Action.DoStuff:
				// interact with object
				interactWithObject(InteractiveObject);
				break;
				
			case Action.Attack:
				// do an attack
				attack(enemyToAttack);
				break;
			
			case Action.Idle:
				// be Idle
				idle();
				break;
			
			case Action.MoveObject:
				//moveing an object
				moveObject();
				break;
				
			}
		
			Animate();
		}
	}
	
	
	
    void moveHero()
    {
        // moves the character controller
        if(!audio.isPlaying)
            audio.Play();
        
        // where is the target?
        targetPos = target.transform.position;

		diferenceVector = targetPos - this.transform.position;
		if( Vector3.Distance(targetPos, this.transform.position) > atTargetDistance)
		{
			currentDirection = getDirection(diferenceVector);
			//Debug.Log(currentDirection);
			move(currentDirection, diferenceVector);
		}
		else
			currentAction = Action.Idle;
    }
	
	
	
	void move(Direction moveDirection, Vector3 moveVectorA)
	{
		
		moveVector = dircetionToVector(moveDirection);
		moveVector.z = 0f;
		moveVector *= Speed;
		moveVector *= Time.deltaTime;
		
		charController.Move(moveVectorA.normalized * Speed * Time.deltaTime );
		
		if(transform.position.z != 0f)
		{
			transform.position = new Vector3(transform.position.x, transform.position.y, 0f);
		}
	}
	
	
	void interactWithObject(GameObject theThing)
	{
		Debug.Log(Vector3.Distance(theThing.transform.position, transform.position));
		if(Vector3.Distance(theThing.transform.position, transform.position) > attackRange)
		{
			actionToAnimate = Hero.Action.Walk;
			moveHero();
		}
		else
		{
			
			currentAction = Action.Idle;
			actionToAnimate = Hero.Action.Idle;
			theThing.BroadcastMessage("Activate");
			
			//TODO: Add some audio
		}
	}
	
	
	
	
    // ---------- this is where I am 
    
	void attack(GameObject enemy)
    {
		if(Vector3.Distance(enemy.transform.position, transform.position) > attackRange)
		{
			actionToAnimate = Hero.Action.Walk;
			moveHero();
		}
		else
		{
			Debug.Log("I should attack Now");
			
			switch(currentItem)
			{
			case Item.Sword:
				actionToAnimate = Hero.Action.Attack;
				
				enemy.BroadcastMessage("TakeDamage", Damage);

        		audio.PlayOneShot(attackSound);
				break;
				
			case Item.Hurley:
				actionToAnimate = Hero.Action.Attack;
				Vector3 attackVector = (targetPos - transform.position).normalized;
				
				enemy.BroadcastMessage("TakeDamage", Damage);
				
				break;
			}
			Available = false;
		}
    }
    
	
	void attackComplete()
    {
        Available = true;
        BroadcastMessage("attackCompleteResetWeapon");
		currentAction = Hero.Action.Idle;
		actionToAnimate = currentAction;
	}
	
	void moveObject()
	{
		
		Available = false;	
		Debug.Log("Move being called");
	}
	
	public void moveComplete()
	{
		Available = true;	
		currentAction = Hero.Action.Idle;
		Debug.Log("Should be able to move!");
	}

    

    void idle()
    {
        //if (audio.isPlaying)
            //audio.Stop();
        // stuff form get input
		actionToAnimate = Hero.Action.Idle;
    }
	
	
	void Animate()
	{
		heroAnimation.SetState(actionToAnimate.GetHashCode(), currentDirection.GetHashCode(), currentItem.GetHashCode());
	}
	
    /*
    public void animationInterface(Action theAction, Direction theDirection)      // pass in the current action
    {

    }
    */
	
	
	
	
	
   

    
	
	void stopDoingStuff()
	{
		this.enabled = false;	
	}
	
	
	Direction getDirection( Vector3 theVector)
	{
		float radians = VectorToRadian(theVector);
		float degrees = (radians * Mathf.Rad2Deg);
		
		if (degrees >= 157.5f)
            return Direction.West;  //left
        else if (degrees < 157.5f && degrees >= 112.5f)
            return Direction.NW;  // left & up
        else if (degrees < 112.5f && degrees >= 67.5f)
            return Direction.North;  // up
        else if (degrees < 67.5f && degrees >= 22.5f)
            return Direction.NE;  // right & up 
        else if (degrees < 22.5f && degrees >= -22.5f)
            return Direction.East;  // right
        else if (degrees < -22.5f && degrees >= -67.5f)
            return Direction.SE;  // right & down
        else if (degrees < -67.5f && degrees >= -112.5f)
            return Direction.South;  // down
        else if (degrees < -112.5f && degrees >= -157.5f)
            return Direction.SW;  // left & down
        else if (degrees < -157.5f)
            return Direction.West;  //left
        else
            return Direction.NONE;
	}
	
	float VectorToRadian(Vector3 vector)
	{
		float angleRaw = (float)Mathf.Atan2(vector.y, vector.x);
		
		while (angleRaw < -(float)Mathf.PI)     // while the angle is less than -180
            angleRaw += (float)(Mathf.PI * 2);  // add 360

        while (angleRaw > (float)Mathf.PI)      // while the angle is greater than 180
            angleRaw -= (float)(Mathf.PI * 2);  // subtract 360

        return angleRaw;
	}
	
	Vector3 dircetionToVector(Direction theDirection)
	{
		//Debug.Log(theDirection.GetHashCode());
		Vector3 theVector = Vector3.zero;
		switch (theDirection) 
		{
		case Direction.East :
				theVector = new Vector3(1f, 0f, 0f);
			break;
			
		case Direction.NE :
				theVector = new Vector3(1f, 1f, 0f);
			break;
			
		case Direction.North :
				theVector = new Vector3(0f, 1f, 0f);
			break;
			
		case Direction.NW :
				theVector = new Vector3(-1f, 1f, 0f);
			break;
			
		case Direction.West:
				theVector = new Vector3(-1f, 0f, 0f);
			break;
			
		case Direction.SW :
				theVector = new Vector3(-1f, -1f, 0f);
			break;
			
		case Direction.South :
				theVector = new Vector3(0f, -1f, 0f);
			break;
			
		case Direction.SE :
				theVector = new Vector3(1f, -1f, 0f);
			break;
			
		case Direction.NONE:
				theVector = Vector3.zero;
			break;
		}
		
		Vector3.Normalize(theVector);
		return theVector;
	}
	
}
