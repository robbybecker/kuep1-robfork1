using UnityEngine;
using System.Collections;

public class TrollAnimation : MonoBehaviour {
	
	private bool isDead = false;
	SuperSprite superSprite;
	
	enum Directions { West = 1, East = 2, North = 3, South = 4 }
	
	private Directions direction = Directions.South;
	
	
	// Use this for initialization
	void Awake () 
	{
		// get a reference to the PS script
        superSprite = this.GetComponent<SuperSprite>();
	}
	
	
	void animateWakeUp()
	{
		superSprite.DoAnim("WakeUp");
		
		superSprite.SetAnimCompleteDelegate(wakeUpDone);
	}
	
	void animateWalk(float angle)
	{
		
		
		if (angle <=0f)
		{
			superSprite.DoAnim("WalkDown");
			if(angle >= -90f)
				transform.eulerAngles = new Vector3(0f, 180f, 0f);
			else
				transform.eulerAngles = new Vector3(0f, 0f, 0f);
		}
		else
		{
			superSprite.DoAnim("WalkUp");
			
			if(angle <= 90f)
				transform.eulerAngles = new Vector3(0f, 0f, 0f);
			else
				transform.eulerAngles = new Vector3(0f, 180f, 0f);
		}
	}
	
	void animateAttack(float angle)
	{
		if(angle <= 0)
			superSprite.DoAnim("AttackDown");
		else 
			superSprite.DoAnim("AttackUp");
		
		
		
		superSprite.SetAnimCompleteDelegate(attackDone);
		
	}
	
	// TODO: get the troll flipping from side to side
	
	
	// here are the functions
	
	Directions calculateDirection(float angle)
	{
		Directions theDirection = Directions.South;
		
		return theDirection;
	}
	
	
	
	// here are the delages
	
	void noDelegate(SuperSprite sprite)
	{
		// nothing to see here
	}
	
	void wakeUpDone(SuperSprite sprite)
	{
		SendMessageUpwards("theSleeperHasAwoken");
		
		superSprite.SetAnimCompleteDelegate(noDelegate);
	}
	
	void attackDone(SuperSprite sprite)
	{
		SendMessageUpwards("trollCanAttackAgain");
		
		superSprite.SetAnimCompleteDelegate(noDelegate);
		
		
	}
	
	void Die()
	{
		if (!isDead){
			superSprite.PlayAnim("Death");	
			isDead=true;
		}
	}
}
