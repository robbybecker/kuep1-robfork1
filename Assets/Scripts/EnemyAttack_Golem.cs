//////////////////////////////////
// Bespoke attack class for the gollem 
// 
//////////////////////////////////



using UnityEngine;
using System.Collections;

public class EnemyAttack_Golem : MonoBehaviour 
{

    // this is the projectile to be fired
    // it should be set in the editor
    public GameObject Weapon;		// the game object that applies dammage
    public float AttackDistance;	// the Distance at which the golem will begin an attakc
    public GameObject Hero;			// the hero
    public AudioClip attackSound;	// the sound to play when he attacks

    private Vector3 attackVector;	// the direction of the attack
    private bool canFire = true;	// can the golem attack?


    void Awake()
    {
        // check for the hero
        if (Hero == null)
            Hero = GameObject.FindGameObjectWithTag("Hero");

        // check to see if the projectile has been set

        if (Weapon == null)
            Debug.Log("Weapon not set in editor");
            
    }




    // this can be called to fattack the hero
    void Fire()
    {
        if (canFire)
        {
            canFire = false;
            // attack, move the weapon
            attackVector = Hero.transform.position - transform.position;
            attackVector.Normalize();
            attackVector *= AttackDistance;

            Weapon.transform.position += attackVector;
            Weapon.BroadcastMessage("Attack");

            audio.PlayOneShot(attackSound);
            StartCoroutine(FinishAttack());
            

        }
    }

    

    IEnumerator FinishAttack()
    {
        yield return new WaitForSeconds(1);
        Weapon.BroadcastMessage("attackCompleteResetWeapon");
        canFire = true;
    }

}
