//------ Hero Animation --------
//------ Plays the correct animation


using UnityEngine;
using System.Collections;

public class HeroAnimation : MonoBehaviour {


    private SuperSprite superSprite;  // the spriteManager class
    public GameObject Hero;             // set in unity, the actual hero object

    public bool Available;              // is the sprite available for a new animation

    public enum Direction { West = 1, NW = 2, North = 3, NE = 4, East = 5, SE = 6, South = 7, SW = 8, NONE = 9 }
	
	private Direction currentDirection = Direction.South;
	
	private enum Action { Idle = 0, Walk = 1, Attack = 2, Die =3 }
	
	private Action currentAction = Action.Idle;
	
	private bool alive = true;
	

	private enum Item { none = 0, Sword = 1, Hurley = 2}
	private Item currentItem = Item.Sword;
	


    void Awake()
    {
		
        // get a reference to the PS script
        superSprite = this.GetComponent<SuperSprite>();
    }

	public void SetState(int theAction, int theDirection, int theItem)
    {
		currentDirection = (Direction) theDirection;
		currentAction = (Action) theAction;
		currentItem = (Item) theItem;
	}
	
	
	void Update()
	{

		Animate(currentAction, currentDirection, currentItem);	
	}
	
	private void Animate(Action theAction, Direction theDirection, Item theItem)
	{
		switch(theAction)
		{
			case Action.Walk:
				// move the game object
				Walk(theDirection);
				break;
			
			case Action.Attack:
				// do an attack

				Attack(theDirection, theItem);
				break;
			
			case Action.Idle:
				// be Idle
				Idle(theDirection);
				break;
	
			case Action.Die:
				// die
				Die();
				break;
		}
	}
	
	private void Walk(Direction walkDirection)
    {

        WalkingAudio.isMoving = true;

        currentDirection = walkDirection;   // this stores the current direction, this is used in the checkForNewDirection function

        switch (walkDirection)              // feed in the direction number
        {
            case Direction.North:
                // UP
                superSprite.DoAnim("WalkUp");
                break;

            case Direction.South:
                // DOWN
                superSprite.DoAnim("WalkDown");
                break;

            case Direction.West:
                // LEFT
                superSprite.DoAnim("WalkLeft");
                break;

            case Direction.East:
                superSprite.DoAnim("WalkRight");
                break;

            case Direction.NE:
                
                superSprite.DoAnim("WalkUpRight");
                break;

            case Direction.NW:
                superSprite.DoAnim("WalkUpLeft");
                break;

            case Direction.SE:
                superSprite.DoAnim("WalkDownRight");
                break;

            case Direction.SW:
                superSprite.DoAnim("WalkDownLeft");
                break;

        }
    }
	

	private void Attack(Direction attackDirection, Item currentItem)
    { 
        superSprite.SetAnimCompleteDelegate(AttackDone);
		if(currentItem == Item.Sword)
		{
			swordAttack(attackDirection);
		}
		else if (currentItem == Item.Hurley)
		{
			rangedAttack(attackDirection);	
		}   
    }
	
	private void swordAttack(Direction attackDirection)
	{
        switch (attackDirection)    // determin the direction of the attack
        {
            case Direction.North:
                // UP
                superSprite.DoAnim("AttackUp");                    // Trigger the UP attack animation
                break;

            case Direction.South:
                // DOWN
                superSprite.DoAnim("AttackDown");                 // Trigger the DOWN attack animation
                break;

            case Direction.West:
                // LEFT
                superSprite.DoAnim("AttackLeft");                  // Trigger the LEFT attack animation
                break;

            case Direction.East:
                // RIGHT
                superSprite.DoAnim("AttackRight");                  // Trigger the LEFT attack - but the sprite is flipped
                break;

			case Direction.NE:
                superSprite.DoAnim("AttackUpRight");
                break;

            case Direction.NW:
                superSprite.DoAnim("AttackUpLeft");
                break;

            case Direction.SE:
                superSprite.DoAnim("AttackDownRight");
                break;

            case Direction.SW:
                superSprite.DoAnim("AttackDownLeft");
                break;
		}

	}
	
	private void rangedAttack(Direction attackDirection)
	{
		switch (attackDirection)    // determin the direction of the attack
        {
            case Direction.North:
                // UP
                superSprite.DoAnim("RangedUp");                    // Trigger the UP attack animation
                break;

            case Direction.South:
                // DOWN
                superSprite.DoAnim("RangedDown");                 // Trigger the DOWN attack animation
                break;

            case Direction.West:
                // LEFT
                superSprite.DoAnim("RangedLeft");                  // Trigger the LEFT attack animation
                break;

            case Direction.East:
                // RIGHT
                superSprite.DoAnim("RangedRight");                  // Trigger the LEFT attack - but the sprite is flipped
                break;

			case Direction.NE:
                superSprite.DoAnim("RangedUpRight");
                break;

            case Direction.NW:
                superSprite.DoAnim("RangedUpLeft");
                break;

            case Direction.SE:
                superSprite.DoAnim("RangedDownRight");
                break;

            case Direction.SW:
                superSprite.DoAnim("RangedDownLeft");
                break;
		}	
	}
	
	
    void AttackDone(SuperSprite sprite)
    {
        superSprite.SetAnimCompleteDelegate(noDelegate);
        SendMessageUpwards("attackComplete");
    }

    void noDelegate(SuperSprite sprite)
    {
        // this is to remove the animation delegate
    }

    private void Idle(Direction idleDirection)
    {
		WalkingAudio.isMoving = false;

		switch (idleDirection)
		{
			case Direction.North:
                // UP
                superSprite.DoAnim("IdleUp");                      // Trigger the UP walk animation
                break;

            case Direction.South:
                // DOWN
                superSprite.DoAnim("IdleDown");                   // Trigger the DOWN walk animation
                break;

            case Direction.West:
                // LEFT
                superSprite.DoAnim("IdleLeft");                    // Trigger the LEFT walk animation
                break;

            case Direction.East:
                superSprite.DoAnim("IdleRight");                    // Trigger the LEFT walk animation, but the sprite is flipped
                break;

            case Direction.NE:
                superSprite.DoAnim("IdleUpRight");
                break;

            case Direction.NW:
                superSprite.DoAnim("IdleUpLeft");
                break;

            case Direction.SE:
                superSprite.DoAnim("IdleDownRight");
                break;

            case Direction.SW:
                superSprite.DoAnim("IdleDownLeft");
                break;
		}
    }

	
	public void Die()
	{
		if(alive)
		{
			superSprite.PlayAnim("death");	
			alive = false;
			this.enabled = false;
		}
	}
}
