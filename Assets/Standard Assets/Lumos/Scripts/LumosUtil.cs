// Copyright (c) 2011 Rebel Hippo Inc. All rights reserved.

using System.Security.Cryptography;
using System.Text;
using UnityEngine;

/// <summary>
/// Utility functions.
/// </summary>
public static class LumosUtil
{
	/// <summary>
	/// The key used to store the player ID in PlayerPrefs.
	/// </summary>
	public static string playerIdPrefsKey {
		get { return "lumos_" + Lumos.appId + "_player_id"; }
	}

	/// <summary>
	/// Generates a string resembling a UUID with the game ID as the first chunk.
	/// It doesn't exactly follow the version 4 UUID spec, but close enough.
	/// We don't use System.Guid because (for some unknown reason) it triggers a NullReferenceException on the iPad.
	/// </summary>
	/// <returns>A unique player ID.</returns>
	public static string GeneratePlayerId ()
	{
		var id = Lumos.appId + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4();
		return id;
	}

	/// <summary>
	/// Helper method for GeneratePlayerId that creates a four character random number.
	/// </summary>
	/// <returns></returns>
	static string S4 ()
	{
		var str = Random.Range(0, ushort.MaxValue + 1).ToString("X4").ToLower();
		return str;
	}
	
	/// <summary>
	/// Generates an MD5 hash of a string.
	/// </summary>
	/// <param name="strings">The strings to create a hash from.</param>
	/// <returns>The hash.</returns>
	public static string MD5Hash (params string[] strings)
	{
		var combined = "";
		
		foreach (var str in strings) {
			combined += str;
		}
		
		var bytes = Encoding.ASCII.GetBytes(combined);
		
		// Encrypt bytes
		var md5 = new MD5CryptoServiceProvider();
		var data = md5.ComputeHash(bytes);
		
		// Convert encrypted bytes back to a string (base 16)
		var hash = new StringBuilder();
		
		foreach (var b in data) {
			hash.Append(b.ToString("x2").ToLower());
		}
		
		return hash.ToString();
	}
}
